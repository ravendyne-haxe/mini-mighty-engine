package mme.geometry;

import mme.math.glmatrix.Vec3;
import lime.math.RGBA;

import mme.core.SceneObject;
import mme.material.LineMaterial;


class PolyLine extends SceneObject {

    public var isClosed : Bool;
    private var isFinalized : Bool;

    public var lineMaterial ( default, null ) : LineMaterial;

    public function new( lineColor : RGBA = 0x000000ff, thickness : Float = 2.0, ?edgeColor : RGBA, ?lineMaterial : LineMaterial ) {

        if( lineMaterial == null )
            lineMaterial = new LineMaterial( lineColor, thickness, edgeColor );

        super( lineMaterial );

        this.lineMaterial = lineMaterial;

        // as far as Mesh is concerned, our polyline
        // is just a collection of points.
        // Mesh.Line element type assumes two vertices per
        // line, which is not the case here, so we use Point
        mesh.elementType = Point;
        isClosed = false;
        isFinalized = false;
    }

    public function addP( point : Vec3 ) {

        if( ! isFinalized ) {
            mesh.vertices.push( point );
        }

        return this;
    }

    public function addPoints( points : Array<Vec3> ) {

        if( ! isFinalized ) {
            for( p in points )
                mesh.vertices.push( p );
        }

        return this;
    }

    public function close() {

        isClosed = true;
        finalize();
    }

    public function finalize() {

        if( mesh.vertices.length < 2 ) {
            throw "PolyLine needs at least 2 points to make a path.";
        }
        if( mesh.vertices.length == 2 ) isClosed = false;

        isFinalized = true;
    }
}
