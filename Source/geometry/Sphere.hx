package mme.geometry;

import mme.math.Parametric;
import mme.math.glmatrix.Vec2;
import lime.utils.Log;
import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;

import mme.core.SceneObject;
import mme.core.Material;

class Sphere extends SceneObject {

    public var radius : Float;

    public var segmentsX : Int;
    public var segmentsY : Int;

    public function new( radius : Float, segmentsW : Int = 10, segmentsH : Int = 5, ?material : Material ) {

        super( material );

        this.radius = radius;

        segmentsX = segmentsW < 10 ? 10 : segmentsW;
        segmentsY = segmentsH < 5 ? 5 : segmentsH;

        updateMesh();
    }

    public function updateMesh() : Void {

        Parametric.ssurface( segmentsX, segmentsY, function( theta, phi ) {

            var p = SphericalGenerators.sphere( theta, phi );
            return [ radius * p.x, radius * p.y, radius * p.z ];
        }, mesh );
    }
}
