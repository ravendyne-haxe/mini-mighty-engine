package mme.geometry;

import mme.math.Parametric;

import mme.core.SceneObject;
import mme.core.Material;
import mme.core.Mesh;


class Plane extends SceneObject {

    public var width : Float;
    public var height : Float;

    public var segmentsW : Int;
    public var segmentsH : Int;

    public function new( width : Float, height: Float, segmentsW : Int = 1, segmentsH : Int = 1, ?material : Material ) {

        super( material );

        this.width = width;
        this.height = height;

        this.segmentsW = segmentsW;
        this.segmentsH = segmentsH;

        updateMesh();
    }

    public function updateMesh() : Void {

        makeXY( width, height, segmentsW, segmentsH, mesh );
    }

    public static function makeXY( sizeX : Float, sizeY: Float, segmentsX : Int = 1, segmentsY : Int = 1, ?mesh : Mesh ) : Mesh  {

        if( mesh == null ) mesh = new Mesh();

        Parametric.psurface( segmentsX, segmentsY, function( u, v ) {
            var p = PlanarGenerators.xyplane( u, v );
            return [ sizeX * p.x, sizeY * p.y, p.z ];
        }, mesh );

        return mesh;
    }
}
