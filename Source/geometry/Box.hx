package mme.geometry;

import mme.math.glmatrix.Vec3;

import mme.core.SceneObject;
import mme.core.Material;


class Box extends SceneObject {

    public var width : Float;
    public var height : Float;

    public var segmentsW : Int;
    public var segmentsH : Int;

    public function new( width : Float, height: Float, segmentsW : Int = 1, segmentsH : Int = 1, ?material : Material ) {

        super( material );

        this.width = width;
        this.height = height;

        this.segmentsW = segmentsW;
        this.segmentsH = segmentsH;

        updateMesh();
    }

    // TODO box has width, height and depth
    public function updateMesh() : Void {

        // front, Z+
        var front = Plane.makeXY( width, height );
        front.moveTo([ 0, 0, height/2 ]);

        // back, Z-
        var back = Plane.makeXY( width, height );
        back.rotateTo( 180, Vec3.Y_AXIS );
        back.moveTo([ 0, 0, -height/2 ]);

        // right, X+
        var right = Plane.makeXY( width, height );
        right.rotateTo( 90, Vec3.Y_AXIS );
        right.moveTo([ height/2, 0, 0 ]);

        // left, X-
        var left = Plane.makeXY( width, height );
        left.rotateTo( -90, Vec3.Y_AXIS );
        left.moveTo([ -height/2, 0, 0 ]);

        // top, Y+
        var top = Plane.makeXY( width, height );
        top.rotateTo( -90, Vec3.X_AXIS );
        top.moveTo([ 0, height/2, 0 ]);

        // bottom, Y-
        var bottom = Plane.makeXY( width, height );
        bottom.rotateTo( 90, Vec3.X_AXIS );
        bottom.moveTo([ 0, -height/2, 0 ]);


        top.merge( bottom );
        top.merge( left );
        top.merge( right );
        top.merge( front );
        top.merge( back );

        mesh.vertices = top.vertices;
        mesh.uvs = top.uvs;
        mesh.normals = top.normals;
        mesh.indices = top.indices;
    }
}
