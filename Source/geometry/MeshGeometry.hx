package mme.geometry;

import mme.core.SceneObject;
import mme.core.Mesh;
import mme.core.Material;


class MeshGeometry extends SceneObject {

    public function new( mesh : Mesh, ?material : Material ) {

        super( material );

        this.mesh = mesh;
    }
}
