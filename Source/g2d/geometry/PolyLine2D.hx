package mme.g2d.geometry;

import mme.math.glmatrix.Vec2;
import lime.math.RGBA;

import mme.core.SceneObject;
import mme.g2d.material.Line2DMaterial;


class PolyLine2D extends SceneObject {

    public var isClosed : Bool;
    private var isFinalized : Bool;

    public var lineMaterial ( default, null ) : Line2DMaterial;

    public function new( lineColor : RGBA = 0x000000ff, thickness : Float = 2.0, ?edgeColor : RGBA, ?lineMaterial : Line2DMaterial ) {

        if( lineMaterial == null )
            lineMaterial = new Line2DMaterial( lineColor, thickness, edgeColor );

        super( lineMaterial );

        this.lineMaterial = lineMaterial;

        // as far as Mesh is concerned, our polyline
        // is just a collection of points.
        // Mesh.Line element type assumes two vertices per
        // line, which is not the case here, so we use Point
        mesh.elementType = Point;
        isClosed = false;
        isFinalized = false;
    }

    public function addP( point : Vec2 ) {

        if( ! isFinalized ) {
            mesh.vertices.push([ point.x, point.y, 0.0 ]);
        }

        return this;
    }

    public function addPoints( points : Array<Vec2> ) {

        if( ! isFinalized ) {
            for( p in points )
                mesh.vertices.push([ p.x, p.y, 0.0 ]);
        }

        return this;
    }

    public function close() {

        isClosed = true;
        finalize();
    }

    public function finalize() {

        if( mesh.vertices.length < 2 ) {
            throw "PolyLine needs at least 2 points to make a path.";
        }
        if( mesh.vertices.length == 2 ) isClosed = false;

        isFinalized = true;
    }
}
