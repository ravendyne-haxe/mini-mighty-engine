package mme.g2d.geometry;

import lime.math.RGBA;
import mme.math.glmatrix.Vec2;

import mme.core.SceneObject;
import mme.g2d.material.Point2DMaterial;


class PointCloud2D extends SceneObject {

    public var width : Float;
    public var height : Float;

    private var isFinalized : Bool;

    public function new( width : Float = 10.0, height : Float = 10.0, ?material : Point2DMaterial, ?color : RGBA ) {

        if( color != null ) {
            material = new Point2DMaterial( color );
        }

        super( material );

        this.height = height;
        this.width = width;

        mesh.elementType = Point;
        isFinalized = false;
    }

    public function addP( point : Vec2 ) {

        if( ! isFinalized ) {
            mesh.vertices.push([ point.x, point.y, 0.0 ]);
            mesh.otherParams.push([ width, height ]);
        }

        return this;
    }

    public function addPoints( points : Array<Vec2> ) {

        if( ! isFinalized ) {
            for( p in points ) {
                mesh.vertices.push([ p.x, p.y, 0.0 ]);
                mesh.otherParams.push([ width, height ]);
            }
        }

        return this;
    }

    public function finalize() {

        isFinalized = true;
    }
}
