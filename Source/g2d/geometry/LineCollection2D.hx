package mme.g2d.geometry;

import mme.util.geometry.line2d.LineUtils;
import mme.math.glmatrix.Vec2;
import lime.math.RGBA;

import mme.core.SceneObject;
import mme.g2d.material.Line2DMaterial;


class LineCollection2D extends SceneObject {

    private var isFinalized : Bool;

    public var lineMaterial ( default, null ) : Line2DMaterial;

    public function new( lineColor : RGBA = 0x000000ff, thickness : Float = 2.0, ?edgeColor : RGBA, ?lineMaterial : Line2DMaterial ) {

        if( lineMaterial == null )
            lineMaterial = new Line2DMaterial( lineColor, thickness, edgeColor );

        super( lineMaterial );

        this.lineMaterial = lineMaterial;

        mesh.elementType = Line;
        isFinalized = false;
    }

    public function addLine( start : Vec2, end : Vec2 ) {

        if( ! isFinalized ) {
            mesh.vertices.push([ start.x, start.y, 0.0 ]);
            mesh.vertices.push([ end.x, end.y, 0.0 ]);
        }

        return this;
    }

    public function addLines( lines : Array< Array< Vec2 > > ) {

        if( ! isFinalized ) {
            for( lin in lines ) {
                mesh.vertices.push([ lin[ 0 ].x, lin[ 0 ].y, 0.0 ]);
                mesh.vertices.push([ lin[ 1 ].x, lin[ 1 ].y, 0.0 ]);
            }
        }

        return this;
    }

    public function finalize() {

        isFinalized = true;
    }
}
