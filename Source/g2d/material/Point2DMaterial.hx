package mme.g2d.material;

import lime.math.RGBA;

import mme.core.Material;

enum PointType {
    Circle;
    Square;
}

class Point2DMaterial extends Material {

    public var edgeColor : RGBA;
    public var type ( default, null ) : PointType;

    public function new( color : RGBA = 0x000000FF, type : PointType = Circle, ?edgeColor : RGBA ) {

        super();

        emissiveColor = color;
        this.edgeColor = edgeColor == null ? color : edgeColor;
        this.type = type;
    }
}
