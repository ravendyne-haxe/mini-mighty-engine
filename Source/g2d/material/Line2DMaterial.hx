package mme.g2d.material;

import lime.math.RGBA;

import mme.core.Material;

enum LineBlending {
    Sharp;
    Smooth;
    ThinLines;
}

class Line2DMaterial extends Material {

    public var thickness : Float;
    public var inner : Float;
    public var edgeColor : RGBA;
    public var lineBlending : LineBlending;

    public function new( color : RGBA = 0x000000FF, thickness : Float = 2.0, ?edgeColor : RGBA ) {

        super();

        emissiveColor = color;
        this.edgeColor = edgeColor == null ? color : edgeColor;

        this.thickness = thickness;
        this.inner = 0.7;

        lineBlending = Smooth;
    }
}
