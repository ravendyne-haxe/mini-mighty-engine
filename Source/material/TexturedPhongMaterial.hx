package mme.material;

import lime.utils.Assets;

import mme.core.Material;


class TexturedPhongMaterial extends Material {

    public function new( imageLocation : String, shininess : Float = 32.0 ) {

        super();

        emissiveColor = 0x000000ff;

		textureImage = Assets.getImage( imageLocation );

        this.ka = [1.0, 1.0, 1.0];
        this.kd = [1.0, 1.0, 1.0];
        this.ks = [0.5, 0.5, 0.5];

        this.shininess = shininess;
    }
}
