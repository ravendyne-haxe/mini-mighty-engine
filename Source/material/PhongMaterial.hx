package mme.material;

import lime.math.RGBA;

import mme.core.Material;
import mme.core.RGBAColor;


class PhongMaterial extends Material {

    public function new( color : RGBA = 0xff00ffff, shininess : Float = 32.0 ) {

        super();

        emissiveColor = 0x000000ff;

        var colorr : RGBAColor = color;

        this.ka = colorr;
        this.kd = colorr;
        this.ks = [0.5, 0.5, 0.5];

        this.shininess = shininess;
    }
}
