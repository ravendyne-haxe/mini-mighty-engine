package mme.material;

import lime.graphics.Image;

import lime.utils.Assets;

import mme.core.Material;

class TextureMaterial extends Material {

    public function new( imageLocation : String ) {

        super();

		textureImage = Assets.getImage( imageLocation );
    }
}
