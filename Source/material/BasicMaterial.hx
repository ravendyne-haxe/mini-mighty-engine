package mme.material;

import lime.graphics.Image;

import lime.math.RGBA;

import mme.core.Material;


class BasicMaterial extends Material {

    public function new( color : RGBA = 0xFF00FFFF ) {

        super();

        emissiveColor = color;
    }
}
