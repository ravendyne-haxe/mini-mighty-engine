package mme.util.loader;

// import lime.utils.Log;
import mme.core.SceneNode;
import mme.core.Group;
import mme.material.NamedColor;
import mme.core.Material;
import mme.material.BasicMaterial;
import mme.geometry.MeshGeometry;
import haxe.io.Bytes;

import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;

import mme.core.Mesh;


class DataProcessorImpl {

    public var vertices : Array< Array<Float> >;
    public var uvs : Array< Array<Float> >;
    public var normals : Array< Array<Float> >;
    public var faces : Array< Array< Array<Null<Int>> > >;

    public function new() {

        vertices = [];
        uvs = [];
        normals = [];
        faces = [];
    }

    public function buildMeshForGroup() : Mesh {

        var mesh = new Mesh();

        for( face in faces ) {

            // ObjParser guarantees face will have 3 elements
            var p0 = face[ 0 ];
            var p1 = face[ 1 ];
            var p2 = face[ 2 ];

            createTriangle( p0, p1, p2, mesh );

            // assume convex planar polygons
            var pe = p2;
            for( i in 3...face.length ) {
                var pt = face[ i ];
                createTriangle( p0, pe, pt, mesh );
                pe = pt;
            }
        }

        // we have to reset faces every time we build a mesh
        // in order to prepare for the next group to process
        faces = [];

        return mesh;
    }

    function createTriangle(
        p0 : Array<Null<Int>>,
        p1 : Array<Null<Int>>,
        p2 : Array<Null<Int>>,
        mesh : Mesh
    ) {

        //
        // VERTICES
        //

        // ObjParser guarantees there will always be vertex index present
        var v0_idx = p0[ 0 ] - 1;
        var v1_idx = p1[ 0 ] - 1;
        var v2_idx = p2[ 0 ] - 1;

        mesh.vertices.push( vertices[ v0_idx ].copy() );
        mesh.vertices.push( vertices[ v1_idx ].copy() );
        mesh.vertices.push( vertices[ v2_idx ].copy() );

        //
        // UVs
        //

        var vt0_idx = p0[ 1 ];
        var vt1_idx = p1[ 1 ];
        var vt2_idx = p2[ 1 ];

        if( vt0_idx != null && vt1_idx != null && vt2_idx != null ) {

            mesh.uvs.push( uvs[ vt0_idx - 1 ].copy() );
            mesh.uvs.push( uvs[ vt1_idx - 1 ].copy() );
            mesh.uvs.push( uvs[ vt2_idx - 1 ].copy() );

        } else {

            mesh.uvs.push( [ 0.0, 0.0 ] );
            mesh.uvs.push( [ 0.0, 0.0 ] );
            mesh.uvs.push( [ 0.0, 0.0 ] );
        }

        //
        // NORMALS
        //

        var vn0_idx = p0[ 2 ];
        var vn1_idx = p1[ 2 ];
        var vn2_idx = p2[ 2 ];

        if( vn0_idx != null && vn1_idx != null && vn2_idx != null ) {

            mesh.normals.push( normals[ vn0_idx - 1 ].copy() );
            mesh.normals.push( normals[ vn1_idx - 1 ].copy() );
            mesh.normals.push( normals[ vn2_idx - 1 ].copy() );

        } else {

            // assume side_A = v1 - v0, side_B = v2 - v1
            // and calculate N = side_A x side_B
            var v0 = vertices[ v0_idx ];
            var v0_ : Vec3 = [ v0[0], v0[1], v0[2] ];
            var v1 = vertices[ v1_idx ];
            var v1_ : Vec3= [ v1[0], v1[1], v1[2] ];
            var v2 = vertices[ v2_idx ];
            var v2_ : Vec3 = [ v2[0], v2[1], v2[2] ];

            var side_A = v1_.subtract( v0_ );
            var side_B = v2_.subtract( v1_ );

            var normal = side_A.cross( side_B );
            normal.normalize( normal );

            var vn = [ normal.x, normal.y, normal.z ];

            mesh.normals.push( vn.copy() );
            mesh.normals.push( vn.copy() );
            mesh.normals.push( vn.copy() );
        }
    }

    public function processVertex( v : Array< Float > ) : Void {

        vertices.push( v );
    }

    public function processVertexTexture( vt : Array< Float > ) : Void {

        uvs.push( vt );
    }

    public function processVertexNormal( vn : Array< Float > ) : Void {

        normals.push( vn );
    }

    public function processFace( f : Array< Array< Null< Int > > > ) : Void {

        faces.push( f );
    }
}


class WaveObjLoader {

    var mtlLoader : mme.util.loader.WaveMtlLoader;
    var resolver : String -> Bytes;
    public var materials : Map< String, Material >;
    var defaultMaterial : Material;


    public function new() {
    }

    public function loadAsMesh( modelName : String, resolver : String -> Bytes ) : Mesh {

        this.resolver = null;
        mtlLoader = null;

        var content = resolver( modelName );
        var i = new haxe.io.BytesInput( content );
        var obj = new mme.format.obj.Reader( i ).read();

        this.resolver = resolver;
        var processor = new DataProcessorImpl();
        materials = [];


        for( d in obj ) {
            switch( d ) {

                case CVertex( v ):
                    processor.processVertex( [ v.x, v.y, v.z, v.w ] );

                case CNormal( vn ):
                    processor.processVertexNormal( [ vn.i, vn.j, vn.k ] );

                case CTextureUV( vt ):
                    processor.processVertexTexture( [ vt.u, vt.v, vt.w ] );

                case CFace( f ):
                    var face = [
                        [ f[0].vertexIndex, f[0].uvIndex, f[0].normalIndex ],
                        [ f[1].vertexIndex, f[1].uvIndex, f[1].normalIndex ],
                        [ f[2].vertexIndex, f[2].uvIndex, f[2].normalIndex ],
                    ];

                    for( i in 3...f.length ) {
                        face.push(
                            [ f[i].vertexIndex, f[i].uvIndex, f[i].normalIndex ]
                        );
                    }

                    processor.processFace( face );

                // case CGroup( g ):
                
                // case CMtlLib( mtllib ):
                
                // case CMaterial( usemtl ):

                // case CObject( o ):

                // case CUnknown( line ):

                default:
            }
        }

        return processor.buildMeshForGroup();
    }

    public function load( modelName : String, resolver : String -> Bytes, ?defaultMaterial : Material ) : SceneNode {

        this.resolver = null;
        mtlLoader = null;

        var content = resolver( modelName );
        var i = new haxe.io.BytesInput( content );
        var obj = new mme.format.obj.Reader( i ).read();

        this.resolver = resolver;
        mtlLoader = new mme.util.loader.WaveMtlLoader();
        var processor = new DataProcessorImpl();
        materials = [];
        if( defaultMaterial != null ) {
            this.defaultMaterial = defaultMaterial;
        } else {
            this.defaultMaterial = new BasicMaterial( NamedColor.fuchsia );
        }

        var currentGroupName = "#nogroup";
        var currentMaterialName = "n/a";

        var sceneObject = new Group();

        for( d in obj ) {
            switch( d ) {

                case CVertex( v ):
                    processor.processVertex( [ v.x, v.y, v.z, v.w ] );

                case CNormal( vn ):
                    processor.processVertexNormal( [ vn.i, vn.j, vn.k ] );

                case CTextureUV( vt ):
                    processor.processVertexTexture( [ vt.u, vt.v, vt.w ] );

                case CFace( f ):
                    var face = [
                        [ f[0].vertexIndex, f[0].uvIndex, f[0].normalIndex ],
                        [ f[1].vertexIndex, f[1].uvIndex, f[1].normalIndex ],
                        [ f[2].vertexIndex, f[2].uvIndex, f[2].normalIndex ],
                    ];

                    for( i in 3...f.length ) {
                        face.push(
                            [ f[i].vertexIndex, f[i].uvIndex, f[i].normalIndex ]
                        );
                    }

                    processor.processFace( face );

                case CGroup( g ):
                    // 'g' means we have to create new mesh with its material
                    // Log.info( 'group(s): $g' );
                    var group = processGroup( currentGroupName, currentMaterialName, processor );
                    if( group != null )
                        sceneObject.add( group );
                    // TODO figure out what to do with multiple group names
                    currentGroupName = g[0];
                
                case CMtlLib( mtllib ):
                    loadMaterials( mtllib );
                
                case CMaterial( usemtl ):
                    // 'usemtl' means we have to create new mesh with its material
                    // Log.info( 'use material: "$usemtl"' );
                    var group = processGroup( currentGroupName, currentMaterialName, processor );
                    if( group != null )
                        sceneObject.add( group );
                    currentMaterialName = usemtl;

                case CObject( o ):
                    // TODO add support for multiple objects
                    // treat object directive as another group
                    // we assume one object per file
                    // if( currentGroupName != null ) {
                    //     var group = processGroup( currentGroupName, currentMaterialName, processor );
                    //     sceneObject.add( group );
                    // }
                    // currentGroupName = o;

                // case CUnknown( line ):

                default:
            }
        }

        var group = processGroup( currentGroupName, currentMaterialName, processor );
        if( group != null )
            sceneObject.add( group );

        return sceneObject;
    }

    function processGroup( groupName : String, materialName : String, processor : DataProcessorImpl ) : SceneNode {

        // Log.info( 'group "$groupName" with material "$materialName"' );

        // var sceneObjectName = groupName + ":" + materialName;
        var mesh = processor.buildMeshForGroup();
        if( mesh.vertices.length == 0 ) {
            // empty group
            // Log.info( 'group "$groupName" is empty' );
            return null;
        }

        var material = defaultMaterial;
        if( materials.exists( materialName ) ) {
            material = materials[ materialName ];
        }
        // Log.info( 'material for name "$materialName" is $material' );

        var sceneObject = new MeshGeometry( mesh, material );

        return sceneObject;
    }

    function loadMaterials( libraryNames : Array<String> ) {

        for( libname in libraryNames ) {

            // Log.info( 'load material library "$libname"' );

            var libcontent = resolver( libname );
            var libmaterials = mtlLoader.load( libcontent );

            for( mat in libmaterials.keys() ) {
                // TODO make sure on how OBJ treats material override: first or last wins?
                if( ! materials.exists( mat ) ) {
                    // "first wins" strategy
                    materials[ mat ] = libmaterials[ mat ];
                    // Log.info( 'add material "$mat" to library' );
                }
            }
        }

    }
}
