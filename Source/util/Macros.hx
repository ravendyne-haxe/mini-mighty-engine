package mme.util;

#if macro
import haxe.io.Path;
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import sys.FileSystem;
import sys.io.File;
import mme.math.Hash;
#end

class Macros {

    public static macro function classId() : ExprOf< Int > {

        // get the current class information. 
        var ref : ClassType = Context.getLocalClass().get();

        // hash the class name
        var hash = Hash.fnvHash32( ref.name );

        return macro $v{hash};
    }

    public static macro function getContentAsString( name : String ) : ExprOf< String > {

        // get the current fields of the class
        var fields : Array< Field > = Context.getBuildFields();

        // get the path of the current current class file, e.g. "src/path/to/MyClassName.hx"
        var posInfos = Context.getPosInfos( Context.currentPos() );
        var directory = Path.directory( posInfos.file );

        // get the current class information. 
        var ref : ClassType = Context.getLocalClass().get();

        var contentFilePath : String = Path.join([ directory, name ]);

        // detect if template file exists
        if( ! FileSystem.exists( contentFilePath ) ) {
            return null;
        }

        // get the file content of the template 
        var content : String = File.getContent( contentFilePath );

        // return as expression
        return macro $v{content};
    }

    public static macro function getVertexShaderSource() : ExprOf< String > {
        // return getStringContent( ".vert" );

        // get the current fields of the class
        var fields : Array< Field > = Context.getBuildFields();

        // get the path of the current current class file, e.g. "src/path/to/MyClassName.hx"
        var posInfos = Context.getPosInfos( Context.currentPos() );
        var directory = Path.directory( posInfos.file );

        // get the current class information. 
        var ref : ClassType = Context.getLocalClass().get();

        var contentFilePath : String = Path.join([ directory, ref.name + ".vert" ]);

        // detect if template file exists
        if( ! FileSystem.exists( contentFilePath ) ) {
            return null;
        }

        // get the file content of the template 
        var content : String = File.getContent( contentFilePath );

        // return as expression
        return macro $v{content};
    }

    public static macro function getFragmentShaderSource() : ExprOf< String > {
        // return getStringContent( ".frag" );

        // get the current fields of the class
        var fields : Array< Field > = Context.getBuildFields();

        // get the path of the current current class file, e.g. "src/path/to/MyClassName.hx"
        var posInfos = Context.getPosInfos( Context.currentPos() );
        var directory = Path.directory( posInfos.file );

        // get the current class information. 
        var ref : ClassType = Context.getLocalClass().get();

        var contentFilePath : String = Path.join([ directory, ref.name + ".frag" ]);

        // detect if template file exists
        if( ! FileSystem.exists( contentFilePath ) ) {
            return null;
        }

        // get the file content of the template 
        var content : String = File.getContent( contentFilePath );

        // return as expression
        return macro $v{content};
    }
}
