package mme.xgl.material;

import lime.graphics.WebGLRenderContext;

import mme.g2d.material.Point2DMaterial;

import mme.core.RGBAColor;

import mme.xgl.engine.XGLMaterial;


class XGLPoint2DMaterial extends XGLMaterial {

    private var objectMaterial : Point2DMaterial;

    public var edgeColor ( get, null ) : RGBAColor;

	public function new( gl : WebGLRenderContext, material : Point2DMaterial ) {

		super( gl, material );

        objectMaterial = material;
	}

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        objectMaterial = null;
    }

    private function get_edgeColor() : RGBAColor {

	    return objectMaterial.edgeColor;
    }
}
