package mme.xgl.material;

import lime.graphics.WebGLRenderContext;

import mme.material.WireframeMaterial;

import mme.xgl.engine.XGLMaterial;


class XGLWireframeMaterial extends XGLMaterial {

    private var objectMaterial : WireframeMaterial;

	public function new( gl : WebGLRenderContext, material : WireframeMaterial ) {

		super( gl, material );

        objectMaterial = material;
	}

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        objectMaterial = null;
    }
}
