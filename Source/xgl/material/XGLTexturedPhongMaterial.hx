package mme.xgl.material;

import lime.graphics.WebGLRenderContext;

import mme.math.glmatrix.Vec3;

import mme.material.TexturedPhongMaterial;

import mme.xgl.engine.XGLMaterial;
import mme.xgl.engine.XGLTexture;


class XGLTexturedPhongMaterial extends XGLMaterial {

    private var objectMaterial : TexturedPhongMaterial;

    /** Material ambient light reflectance **/
    public var ka ( get, null ) : Vec3;
    /** Material diffuse light reflectance **/
    public var kd ( get, null ) : Vec3;
    /** Material specular light reflectance **/
    public var ks ( get, null ) : Vec3;

    public var shininess ( get, null ) : Float;

	public var textureImageLocation : String;

    /** Texture object containing uploaded texture handle and other data **/
	public var texture : XGLTexture;

	public function new( gl : WebGLRenderContext, material : TexturedPhongMaterial ) {

		super( gl, material );

        objectMaterial = material;

		texture = new XGLTexture( gl, objectMaterial.textureImage );
	}

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );


        texture.dispose( gl );
        texture = null;

        textureImageLocation = null;
        objectMaterial = null;
    }

    private function get_ka() : Vec3 {

        return objectMaterial.ka;
    }

    private function get_kd() : Vec3 {

        return objectMaterial.kd;
    }

    private function get_ks() : Vec3 {

        return objectMaterial.ks;
    }

    private function get_shininess() : Float {

        return objectMaterial.shininess;
    }
}
