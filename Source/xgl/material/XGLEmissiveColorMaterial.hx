package mme.xgl.material;

import lime.graphics.WebGLRenderContext;

import mme.material.BasicMaterial;

import mme.xgl.engine.XGLMaterial;


class XGLEmissiveColorMaterial extends XGLMaterial {

    private var objectMaterial : BasicMaterial;

	public function new( gl : WebGLRenderContext, material : BasicMaterial ) {

		super( gl, material );

        objectMaterial = material;
	}

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        objectMaterial = null;
    }
}
