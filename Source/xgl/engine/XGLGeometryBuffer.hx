package mme.xgl.engine;

import lime.graphics.WebGLRenderContext;
import lime.graphics.opengl.GL;

import mme.xgl.engine.XGLArrayBuffer;
import mme.xgl.engine.XGLShader;
import mme.xgl.geometry.XGLMeshConverter;


class XGLGeometryBuffer {

	public var buffer : XGLArrayBuffer;
	public var elementBuffer : XGLElementArrayBuffer;
    public var shader : XGLShader;

    private var meshConverter : XGLMeshConverter;

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLMeshConverter, attributeSize : Int ) {

        this.shader = shader;
        this.meshConverter = meshConverter;

        elementBuffer = null;

        meshConverter.convert();
        var usage = meshConverter.getUsage();

        if( meshConverter.isIndexed ) {
            elementBuffer = new XGLElementArrayBuffer( gl, usage );
            elementBuffer.uploadElementBuffer( gl, meshConverter.indicesBuffer );
        }

		buffer = new XGLArrayBuffer( gl, attributeSize, usage );
        buffer.uploadVertexBuffer( gl, meshConverter.attributeBuffer );
    }

    public function update( gl : WebGLRenderContext ) {

        meshConverter.convert();

        if( meshConverter.isIndexed ) {
            elementBuffer.uploadElementBuffer( gl, meshConverter.indicesBuffer );
        }

        buffer.uploadVertexBuffer( gl, meshConverter.attributeBuffer );
    }

    public function dispose( gl : WebGLRenderContext ) {

        buffer.dispose( gl );
        buffer = null;
        if( elementBuffer != null )  elementBuffer.dispose( gl );
        elementBuffer = null;
        shader.dispose( gl );
        shader = null;
        meshConverter.dispose();
        meshConverter = null;
    }

    public function describeProgramAttributes( gl : WebGLRenderContext ) : Void {

        for( attrName in buffer.attributeParams.keys() ) {

            gl.vertexAttribPointer(
                shader.program.attributeHandles[ attrName ],
                buffer.attributeParams[ attrName ].componentCount,
                GL.FLOAT,
                false,
                buffer.vertexAttributesSize,
                buffer.attributeParams[ attrName ].attributeOffset
            );
        }
    }
}
