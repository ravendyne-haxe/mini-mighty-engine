package mme.xgl.engine;

import lime.graphics.opengl.GL;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GLProgram;
import lime.graphics.opengl.GLUniformLocation;


class XGLShaderProgram {

	public var attributeHandles : Map< String, Int > = [];
	public var uniformHandles : Map< String, GLUniformLocation > = [];

	public var programHandle : GLProgram;

	public function new( gl : WebGLRenderContext, vertexSource : String, fragmentSource : String ) {

		programHandle = GLProgram.fromSources( gl, vertexSource, fragmentSource );
        var attributeNames = enumerateAttributes( gl );
        var uniformNames = enumerateUniforms( gl );

		for( attrName in attributeNames ) {

			attributeHandles[ attrName ] = gl.getAttribLocation( programHandle, attrName );
		}

		for( uniName in uniformNames ) {

			uniformHandles[ uniName ] = gl.getUniformLocation( programHandle, uniName );
		}
	}

    public function dispose( gl : WebGLRenderContext ) {

        attributeHandles = [];
        uniformHandles = [];

        gl.deleteProgram( programHandle );
        programHandle = null;
    }

    function enumerateAttributes( gl : WebGLRenderContext ) : Array<String> {

        /*
        http://docs.gl/es2/glGetActiveAttrib
        GLActiveInfo.type value for OpenGL ES2:
        GL_FLOAT, GL_FLOAT_VEC2, GL_FLOAT_VEC3, GL_FLOAT_VEC4, GL_FLOAT_MAT2, GL_FLOAT_MAT3, or GL_FLOAT_MAT4

        GLActiveInfo.type value for OpenGL ES3
        see a list here: http://docs.gl/es3/glGetActiveAttrib
        */

        var activeAttributeCount = gl.getProgramParameter( programHandle, GL.ACTIVE_ATTRIBUTES );

        var attributeNames = [];

        for( idx in 0...activeAttributeCount ) {
            var attrInfo = gl.getActiveAttrib( programHandle, idx );
            attributeNames.push( attrInfo.name );
            // lime.utils.Log.info( 'attr ${attrInfo}' );
        }

        return attributeNames;
    }

    function enumerateUniforms( gl : WebGLRenderContext ) : Array<String> {

        /*
        http://docs.gl/es2/glGetActiveUniform
        GLActiveInfo.type value for OpenGL ES2:
        GL_FLOAT, GL_FLOAT_VEC2, GL_FLOAT_VEC3, GL_FLOAT_VEC4, 
        GL_INT, GL_INT_VEC2, GL_INT_VEC3, GL_INT_VEC4, 
        GL_BOOL, GL_BOOL_VEC2, GL_BOOL_VEC3, GL_BOOL_VEC4, 
        GL_FLOAT_MAT2, GL_FLOAT_MAT3, GL_FLOAT_MAT4, 
        GL_SAMPLER_2D, or GL_SAMPLER_CUBE

        GLActiveInfo.type value for OpenGL ES3
        see a list here: http://docs.gl/es3/glGetActiveUniform
        */

        var activeUniformCount = gl.getProgramParameter( programHandle, GL.ACTIVE_UNIFORMS );

        var uniformNames = [];

        for( idx in 0...activeUniformCount ) {
            var uniInfo = gl.getActiveUniform( programHandle, idx );
            uniformNames.push( uniInfo.name );
            // lime.utils.Log.info( 'uni ${uniInfo}' );
        }

        return uniformNames;
    }
}
