package mme.xgl.engine;

import lime.graphics.WebGLRenderContext;

import mme.core.Scene;

import mme.xgl.engine.XGLShaderProgram;


class XGLShader {

    public var program : XGLShaderProgram;

    public function new( gl : WebGLRenderContext ) {

        createShader( gl );
    }

    public function dispose( gl : WebGLRenderContext ) {

        program.dispose( gl );
        program = null;
    }

    public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) { }

    public function renderCommonProgramCleanup( gl : WebGLRenderContext, scene : Scene ) { }

    private function createShader( gl : WebGLRenderContext ) { }
}
