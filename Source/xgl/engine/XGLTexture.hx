package mme.xgl.engine;

import lime.graphics.WebGLRenderContext;
import lime.graphics.Image;
import lime.graphics.PixelFormat;

import lime.graphics.opengl.GL;
import lime.graphics.opengl.GLTexture;


class XGLTexture {

    public var textureUnit : Int = 1;
    public var textureHandle : GLTexture;

    var mipMap : Bool;
    var textureWidth : Int;
    var textureHeight : Int;

    public function new( gl : WebGLRenderContext, image : Image ) {

        mipMap = false;

        textureWidth = image.width;
        textureHeight = image.height;
        initTexture( gl );

	    uploadTextureBuffer( gl, image );
	}

    public function dispose( gl : WebGLRenderContext ) {

        textureUnit = -1;
        gl.deleteTexture( textureHandle );
        textureHandle = null;
    }

    private function initTexture( gl : WebGLRenderContext ) {

        textureHandle = gl.createTexture();

	    gl.bindTexture( GL.TEXTURE_2D, textureHandle );

        // when scaling up
        // use GL2ES2.GL_LINEAR or GL2ES2.GL_NEAREST_MIPMAP_LINEAR for pixelated look
		gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, mipMap ? GL.LINEAR_MIPMAP_LINEAR : GL.LINEAR ); // GL.NEAREST
        // when scaling down
        // use GL2ES2.GL_LINEAR or GL2ES2.GL_NEAREST_MIPMAP_LINEAR for pixelated look
		gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, mipMap ? GL.LINEAR_MIPMAP_LINEAR : GL.LINEAR ); // GL.NEAREST

		gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE );
		gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE );

        // allocate space only
        gl.texImage2D(
            GL.TEXTURE_2D, 
            0, // base image level. if > 0 it references n-th mipmap reduction level
            GL.RGBA, // 'internalFormat': ask OpenGL how to store texture internally. We asked for an array of uncompressed RGBA 32-bit values
            textureWidth, 
            textureHeight, 
            0, // border width: 0 or 1.
            GL.RGBA, // 'format': format for one pixel in our texture data we are uploading to GPU
            GL.UNSIGNED_BYTE, // 'type': type of one element of one pixel data. We said each one of R, G, B and A components is represented by an unsigned byte value.
            null // actual data of the image we want to upload to GPU (lime's UInt8Array)
        );

	    gl.bindTexture( GL.TEXTURE_2D, null );
    }

    // https://open.gl/textures
    public function uploadTextureBuffer( gl : WebGLRenderContext, image : Image ) : Void {

        if( image.width != textureWidth && image.height != textureHeight ) {
            throw "XGLTexture: can only upload texture of the same size";
        }

        if( image.format != PixelFormat.RGBA32 ) {
            // this converts image to RGBA32
            image.format = PixelFormat.RGBA32;
        }

	    gl.bindTexture( GL.TEXTURE_2D, textureHandle );

        // This effectively uploads the texture data (pixels) to GPU
        gl.texImage2D(
            GL.TEXTURE_2D, 
            0, // base image level. if > 0 it references n-th mipmap reduction level
            GL.RGBA, // 'internalFormat': ask OpenGL how to store texture internally. We asked for an array of uncompressed RGBA 32-bit values
            textureWidth, 
            textureHeight, 
            0, // border width: 0 or 1.
            GL.RGBA, // 'format': format for one pixel in our texture data we are uploading to GPU
            GL.UNSIGNED_BYTE, // 'type': type of one element of one pixel data. We said each one of R, G, B and A components is represented by an unsigned byte value.
            image.data // actual data of the image we want to upload to GPU (lime's UInt8Array)
        );

        // generate mipmaps AFTER image data has been loaded to GPU
        if( mipMap ) {
            // we will use OpenGL to generate mipmap levels
            // we could have specified each level ourselves by calling
            // gl.texImage2D() for each level and specifying the level as second parameter
            gl.generateMipmap( GL.TEXTURE_2D );
        }

        // when done, unbind
	    gl.bindTexture( GL.TEXTURE_2D, null );
	}
}
