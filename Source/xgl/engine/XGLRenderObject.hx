package mme.xgl.engine;

import lime.graphics.WebGLRenderContext;

import mme.core.Mesh;
import mme.core.Material;
import mme.core.Scene;
import mme.core.SceneObject;


class XGLRenderObject {

	private var sceneObject : SceneObject;
    /** A shortcut for `sceneObject.mesh` **/
    public var mesh : Mesh;
    /** A shortcut for `sceneObject.material **/
    public var meshMaterial : Material;

    public var material : XGLMaterial;
    public var geometryBuffer : XGLGeometryBuffer;
    public var shader : XGLShader;

	public function new( gl : WebGLRenderContext, sceneObject : SceneObject ) {

		this.sceneObject = sceneObject;

        mesh = sceneObject.mesh;
		meshMaterial = sceneObject.material;

		create( gl );
	}

    public function dispose( gl : WebGLRenderContext ) {

        sceneObject = null;
        mesh = null;
        meshMaterial = null;

        material.dispose( gl );
        material = null;
        geometryBuffer.dispose( gl );
        geometryBuffer = null;
        shader.dispose( gl );
        shader = null;
    }

	private function create( gl : WebGLRenderContext ) : Void { }

    public function update( gl : WebGLRenderContext ) : Void {

        geometryBuffer.update( gl );
    }

	public function render( gl : WebGLRenderContext, scene : Scene ) : Void {

		//
		// --- SETUP
		//


		//
		// --- RENDER
		//
		renderSetupGeometry( gl, scene );
		renderSetupMaterial( gl, scene );
		renderDraw( gl );


		//
		// --- CLEANUP
		//

	}

	private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void { }

	private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void { }
	
	private function renderDraw( gl : WebGLRenderContext ) : Void { }
}
