package mme.xgl.engine;

import lime.graphics.WebGLRenderContext;

import mme.core.RGBAColor;

import mme.core.Material;


class XGLMaterial {

    /** `Material` instance this `XGLMaterial` represents **/
    private var material : Material;

	/** Material base color, a.k.a. emissive color, a.k.a. Ke **/
    public var color ( get, null ) : RGBAColor;

	// default emissive color: fuchsia
    public function new( gl : WebGLRenderContext, material : Material ) {

        this.material = material;
	}

    public function dispose( gl : WebGLRenderContext ) {

        material = null;
    }

    private function get_color() : RGBAColor {

	    return material.emissiveColor;
    }
}
