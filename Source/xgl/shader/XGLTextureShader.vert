attribute vec3 position;
attribute vec2 aTextureUV;

uniform mat4 u_TransformMatrix;

varying vec2 v_TextureUV;

void main( void ) {
    v_TextureUV = aTextureUV;
    gl_Position = u_TransformMatrix * vec4(position, 1.0);
}
