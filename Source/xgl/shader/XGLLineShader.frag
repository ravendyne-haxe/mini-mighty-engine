// #if (!desktop || rpi)
#ifdef GL_ES
precision mediump float;
#endif
// #end

uniform vec3 u_LineColor;
uniform vec3 u_EdgeColor;
uniform float u_Inner;
uniform float u_BlendStart;

void main() {

    gl_FragColor = vec4( u_LineColor, 1.0 );
}
