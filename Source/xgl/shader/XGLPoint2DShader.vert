attribute vec3 position;
attribute vec2 aTextureUV;

uniform mat4 u_TransformMatrix;

varying vec2 v_TextureUV;

void main() {

    v_TextureUV = aTextureUV;

    gl_Position = u_TransformMatrix * vec4( position.xy, 0.0, 1.0 );
}
