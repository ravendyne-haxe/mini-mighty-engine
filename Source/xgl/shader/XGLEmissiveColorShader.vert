attribute vec3 position;

uniform mat4 u_TransformMatrix;

void main (void) {
    gl_Position = u_TransformMatrix * vec4(position, 1.0);
}
