package mme.xgl.shader;

import lime.graphics.WebGLRenderContext;

import mme.core.Scene;

import mme.xgl.engine.XGLShaderProgram;
import mme.xgl.engine.XGLShader;

import mme.util.Macros;


private class XGLPoint2DShader extends XGLShader {

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSource = Macros.getVertexShaderSource();

        var fragmentShaderSourceCircle = Macros.getFragmentShaderSource();

        program = new XGLShaderProgram( gl,
            vertexShaderSource,
            fragmentShaderSourceCircle
            // ,[ "position", "aTextureUV" ],
            // [ 
            //     "transformMatrix", "type",
            //     "pointColor", "edgeColor",
            // ]
        );
    }

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        gl.enable(gl.BLEND);
        gl.blendEquation(gl.FUNC_ADD);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    }

}

class XGLPoint2DCircleShader extends XGLPoint2DShader {

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        super.renderCommonProgramSetup( gl, scene );

        gl.uniform1i( program.uniformHandles['u_Type'], 1 );
    }
}

class XGLPoint2DSquareShader extends XGLPoint2DShader {

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        super.renderCommonProgramSetup( gl, scene );

        gl.uniform1i( program.uniformHandles['u_Type'], 2 );
    }
}
