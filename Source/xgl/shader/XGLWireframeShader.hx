package mme.xgl.shader;

import mme.core.Scene;
import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLShaderProgram;

import mme.xgl.engine.XGLShader;

import mme.util.Macros;


class XGLWireframeShader extends XGLShader {

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSourceA = Macros.getVertexShaderSource();

        var fragmentShaderSourceA =  Macros.getFragmentShaderSource();

        #if html5
        if( gl.getExtension("OES_standard_derivatives") == null ) {
            // throw "OES_standard_derivatives extension not supported: required by XGLWireframeShader";
            lime.utils.Log.warn("OES_standard_derivatives extension not supported: required by XGLWireframeShader. Fallback used: 'ugly' wireframe.");
        }
        #end
        // var exts = gl.getSupportedExtensions();
        // lime.utils.Log.info('exts = $exts');

        program = new XGLShaderProgram( gl, vertexShaderSourceA, fragmentShaderSourceA
        // , [ "position", "barycentric" ], [ "transformMatrix", "emissiveColor" ]
        );
    }
}
