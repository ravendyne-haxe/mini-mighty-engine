attribute vec3 prevPosition;
attribute vec3 curPosition;
attribute vec3 nextPosition;
attribute float direction; 

uniform mat4 u_TransformMatrix;
uniform float u_Thickness;
// uniform float aspect;
const float aspect = 1.0;

// do miter or do not, there is no try
// uniform int miter;
const int do_miter = 1;

void main() {

    vec2 aspectVec = vec2( aspect, 1.0 );

    vec4 previousProjected = u_TransformMatrix * vec4( prevPosition, 1.0 );
    vec4 currentProjected = u_TransformMatrix * vec4( curPosition, 1.0 );
    vec4 nextProjected = u_TransformMatrix * vec4( nextPosition, 1.0 );

    // get 2D screen space with W divide and aspect correction
    vec2 currentScreen = currentProjected.xy / currentProjected.w * aspectVec;
    vec2 previousScreen = previousProjected.xy / previousProjected.w * aspectVec;
    vec2 nextScreen = nextProjected.xy / nextProjected.w * aspectVec;

    float len = u_Thickness;
    float orientation = direction;

    //starting point uses (nextPosition - current)
    vec2 dir = vec2(0.0);
    if( currentScreen == previousScreen ) {
        dir = normalize( nextScreen - currentScreen );
    } 
    //ending point uses (current - prevPosition)
    else if( currentScreen == nextScreen ) {
        dir = normalize( currentScreen - previousScreen );
    }
    //somewhere in middle, needs a join
    else {
        //get directions from (C - B) and (B - A)
        vec2 dirA = normalize((currentScreen - previousScreen));

        if( do_miter == 1 ) {

            vec2 dirB = normalize((nextScreen - currentScreen));
            //now compute the miter join normal and length
            vec2 tangent = normalize( dirA + dirB );
            vec2 perp = vec2( -dirA.y, dirA.x );
            vec2 miter = vec2( -tangent.y, tangent.x );
            dir = tangent;
            len = u_Thickness / dot( miter, perp );

        } else {

            dir = dirA;
        }
    }
    vec2 normal = vec2( -dir.y, dir.x );
    normal *= len/2.0;
    normal.x /= aspect;

    vec4 offset = vec4( normal * orientation, 0.0, 0.0 );
    gl_Position = currentProjected + offset;
    gl_PointSize = 1.0;
}
