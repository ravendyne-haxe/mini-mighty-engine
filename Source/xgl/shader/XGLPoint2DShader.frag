// #if (!desktop || rpi)
#ifdef GL_ES
precision mediump float;
#endif
// #end

uniform vec3 u_PointColor;
uniform vec3 u_EdgeColor;
uniform int u_Type;

varying vec2 v_TextureUV;

vec4 circle() {
    float pct = 0.0;

    // distance from center: (0.5, 0.5)
    pct = distance( v_TextureUV, vec2( 0.5 ) );
    
    // inner circle edge
    float stp1 = step( 0.4, pct );
    // outer circle edge, inverted
    float stp2 = step( 0.5, 1.0 - pct );
    // with step(), this is same as logical AND
    pct = stp1 * stp2;

    // pct is 0 or 1 so mix will show
    // u_PointColor for 0 and u_EdgeColor for 1
    vec3 vmix = mix( u_PointColor, u_EdgeColor, pct );

    // use stp2 as alpha to mask out corners
    return vec4( vmix, stp2 );
}
vec4 square() {
    // bottom-left
    vec2 bl = step( vec2( 0.1 ), v_TextureUV );
    // top-right, it's just bottom-left inverted
    vec2 tr = step( vec2( 0.1 ), 1.0 - v_TextureUV );
    // with step(), this is same as logical AND
    float pct = bl.x * bl.y * tr.x * tr.y;

    // pct is 0 or 1 so mix will show
    // u_EdgeColor for 0 and u_PointColor for 1
    vec3 vmix = mix( u_EdgeColor, u_PointColor, pct );

    return vec4( vmix, 1.0 );

    // return vec4( v_TextureUV, 0.0, 1.0 );
}

void main() {
    vec4 color = vec4( 1.0 );

    if( u_Type == 1 )
        color = circle();
    if( u_Type == 2 )
        color = square();

    gl_FragColor = color;
}
