attribute vec3 position;
attribute vec3 normal;
attribute vec2 aTextureUV;

uniform mat4 u_TransformMatrix;
uniform mat4 u_ProjectionMatrix;
uniform mat3 u_NormalTransform;

varying vec3 v_Normal;
varying vec3 v_ModelPosition;
varying vec2 v_TextureUV;

void main (void) {

    v_TextureUV = aTextureUV;

    vec4 modelPosition = u_TransformMatrix * vec4( position, 1.0 );
    v_Normal = u_NormalTransform * normal;

    v_ModelPosition = vec3( modelPosition );
    gl_Position = u_ProjectionMatrix * modelPosition;
}
