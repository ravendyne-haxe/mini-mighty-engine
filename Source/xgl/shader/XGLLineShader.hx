package mme.xgl.shader;

import lime.graphics.WebGLRenderContext;

import mme.core.Scene;

import mme.xgl.engine.XGLShaderProgram;
import mme.xgl.engine.XGLShader;

import mme.util.Macros;


class XGLLineShader extends XGLShader {

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSourceA = Macros.getVertexShaderSource();

        var fragmentShaderSourceA = Macros.getFragmentShaderSource();

        program = new XGLShaderProgram( gl,
            vertexShaderSourceA,
            fragmentShaderSourceA
            // ,[ "prevPosition", "curPosition", "nextPosition", "direction" ],
            // [ 
            //     "transformMatrix", "thickness",
            //     "lineColor", "edgeColor", "inner",
            //     "blend_start",
            // ]
        );
    }

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        gl.enable(gl.BLEND);
        gl.blendEquation(gl.FUNC_ADD);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    }

}
