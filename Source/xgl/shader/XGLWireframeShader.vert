attribute vec3 position;
attribute vec3 barycentric;

uniform mat4 u_TransformMatrix;

varying vec3 v_VertexEdgeParameter;

void main (void) {
    v_VertexEdgeParameter = barycentric;
    gl_Position = u_TransformMatrix * vec4(position, 1.0);
}
