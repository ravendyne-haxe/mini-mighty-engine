attribute vec2 position;
attribute vec2 normal;
attribute float miter; 

uniform mat4 u_TransformMatrix;
uniform float u_Thickness;

varying float v_Edge;

void main() {

    v_Edge = sign( miter );
    vec2 pointPos = position + vec2( normal * u_Thickness / 2.0 * miter );

    gl_Position = u_TransformMatrix * vec4( pointPos, 0.0, 1.0 );
    // gl_PointSize = 1.0;
}
