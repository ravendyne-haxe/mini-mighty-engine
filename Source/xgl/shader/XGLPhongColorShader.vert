attribute vec3 position;
attribute vec3 normal;

uniform mat4 u_TransformMatrix;
uniform mat4 u_ProjectionMatrix;
uniform mat3 u_NormalTransform;

varying vec3 v_Normal;
varying vec3 v_ModelPosition;

void main (void) {

    vec4 modelPosition = u_TransformMatrix * vec4( position, 1.0 );
    v_Normal = u_NormalTransform * normal;

    v_ModelPosition = vec3( modelPosition );
    gl_Position = u_ProjectionMatrix * modelPosition;
}
