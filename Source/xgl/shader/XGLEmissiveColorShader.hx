package mme.xgl.shader;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLShaderProgram;

import mme.xgl.engine.XGLShader;

import mme.util.Macros;


class XGLEmissiveColorShader extends XGLShader {

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSourceA = Macros.getVertexShaderSource();

        var fragmentShaderSourceA =  Macros.getFragmentShaderSource();

        program = new XGLShaderProgram( gl, vertexShaderSourceA, fragmentShaderSourceA
        // , [ "position" ], [ "transformMatrix", "emissiveColor" ]
        );
    }
}
