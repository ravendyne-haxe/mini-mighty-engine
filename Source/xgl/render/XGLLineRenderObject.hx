package mme.xgl.render;

import lime.graphics.WebGLRenderContext;
import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;
import mme.material.LineMaterial;

import mme.xgl.XGLRegistry;
import mme.xgl.material.XGLLineMaterial;
import mme.xgl.geometry.XGLGeometryBufferLine;
import mme.xgl.geometry.converter.XGLConvertToBufferLine3D;
import mme.xgl.engine.XGLRenderObject;


class XGLLineRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLLineMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( meshMaterial, LineMaterial ) ) {
            throw "!!BUG!!: XGLLineRenderObject requires LineMaterial.";
        }
        var objectMaterial : LineMaterial = cast meshMaterial;
        renderMaterial = new XGLLineMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        shader = XGLRegistry.getShader( ShaderName.Line3D );

        //
        // GEOMETRY
        //

        geometryBuffer = new XGLGeometryBufferLine( gl, shader, new XGLConvertToBufferLine3D( mesh ) );
    }

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        renderMaterial.dispose( gl );
        renderMaterial = null;
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform3f( shader.program.uniformHandles['u_LineColor'], material.color.r, material.color.g, material.color.b );
        gl.uniform3f( shader.program.uniformHandles['u_EdgeColor'], renderMaterial.edgeColor.r, renderMaterial.edgeColor.g, renderMaterial.edgeColor.b );

        gl.uniform1f( shader.program.uniformHandles['u_Inner'], renderMaterial.inner );
        gl.uniform1f( shader.program.uniformHandles['u_BlendStart'], renderMaterial.blendStart );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );

        // we set MATERIAL thickness uniform here so we don't have to calculate *transform* matrix twice
        var thickness = renderMaterial.thickness;
        // Line3d has *constant* thickness no matter the angle we look at it.
        // The thickness scales with distance (further parts of the line are thinner), but
        // it stays the same no matter the angle we look at it (as opposed to Line2D, which has zero thickness in Z-direction).
        // We achieve this by calculating new positions for line vertices in vertex shader, in *screen* space.
        // This means that vertex position is transformed in vertex shader to get its on-screen position
        // and that position is then used for calculations.
        // *renderMaterial.thickness* is given in screen pixels and is *not* transformed in vertex shader.
        // The way transformation to screen space is implemented in vertex shader, it works well for perspective
        // projection, but fails majestically for orthographic projection.
        // The reason is that ortho projection matrix transforms vectors so that their *w* component is *1.0*
        // so there is no perspective divide and vectors are already in NDC space. This means that for ortho projection
        // we need to pass *thickness* value that is scaled to NDC space. We could scale thickness in vertex shader,
        // or we could do it here once for all vertices.
        if( ! scene.activeCamera.isPerspective ) {
            // We only need X-coordinate scale value from projection matrix, which is element m00
            thickness = transform[ 0 ] * thickness;
        }
        gl.uniform1f( shader.program.uniformHandles['u_Thickness'], thickness );

        //
        // Setup ATTRIBUTE values
        //
        gl.bindBuffer( GL.ARRAY_BUFFER, geometryBuffer.buffer.vertexBufferHandle );
        geometryBuffer.describeProgramAttributes( gl );

        gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, geometryBuffer.elementBuffer.elementBufferHandle );
    }

    override private function renderDraw( gl : WebGLRenderContext ) : Void {

        //
        // Do DRAWING
        //
        gl.drawElements( GL.TRIANGLES, geometryBuffer.elementBuffer.length, GL.UNSIGNED_SHORT, 0 );
    }
}
