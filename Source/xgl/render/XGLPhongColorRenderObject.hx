package mme.xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import mme.core.Scene;

import mme.material.PhongMaterial;

import mme.xgl.XGLRegistry;
import mme.xgl.XGLRegistry.ShaderName;

import mme.xgl.material.XGLPhongColorMaterial;

import mme.xgl.geometry.XGLGeometryBufferNormal;
import mme.xgl.geometry.converter.XGLConvertToBufferNormal;

import mme.xgl.engine.XGLRenderObject;


class XGLPhongColorRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLPhongColorMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( meshMaterial, PhongMaterial ) ) {
            throw "!!BUG!!: XGLPhongColorRenderObject requires PhongMaterial.";
        }
        var objectMaterial : PhongMaterial = cast meshMaterial;
        renderMaterial = new XGLPhongColorMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        shader = XGLRegistry.getShader( ShaderName.Phong );

        //
        // GEOMETRY
        //

        if( mesh.isIndexed ) {
            mesh.toNonIndexed();
        }
        geometryBuffer = new XGLGeometryBufferNormal( gl, shader, new XGLConvertToBufferNormal( mesh ) );
    }

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        renderMaterial.dispose( gl );
        renderMaterial = null;
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform3f( shader.program.uniformHandles['u_EmissiveColor'], material.color.r, material.color.g, material.color.b );

        gl.uniform3fv( shader.program.uniformHandles['u_Ka'],renderMaterial.ka );
        gl.uniform3fv( shader.program.uniformHandles['u_Kd'],renderMaterial.kd );
        gl.uniform3fv( shader.program.uniformHandles['u_Ks'],renderMaterial.ks );

        gl.uniform1f( shader.program.uniformHandles['u_Shininess'], renderMaterial.shininess );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, sceneObject.transform );
        gl.uniformMatrix3fv( shader.program.uniformHandles['u_NormalTransform'], false, sceneObject.normalTransform );

        //
        // Setup ATTRIBUTE values
        //
        gl.bindBuffer( GL.ARRAY_BUFFER, geometryBuffer.buffer.vertexBufferHandle );
        geometryBuffer.describeProgramAttributes( gl );
    }
    
    override private function renderDraw( gl : WebGLRenderContext ) : Void {

        //
        // Do DRAWING
        //
        gl.drawArrays( GL.TRIANGLES, 0, geometryBuffer.buffer.length );
    }
}
