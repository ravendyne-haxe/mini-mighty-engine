package mme.xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;

import mme.material.BasicMaterial;

import mme.xgl.XGLRegistry;
import mme.xgl.XGLRegistry.ShaderName;

import mme.xgl.material.XGLEmissiveColorMaterial;

import mme.xgl.geometry.XGLGeometryBufferBasic;
import mme.xgl.geometry.converter.XGLConvertToBufferBasic;

import mme.xgl.engine.XGLRenderObject;


class XGLEmissiveColorRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLEmissiveColorMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( meshMaterial, BasicMaterial ) ) {
            throw "!!BUG!!: XGLEmissiveColorRenderObject requires BasicMaterial.";
        }
        var objectMaterial : BasicMaterial = cast meshMaterial;
        renderMaterial = new XGLEmissiveColorMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        shader = XGLRegistry.getShader( ShaderName.EmissiveColor );

        //
        // GEOMETRY
        //

        // TODO switch to drawElements instead of converting to non-indexed
        if( mesh.isIndexed ) {
            mesh.toNonIndexed();
        }
        geometryBuffer = new XGLGeometryBufferBasic( gl, shader, new XGLConvertToBufferBasic( mesh ) );
    }

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        renderMaterial.dispose( gl );
        renderMaterial = null;
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform3f( shader.program.uniformHandles['u_EmissiveColor'], material.color.r, material.color.g, material.color.b );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );

        //
        // Setup ATTRIBUTE values
        //
        gl.bindBuffer( GL.ARRAY_BUFFER, geometryBuffer.buffer.vertexBufferHandle );
        geometryBuffer.describeProgramAttributes( gl );
    }
    
    override private function renderDraw( gl : WebGLRenderContext ) : Void {

        //
        // Do DRAWING
        //
        gl.drawArrays( GL.TRIANGLES, 0, geometryBuffer.buffer.length );
    }
}
