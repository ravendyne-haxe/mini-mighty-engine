package mme.xgl.render;

import mme.xgl.geometry.XGLMeshConverter;
import mme.g2d.geometry.LineCollection2D;
import mme.g2d.geometry.PolyLine2D;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;

import mme.g2d.material.Line2DMaterial;

import mme.xgl.XGLRegistry;
import mme.xgl.XGLRegistry.ShaderName;

import mme.xgl.material.XGLLine2DMaterial;

import mme.xgl.geometry.XGLGeometryBufferLine2D;
import mme.xgl.geometry.converter.XGLConvertPolyline2DToBufferLine2D;
import mme.xgl.geometry.converter.XGLConvertLineCollection2DToBufferLine2D;

import mme.xgl.engine.XGLRenderObject;


class XGLLine2DRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLLine2DMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( meshMaterial, Line2DMaterial ) ) {
            throw "!!BUG!!: XGLLine2DRenderObject requires Line2DMaterial.";
        }
        var objectMaterial : Line2DMaterial = cast meshMaterial;
        renderMaterial = new XGLLine2DMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        shader = XGLRegistry.getShader( ShaderName.Line2D );

        //
        // GEOMETRY
        //

        var converter : XGLMeshConverter = null;
        if( Std.is( sceneObject, PolyLine2D ) ) {
            converter = new XGLConvertPolyline2DToBufferLine2D( cast sceneObject );
        }
        if( Std.is( sceneObject, LineCollection2D ) ) {
            converter = new XGLConvertLineCollection2DToBufferLine2D( cast sceneObject );
        }
        if( converter == null ) {
            throw "!!BUG!!: XGLLine2DRenderObject requires PolyLine2D or LineCollection2D.";
        }
        geometryBuffer = new XGLGeometryBufferLine2D( gl, shader, converter );
    }

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        renderMaterial.dispose( gl );
        renderMaterial = null;
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform3f( shader.program.uniformHandles['u_LineColor'], material.color.r, material.color.g, material.color.b );
        gl.uniform3f( shader.program.uniformHandles['u_EdgeColor'], renderMaterial.edgeColor.r, renderMaterial.edgeColor.g, renderMaterial.edgeColor.b );

        gl.uniform1f( shader.program.uniformHandles['u_Thickness'], renderMaterial.thickness );
        gl.uniform1f( shader.program.uniformHandles['u_Inner'], renderMaterial.inner );
        gl.uniform1f( shader.program.uniformHandles['u_BlendStart'], renderMaterial.blendStart );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );

        //
        // Setup ATTRIBUTE values
        //
        gl.bindBuffer( GL.ARRAY_BUFFER, geometryBuffer.buffer.vertexBufferHandle );
        geometryBuffer.describeProgramAttributes( gl );

        gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, geometryBuffer.elementBuffer.elementBufferHandle );
    }

    override private function renderDraw( gl : WebGLRenderContext ) : Void {

        //
        // Do DRAWING
        //
        gl.drawElements( GL.TRIANGLES, geometryBuffer.elementBuffer.length, GL.UNSIGNED_SHORT, 0 );
    }
}
