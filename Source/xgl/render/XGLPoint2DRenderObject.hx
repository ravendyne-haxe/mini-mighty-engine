package mme.xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;

import mme.g2d.material.Point2DMaterial;

import mme.xgl.XGLRegistry;
import mme.xgl.XGLRegistry.ShaderName;

import mme.xgl.material.XGLPoint2DMaterial;

import mme.xgl.geometry.XGLGeometryBufferUV;
import mme.xgl.geometry.converter.XGLConvertPoint2DToBufferUV;

import mme.xgl.engine.XGLRenderObject;


class XGLPoint2DRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLPoint2DMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( meshMaterial, Point2DMaterial ) ) {
            throw "!!BUG!!: XGLPoint2DRenderObject requires Point2DMaterial.";
        }
        var objectMaterial : Point2DMaterial = cast meshMaterial;
        renderMaterial = new XGLPoint2DMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        switch( objectMaterial.type ) {
            case Circle:
                shader = XGLRegistry.getShader( ShaderName.Point2DCircle );
            case Square:
                shader = XGLRegistry.getShader( ShaderName.Point2DSquare );
            default:
                throw '!!BUG!!: unsupported Point2D type: ${objectMaterial.type}';
        }

        //
        // GEOMETRY
        //

        geometryBuffer = new XGLGeometryBufferUV( gl, shader, new XGLConvertPoint2DToBufferUV( mesh ) );
    }

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        renderMaterial.dispose( gl );
        renderMaterial = null;
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform3f( shader.program.uniformHandles['u_PointColor'], material.color.r, material.color.g, material.color.b );
        gl.uniform3f( shader.program.uniformHandles['u_EdgeColor'], renderMaterial.edgeColor.r, renderMaterial.edgeColor.g, renderMaterial.edgeColor.b );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );

        //
        // Setup ATTRIBUTE values
        //
        gl.bindBuffer( GL.ARRAY_BUFFER, geometryBuffer.buffer.vertexBufferHandle );
        geometryBuffer.describeProgramAttributes( gl );

        gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, geometryBuffer.elementBuffer.elementBufferHandle );
    }

    override private function renderDraw( gl : WebGLRenderContext ) : Void {

        //
        // Do DRAWING
        //
        gl.drawElements( GL.TRIANGLES, geometryBuffer.elementBuffer.length, GL.UNSIGNED_SHORT, 0 );
    }
}
