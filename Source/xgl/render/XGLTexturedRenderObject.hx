package mme.xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;

import mme.material.TextureMaterial;

import mme.xgl.XGLRegistry;
import mme.xgl.XGLRegistry.ShaderName;

import mme.xgl.material.XGLTexturedMaterial;

import mme.xgl.geometry.XGLGeometryBufferUV;
import mme.xgl.geometry.converter.XGLConvertToBufferUV;

import mme.xgl.engine.XGLRenderObject;


class XGLTexturedRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLTexturedMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( meshMaterial, TextureMaterial ) ) {
            throw "!!BUG!!: XGLTexturedRenderObject requires TextureMaterial.";
        }
        var objectMaterial : TextureMaterial = cast meshMaterial;
        renderMaterial = new XGLTexturedMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        shader = XGLRegistry.getShader( ShaderName.Texture );

        //
        // GEOMETRY
        //

        // TODO switch to drawElements instead of converting to non-indexed
        if( mesh.isIndexed ) {
            mesh.toNonIndexed();
        }
        geometryBuffer = new XGLGeometryBufferUV( gl, shader, new XGLConvertToBufferUV( mesh ) );
    }

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        renderMaterial.dispose( gl );
        renderMaterial = null;
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform1i( shader.program.uniformHandles['u_Texture'], renderMaterial.texture.textureUnit );

        //
        //  Setup TEXTURE
        //
        gl.activeTexture( GL.TEXTURE0 + renderMaterial.texture.textureUnit );
        gl.bindTexture( GL.TEXTURE_2D, renderMaterial.texture.textureHandle );
    }
    
    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );

        //
        // Setup ATTRIBUTE values
        //
        gl.bindBuffer( GL.ARRAY_BUFFER, geometryBuffer.buffer.vertexBufferHandle );
        geometryBuffer.describeProgramAttributes( gl );
    }
    
    override private function renderDraw( gl : WebGLRenderContext ) : Void {

        //
        // Do DRAWING
        //
        gl.drawArrays( GL.TRIANGLES, 0, geometryBuffer.buffer.length );
    }
}

