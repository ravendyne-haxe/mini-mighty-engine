package mme.xgl.geometry;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLGeometryBuffer;
import mme.xgl.geometry.converter.XGLConvertToBufferLine3D;
import mme.xgl.engine.XGLShader;


class XGLGeometryBufferLine extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLConvertToBufferLine3D ) {

        // attribute format:
        // X, Y, Z, direction -> number of elements == 4
        super( gl, shader, meshConverter, 4 );

        // this one is a bit tricky:
        // sum of all attributes' elements can't actually fit
        // into number of elements we gave as parameter to super().
        // this is because we are using overlapping attributes.
        // buffer will have 4 extra attributes added so we will not
        // overshoot 'curPosition' and 'nextPosition' on the last element
        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'prevPosition', 3, 0 );
        // X, Y, Z -> number of elements == 3, offset == 8
		buffer.addAttribute( 'curPosition', 3, 8 );
        // direction -> number of elements == 1, offset == 8 + 3
        // we wan't 'direction' that comes with 'curPosition'
		buffer.addAttribute( 'direction', 1, 8 + 3 );
        // X, Y, Z -> number of elements == 3, offset == 16
		buffer.addAttribute( 'nextPosition', 3, 16 );
    }
}
