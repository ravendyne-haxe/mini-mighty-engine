package mme.xgl.geometry;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLGeometryBuffer;
import mme.xgl.engine.XGLShader;


class XGLGeometryBufferUV extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLMeshConverter ) {

        // attribute format:
        // X, Y, Z, U, V -> number of elements == 5
        super( gl, shader, meshConverter, 5 );

        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'position', 3, 0 );
        // U, V -> number of elements == 2, offset == 3
		buffer.addAttribute( 'aTextureUV', 2, 3 );
    }
}
