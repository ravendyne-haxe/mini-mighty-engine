package mme.xgl.geometry;

import mme.math.glmatrix.Vec3;
import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLGeometryBuffer;
import mme.xgl.engine.XGLShader;


class XGLGeometryBufferBarycentric extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLMeshConverter ) {

        // attribute format:
        // X, Y, Z, BCx, BCy, BCz -> number of elements == 6
        super( gl, shader, meshConverter, 6 );

        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'position', 3, 0 );
        // BCx, BCy, BCz -> number of elements == 3, offset == 3
		buffer.addAttribute( 'barycentric', 3, 3 );
    }
}
