package mme.xgl.geometry;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLGeometryBuffer;
import mme.xgl.engine.XGLShader;


class XGLGeometryBufferBasic extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLMeshConverter ) {

        // attribute format:
        // X, Y, Z -> number of elements == 3
        super( gl, shader, meshConverter, 3 );

        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'position', 3, 0 );
    }
}
