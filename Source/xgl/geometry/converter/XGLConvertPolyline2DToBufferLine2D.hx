package mme.xgl.geometry.converter;

import mme.math.glmatrix.Vec2;
import mme.g2d.geometry.PolyLine2D;
import mme.util.Utils;

import lime.utils.UInt16Array;
import lime.utils.Float32Array;

import mme.math.glmatrix.Vec3;
import mme.util.geometry.line2d.LineUtils;

import mme.geometry.Plane;
import mme.core.Mesh;


class XGLConvertPolyline2DToBufferLine2D extends XGLMeshConverter {

    var sceneObject : PolyLine2D;

    public function new( sceneObject : PolyLine2D ) {
        super( sceneObject.mesh );
        this.sceneObject = sceneObject;
    }

    override public function convert() : Void {

        // Sys.println('XGLConvertPolyline2DToBufferLine2D.convert');

        var convertedMesh = convertMesh();
        // Utils.printMesh(convertedMesh);

        setAttributeBufferData( meshToArray( convertedMesh ) );

        if( convertedMesh.isIndexed ) {
            isIndexed = true;
            setIndicesBufferData( convertedMesh.indices );
        }
    }

    function meshToArray( convertedMesh : Mesh ) : Array< Float > {

        var array = [];

		for( idx in 0...convertedMesh.vertices.length ) {

			array.push( convertedMesh.vertices[ idx ][0] );
			array.push( convertedMesh.vertices[ idx ][1] );

			array.push( convertedMesh.normals[ idx ][0] );
			array.push( convertedMesh.normals[ idx ][1] );

			array.push( convertedMesh.otherParams[ idx ][0] );
		}

        return array;
    }

    public function convertMesh() : Mesh {

        var convertedMesh = new Mesh();

        var path : Array<Vec2> = [];

        for( v in mesh.vertices ) {
            path.push([ v.x, v.y ]);
        }

        var outline = LineUtils.createOutline( path, sceneObject.isClosed );


		for( idx in 0...outline.points.length ) {

            convertedMesh.vertices.push( [ outline.points[ idx ].x, outline.points[ idx ].y, 0.0 ] );
            convertedMesh.normals.push( [ outline.normals[ idx ].x, outline.normals[ idx ].y, 0.0 ] );

            convertedMesh.otherParams.push( [ outline.miters[ idx ] ] );
		}

        convertedMesh.indices = outline.indices.copy();

        return convertedMesh;
    }
}
