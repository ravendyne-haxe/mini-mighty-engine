package mme.xgl.geometry.converter;

import lime.utils.UInt16Array;
import lime.utils.Float32Array;
import mme.core.Mesh;
import mme.util.geometry.line2d.LineUtils;


class XGLConvertToBufferLine3D extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override public function convert() : Void {

        // Sys.println('XGLConvertToBufferLine3D.convert');

        var convertedMesh = convertMesh();

        var array = [];

        for( idx in 0...convertedMesh.vertices.length ) {

            var vertex = convertedMesh.vertices[ idx ];
            var dist = convertedMesh.otherParams[ idx ];

            array.push( vertex[ 0 ] );
            array.push( vertex[ 1 ] );
            array.push( vertex[ 2 ] );
            array.push( dist[ 0 ] );
        }

        setAttributeBufferData( array );

        if( convertedMesh.isIndexed ) {
            isIndexed = true;
            setIndicesBufferData( convertedMesh.indices );
        }
    }

    public function convertMesh() : Mesh {

        var convertedMesh = new Mesh();
        var path = mesh.vertices;

        // each line consists of two points.
        // we will be rendering lines using quads and for
        // that we need 4 points. to get a quad for a line
        // we simply duplicate each of the line's two points and add
        // a parameter (+1/-1 value) to each. once we finished
        // duplicating, we call LineUtils.createIndices() which
        // will create indexes into our (duplicated) vertex array
        // so that we end up having two (indexed) triangles for
        // each quad.


        // we need to have the start point twice at the beginning.
        // this is the first copy, second one will be in the loop
        convertedMesh.vertices.push( path[ 0 ] );
        convertedMesh.otherParams.push([ 1 ]);
        convertedMesh.vertices.push( path[ 0 ] );
        convertedMesh.otherParams.push([ -1 ]);

		for( idx in 0...path.length ) {

            // duplicate each point in the path
            // marking one to be offset in one direction (+1)
            // and the other in the opposite direction (-1)
            convertedMesh.vertices.push( path[ idx ] );
            convertedMesh.otherParams.push([ 1 ]);
            convertedMesh.vertices.push( path[ idx ] );
            convertedMesh.otherParams.push([ -1 ]);
		}

        // we need to have the end point twice at the end.
        // this is the second copy, first one was in the loop
        convertedMesh.vertices.push( path[ path.length - 1 ] );
        convertedMesh.otherParams.push([ 1 ]);
        convertedMesh.vertices.push( path[ path.length - 1 ] );
        convertedMesh.otherParams.push([ -1 ]);

        convertedMesh.indices = LineUtils.createIndices( path.length );

        return convertedMesh;
    }
}
