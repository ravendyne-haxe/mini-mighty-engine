package mme.xgl.geometry.converter;

import lime.utils.UInt16Array;
import lime.utils.Float32Array;
import mme.core.Mesh;


class XGLConvertToBufferNormal extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override public function convert() : Void {

        // Sys.println('XGLConvertToBufferNormal.convert');

        var array = [];

        for( idx in 0...mesh.vertices.length ) {

            // NOT checking here if mesh.vertices and mesh.normals
            // have same length, that is a requirement for well-formed Mesh
            // that wants to become an instance of XGLGeometryBufferNormal
            var vertex = mesh.vertices[ idx ];
            var normal = mesh.normals[ idx ];

            array.push( vertex[ 0 ] );
            array.push( vertex[ 1 ] );
            array.push( vertex[ 2 ] );
            array.push( normal[ 0 ] );
            array.push( normal[ 1 ] );
            array.push( normal[ 2 ] );
        }

        setAttributeBufferData( array );

        if( mesh.isIndexed ) {
            isIndexed = true;
            setIndicesBufferData( mesh.indices );
        }
    }
}
