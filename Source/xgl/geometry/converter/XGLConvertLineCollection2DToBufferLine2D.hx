package mme.xgl.geometry.converter;

import mme.g2d.geometry.LineCollection2D;
import mme.math.glmatrix.Vec2;
import mme.util.Utils;

import lime.utils.UInt16Array;
import lime.utils.Float32Array;

import mme.math.glmatrix.Vec3;
import mme.util.geometry.line2d.LineUtils;

import mme.geometry.Plane;
import mme.core.Mesh;


class XGLConvertLineCollection2DToBufferLine2D extends XGLMeshConverter {

    var sceneObject : LineCollection2D;

    public function new( sceneObject : LineCollection2D ) {
        super( sceneObject.mesh );
        this.sceneObject = sceneObject;
    }

    override public function convert() : Void {

        // Sys.println('XGLConvertLineCollection2DToBufferLine2D.convert');

        var convertedMesh = convertMesh();
        // Utils.printMesh(convertedMesh);

        setAttributeBufferData( meshToArray( convertedMesh ) );

        if( convertedMesh.isIndexed ) {
            isIndexed = true;
            setIndicesBufferData( convertedMesh.indices );
        }
    }

    function meshToArray( convertedMesh : Mesh ) : Array< Float > {


        var array = [];

		for( idx in 0...convertedMesh.vertices.length ) {

			array.push( convertedMesh.vertices[ idx ][0] );
			array.push( convertedMesh.vertices[ idx ][1] );

			array.push( convertedMesh.normals[ idx ][0] );
			array.push( convertedMesh.normals[ idx ][1] );

			array.push( convertedMesh.otherParams[ idx ][0] );
		}

        return array;
    }

    public function convertMesh() : Mesh {

        var convertedMesh = new Mesh();

        var idx = 0;
        while( idx < mesh.vertices.length ) {

            var v1 : Vec2 = [ mesh.vertices[ idx ].x, mesh.vertices[ idx ].y ];
            var v2 : Vec2 = [ mesh.vertices[ idx + 1 ].x, mesh.vertices[ idx + 1 ].y ];

            var outline = LineUtils.createOutline( [ v1, v2 ], false );

            var indexOffset = convertedMesh.vertices.length;

            for( idx in 0...outline.points.length ) {

                convertedMesh.vertices.push( [ outline.points[ idx ].x, outline.points[ idx ].y, 0.0 ] );
                convertedMesh.normals.push( [ outline.normals[ idx ].x, outline.normals[ idx ].y, 0.0 ] );

                convertedMesh.otherParams.push( [ outline.miters[ idx ] ] );
            }

            for( idx in outline.indices ) {

                convertedMesh.indices.push( indexOffset + idx );
            }

            idx += 2;
        }

        return convertedMesh;
    }
}
