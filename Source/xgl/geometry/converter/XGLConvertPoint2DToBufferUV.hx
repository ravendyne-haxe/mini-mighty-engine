package mme.xgl.geometry.converter;

import mme.util.Utils;

import lime.utils.UInt16Array;
import lime.utils.Float32Array;

import mme.math.glmatrix.Vec3;

import mme.geometry.Plane;
import mme.core.Mesh;


class XGLConvertPoint2DToBufferUV extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override public function convert() : Void {

        // Sys.println('XGLConvertPoint2DToBufferUV.convert');

        var convertedMesh = convertMesh();
        // Utils.printMesh(convertedMesh);

        setAttributeBufferData( meshToArray( convertedMesh ) );

        if( convertedMesh.isIndexed ) {
            isIndexed = true;
            setIndicesBufferData( convertedMesh.indices );
        }
    }

    function meshToArray( convertedMesh : Mesh ) : Array< Float > {

        var array = [];

        for( idx in 0...convertedMesh.vertices.length ) {

            // NOT checking here if mesh.vertices and mesh.uvs
            // have same length, that is a requirement for well-formed Mesh
            // that wants to become an instance of XGLGeometryBufferUV
            var vertex = convertedMesh.vertices[ idx ];
            var uv = convertedMesh.uvs[ idx ];
            // lime.utils.Log.info('vertex = ${vertex}');
            // lime.utils.Log.info('uv = ${uv}');

            array.push( vertex[ 0 ] );
            array.push( vertex[ 1 ] );
            array.push( vertex[ 2 ] );
            array.push( uv[ 0 ] );
            array.push( uv[ 1 ] );
        }

        return array;
    }

    public function convertMesh() : Mesh {

        var points = mesh.vertices;
        var params = mesh.otherParams;

        var p = points[ 0 ];
        var par : Array<Float> = cast params[ 0 ];
        // a hack to make sure that we end up with indexed mesh
        // Plane.makeXY creates indexed mesh and
        // Mesh.merge will assume a mesh is non-indexed if it is
        // initially empty
        var newmesh = Plane.makeXY( par[ 0 ], par[ 1 ] );
        newmesh.moveTo( p );

        for( idx in 1...points.length ) {

            p = points[ idx ];
            par = cast params[ idx ];

            makeAPoint( newmesh, p, par[ 0 ], par[ 1 ] );
        }

        return newmesh;
    }

    private function makeAPoint( newmesh : Mesh, p : Vec3, width : Float, height : Float ) {

        var pointMesh = Plane.makeXY( width, height );
        pointMesh.moveTo( p );
        newmesh.merge( pointMesh );
    }
}
