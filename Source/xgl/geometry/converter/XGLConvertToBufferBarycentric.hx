package mme.xgl.geometry.converter;

import lime.utils.UInt16Array;
import lime.utils.Float32Array;

import mme.math.glmatrix.Vec3;
import mme.core.Mesh;


class XGLConvertToBufferBarycentric extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override public function convert() : Void {

        // Sys.println('XGLConvertToBufferBarycentric.convert');

        if( mesh.elementType != Triangle ) {
            throw "XGLConvertToBufferBarycentric requires Mesh with Triangle element type";
        }

        var array = [];

        var addToArray = function( v : Vec3, bc : Vec3 ) {
            array.push( v.x );
            array.push( v.y );
            array.push( v.z );
            array.push( bc.x );
            array.push( bc.y );
            array.push( bc.z );
        }

        for( triangle in mesh ) {

            var v0 = mesh.vertices[ triangle.i0 ];
            var v1 = mesh.vertices[ triangle.i1 ];
            var v2 = mesh.vertices[ triangle.i2 ];
            var bc0 : Vec3 = [ 1, 0, 0 ];
            var bc1 : Vec3 = [ 0, 1, 0 ];
            var bc2 : Vec3 = [ 0, 0, 1 ];

            addToArray( v0, bc0 );
            addToArray( v1, bc1 );
            addToArray( v2, bc2 );
        }

        setAttributeBufferData( array );

        isIndexed = false;
    }
}
