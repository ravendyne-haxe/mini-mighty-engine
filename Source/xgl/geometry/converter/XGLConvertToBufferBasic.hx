package mme.xgl.geometry.converter;

import mme.core.Mesh;


class XGLConvertToBufferBasic extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override public function convert() : Void {

        // Sys.println('XGLConvertToBufferBasic.convert');

        var array = [];

        for( idx in 0...mesh.vertices.length ) {

            var vertex = mesh.vertices[ idx ];

            array.push( vertex[ 0 ] );
            array.push( vertex[ 1 ] );
            array.push( vertex[ 2 ] );
        }

        setAttributeBufferData( array );

        if( mesh.isIndexed ) {
            isIndexed = true;
            setIndicesBufferData( mesh.indices );
        }
    }
}
