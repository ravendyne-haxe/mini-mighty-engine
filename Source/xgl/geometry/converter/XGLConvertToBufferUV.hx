package mme.xgl.geometry.converter;

import lime.utils.UInt16Array;
import lime.utils.Float32Array;
import mme.core.Mesh;


class XGLConvertToBufferUV extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override public function convert() : Void {

        // Sys.println('XGLConvertToBufferUV.convert');

        var array = [];

        for( idx in 0...mesh.vertices.length ) {

            // NOT checking here if mesh.vertices and mesh.uvs
            // have same length, that is a requirement for well-formed Mesh
            // that wants to become an instance of XGLGeometryBufferUV
            var vertex = mesh.vertices[ idx ];
            var uv = mesh.uvs[ idx ];

            array.push( vertex[ 0 ] );
            array.push( vertex[ 1 ] );
            array.push( vertex[ 2 ] );
            array.push( uv[ 0 ] );
            array.push( uv[ 1 ] );
        }

        setAttributeBufferData( array );

        if( mesh.isIndexed ) {
            isIndexed = true;
            setIndicesBufferData( mesh.indices );
        }
    }
}
