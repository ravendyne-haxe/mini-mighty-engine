package mme.xgl.geometry;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLGeometryBuffer;
import mme.xgl.engine.XGLShader;


class XGLGeometryBufferLine2D extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLMeshConverter ) {

        // attribute format:
        // X, Y, Nx, Ny, miter -> number of elements == 5
        super( gl, shader, meshConverter, 5 );

        // X, Y -> number of elements == 2, offset == 0
		buffer.addAttribute( 'position', 2, 0 );
        // Nx, Ny -> number of elements == 2, offset == 2
		buffer.addAttribute( 'normal', 2, 2 );
        // miter -> number of elements == 1, offset == 4
		buffer.addAttribute( 'miter', 1, 4 );
    }
}
