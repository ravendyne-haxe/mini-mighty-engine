package mme.xgl.geometry;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLGeometryBuffer;
import mme.xgl.geometry.converter.XGLConvertToBufferNormal;
import mme.xgl.engine.XGLShader;


class XGLGeometryBufferNormal extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLMeshConverter ) {

        // attribute format:
        // X, Y, Z, Nx, Ny, Nz -> number of elements == 6
        super( gl, shader, meshConverter, 6 );

        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'position', 3, 0 );
        // Nx, Ny, Nz -> number of elements == 3, offset == 3
		buffer.addAttribute( 'normal', 3, 3 );
    }
}
