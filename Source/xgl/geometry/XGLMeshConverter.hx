package mme.xgl.geometry;

import lime.utils.UInt16Array;
import lime.utils.Float32Array;
import lime.graphics.opengl.GL;
import mme.core.Mesh;


class XGLMeshConverter {

    private var mesh : Mesh;

    public var attributeBuffer ( default, null ) : Float32Array;
    public var indicesBuffer ( default, null ) : UInt16Array;
    public var isIndexed ( default, null ) : Bool;

    public function new( mesh : Mesh ) {
        this.mesh = mesh;
        isIndexed = false;
    }

    public function dispose() {
        if( attributeBuffer != null ) attributeBuffer = null;
        if( indicesBuffer != null ) indicesBuffer = null;
    }

    public function setAttributeBufferData( data : Array< Float > ) {
        // TODO : attributeBuffer.set( data );
        attributeBuffer = new Float32Array( data );
    }

    public function setIndicesBufferData( data : Array< Int > ) {
        // TODO : indicesBuffer.set( data );
        indicesBuffer = new UInt16Array( data );
    }

    public function convert() : Void { }

    public function getUsage() : Int {

        switch( mesh.usage ) {

            case MeshStatic:
            return GL.STATIC_DRAW;

            case MeshDynamic:
            return GL.DYNAMIC_DRAW;

            case MeshStream:
            return GL.STREAM_DRAW;
        }

        return GL.STATIC_DRAW;
    }
}
