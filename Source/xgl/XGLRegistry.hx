package mme.xgl;

import lime.graphics.WebGLRenderContext;

import mme.core.SceneObject;

import mme.material.BasicMaterial;
import mme.material.TextureMaterial;
import mme.material.PhongMaterial;
import mme.g2d.material.Line2DMaterial;
import mme.g2d.material.Point2DMaterial;
import mme.material.LineMaterial;
import mme.material.TexturedPhongMaterial;
import mme.material.WireframeMaterial;

import mme.xgl.engine.XGLShader;
import mme.xgl.engine.XGLRenderObject;

import mme.xgl.render.XGLEmissiveColorRenderObject;
import mme.xgl.render.XGLWireframeRenderObject;
import mme.xgl.render.XGLPhongColorRenderObject;
import mme.xgl.render.XGLTexturedRenderObject;
import mme.xgl.render.XGLLine2DRenderObject;
import mme.xgl.render.XGLPoint2DRenderObject;
import mme.xgl.render.XGLLineRenderObject;
import mme.xgl.render.XGLTexturedPhongRenderObject;

import mme.xgl.shader.XGLEmissiveColorShader;
import mme.xgl.shader.XGLWireframeShader;
import mme.xgl.shader.XGLPhongColorShader;
import mme.xgl.shader.XGLTextureShader;
import mme.xgl.shader.XGLLine2DShader;
import mme.xgl.shader.XGLPoint2DShader.XGLPoint2DCircleShader;
import mme.xgl.shader.XGLPoint2DShader.XGLPoint2DSquareShader;
import mme.xgl.shader.XGLLineShader;
import mme.xgl.shader.XGLTexturedPhongShader;


class ShaderName {
    public static inline var EmissiveColor = 0x80000001;
    public static inline var Wireframe = 0x80000002;
    public static inline var Phong = 0x80000003;
    public static inline var Texture = 0x80000004;
    public static inline var Line2D = 0x80000005;
    public static inline var Point2DCircle = 0x80000006;
    public static inline var Point2DSquare = 0x80000007;
    public static inline var Line3D = 0x80000008;
    public static inline var TexturedPhong = 0x80000009;
}

interface XGLRegistryExtension {
    function initialize( gl : WebGLRenderContext ) : Void;
    function dispose( gl : WebGLRenderContext ) : Void;
    function getShader( shaderName : Int ) : XGLShader;
    function createRenderObjectFor( gl : WebGLRenderContext, sceneObject : SceneObject ) : XGLRenderObject;
}

class XGLRegistry {

    private static var emissiveColorShader : XGLShader;
    private static var wireframeShader : XGLShader;
    private static var phongColorShader : XGLShader;
    private static var textureShader : XGLShader;
    private static var line2DShader : XGLShader;
    private static var point2DCircleShader : XGLShader;
    private static var point2DSquareShader : XGLShader;
    private static var line3DShader : XGLShader;
    private static var texturedPhongShader : XGLShader;

    private static var initialized : Bool;

    private static var extensions : Array< XGLRegistryExtension >;

    public static function initialize( gl : WebGLRenderContext ) : Void {

        if( initialized ) return;

        emissiveColorShader = new XGLEmissiveColorShader( gl );
        wireframeShader = new XGLWireframeShader( gl );
        phongColorShader = new XGLPhongColorShader( gl );
        textureShader = new XGLTextureShader( gl );
        line2DShader = new XGLLine2DShader( gl );
        point2DCircleShader = new XGLPoint2DCircleShader( gl );
        point2DSquareShader = new XGLPoint2DSquareShader( gl );
        line3DShader = new XGLLineShader( gl );
        texturedPhongShader = new XGLTexturedPhongShader( gl );

        extensions = [];

        initialized = true;
    }

    public static function registerExtension( extension : XGLRegistryExtension ) {
        extensions.push( extension );
    }

    public static function dispose( gl : WebGLRenderContext ) {

        initialized = false;

        emissiveColorShader.dispose( gl );
        emissiveColorShader = null;
        wireframeShader.dispose( gl );
        wireframeShader = null;
        phongColorShader.dispose( gl );
        phongColorShader = null;
        textureShader.dispose( gl );
        textureShader = null;
        line2DShader.dispose( gl );
        line2DShader = null;
        point2DCircleShader.dispose( gl );
        point2DCircleShader = null;
        point2DSquareShader.dispose( gl );
        point2DSquareShader = null;
        line3DShader.dispose( gl );
        line3DShader = null;
        texturedPhongShader.dispose( gl );
        texturedPhongShader = null;

        for( ext in extensions ) {
            ext.dispose( gl );
        }
    }

    public static function getShader( shaderName : Int ) : XGLShader {

        switch( shaderName ) {

            case ShaderName.EmissiveColor:
                return emissiveColorShader;

            case ShaderName.Wireframe:
                return wireframeShader;

            case ShaderName.Phong:
                return phongColorShader;

            case ShaderName.Texture:
                return textureShader;

            case ShaderName.Line2D:
                return line2DShader;

            case ShaderName.Point2DCircle:
                return point2DCircleShader;

            case ShaderName.Point2DSquare:
                return point2DSquareShader;

            case ShaderName.Line3D:
                return line3DShader;

            case ShaderName.TexturedPhong:
                return texturedPhongShader;
            
            default:
            for( ext in extensions ) {
                var shader = ext.getShader( shaderName );
                // lime.utils.Log.info('ext.getShader($shaderName) != null ? ${shader != null}');
                if( shader != null ) return shader;
            }
        }

        return null;
    }

    public static function createRenderObjectFor( gl : WebGLRenderContext, sceneObject : SceneObject ) : XGLRenderObject {

        if( Std.is( sceneObject.material, BasicMaterial ) ) {

            return new XGLEmissiveColorRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, WireframeMaterial ) ) {

            return new XGLWireframeRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, PhongMaterial ) ) {

            return new XGLPhongColorRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, TextureMaterial ) ) {

            return new XGLTexturedRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, Line2DMaterial ) ) {

            return new XGLLine2DRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, Point2DMaterial ) ) {

            return new XGLPoint2DRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, LineMaterial ) ) {

            return new XGLLineRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, TexturedPhongMaterial ) ) {

            return new XGLTexturedPhongRenderObject( gl, sceneObject );
        }

        for( ext in extensions ) {
            var ro = ext.createRenderObjectFor( gl, sceneObject );
            // lime.utils.Log.info('createRenderObjectFor ro != null ? ${ro != null}');
            if( ro != null ) return ro;
        }


        return null;
    }
}
