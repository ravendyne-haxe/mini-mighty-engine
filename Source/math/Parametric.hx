package mme.math;

import mme.core.Mesh;
import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;

/**
    Implementation of parametric generators for planar, cylindrical and spherical 2D domains.

    Based on ["Mesh Generation with Python"](https://prideout.net/blog/old/blog/index.html@p=44.html) article and
    ["Some Common Surfaces and their Parameterizations"](http://clas.sa.ucsb.edu/docs/default-source/Math-6B/parametricsurfaces.pdf?sfvrsn=2)
    UCSB `Math 6B` class notes.
**/
class Parametric {

    /**
        Planar parametric surface of the form `V = F( x, y )`.

        Vector `V=[x,y,z]` is generated from parameters `u` and `v` both in range `[ 0, 1 ]`:
            - vertices at `u = 0` and `u = 1` are not considered to be the same
            - vertices at `v = 0` and `v = 1` are not considered to be the same

        `slices` is number of horizontal segments, number of divisions in `v` dimension, and `v` goes from `0` to `1`.
        We generate `slices + 1` vertex stripes for this dimension since the surface is *not* connected at the edges in this direction.

        `stacks` is number of vertical segments, number of divisions in `u` dimension, and `u` goes from `0` to `1`.
        We generate `stacks + 1` vertex stripes for this dimension since the surface is *not* connected at the edges in this direction.

        @param slices number of divisions of `v` dimension domain
        @param stacks number of divisions of `u` dimension domain
        @param func function that maps `u, v` values to `[x,y,z] Vec3` point
        @param mesh optional `Mesh` to use as output
    **/
    public static function psurface( slices : Int, stacks : Int, func : Float -> Float -> Vec3, ?mesh : Mesh ) : Mesh {

        if( slices < 1 )
            throw "not going to work";
        if( stacks < 1 )
            throw "not going to work";

        if( mesh == null ) {
            mesh = new Mesh();
        }

        mesh.vertices = [];
        mesh.uvs = [];
        mesh.normals = [];
        mesh.indices = [];

        for( i in 0...(slices + 1) ) {
            var v = i / slices;
            for( j in 0...(stacks + 1) ) {
                var u = j / stacks;
                var p = func( u, v );
                mesh.vertices.push( p );
                generateUV( u, v, mesh );
                generateNormal( u, v, func, mesh );
            }
        }

        makeOpenMesh( slices, stacks, mesh );

        return mesh;
    }

    /**
        Spherical parametric surface of the form `V = F( theta, phi )`.

        [Radial distance](https://en.wikipedia.org/wiki/Spherical_coordinate_system) is assumed constant and equals to `1.0`.
        Spherical coordinates are named by mathematical conventions.

        Function `generateNormal()`, which generates normal vectors for surface vertices, expects `z` coordinate values returned by `F( theta, phi )`
        to go from negative to positive when `phi` goes from `0` to `PI`.
        Because `cos( phi )` goes [from positive to negative values](https://en.wikipedia.org/wiki/Spherical_coordinate_system#/media/File:3D_Spherical_2.svg),
        an implementation of `F( theta, phi )` function has to take that into consideration when generating values for Cartezian `z` coordinate.

        Vector `V=[x,y,z]` is generated from parameters `theta` and `phi` in range:
            - `theta` from `0` to `2 * PI`
            - `phi` from `0` to `PI`
            - vertices at `theta` = 0 and `theta` = 2 * PI are considered to be the same
            - vertices at `phi` = 0 and `phi` = PI are not considered to be the same

        `slices` is number of horizontal segments, number of divisions in `phi` dimension,  and `phi` goes from `0` to `PI`.
        We generate `slices + 1` vertex stripes for this dimension since the surface is *not* connected at the edges in this direction.

        `stacks` is number of vertical segments, number of divisions in `theta` dimension,  and `theta` goes from `0` to `2 * PI`.
        We generate `stacks` vertex stripes for this dimension since the surface *is* connected at the edges in this direction
        `stacks` has to be at minimum > 1 for spherical surface to make sense.


        @param slices number of divisions of `phi` dimension domain
        @param stacks number of divisions of `theta` dimension domain
        @param func function that maps `phi, theta` values to `[x,y,z] Vec3` point
        @param mesh optional `Mesh` to use as output
    **/
    public static function ssurface( slices : Int, stacks : Int, func : Float -> Float -> Vec3, ?mesh : Mesh ) : Mesh {

        if( slices < 1 )
            throw "not going to work";
        if( stacks < 2 )
            throw "not going to work";

        if( mesh == null ) {
            mesh = new Mesh();
        }

        mesh.vertices = [];
        mesh.uvs = [];
        mesh.normals = [];
        mesh.indices = [];

        // Sys.println('stacks (->theta) = ${stacks}, slices (->phi) = ${slices}');
        for( i in 0...(slices + 1) ) {
            var phi = i * Math.PI / slices; // v
            for( j in 0...(stacks) ) {
                var theta = j * 2.0 * Math.PI / stacks; // u
                // Sys.println('theta = ${theta}, phi = ${phi}');
                var p = func( theta, phi );
                // Sys.println('F( theta, phi ) = ${p}');
                mesh.vertices.push( p );
                // TODO texture mapping could be an issue because
                // v parameter (j/stacks) is never == 1.0
                // triangles at upper edge of theta range connect
                // to points from theta = 0
                generateUV( j / stacks, i / slices, mesh );
                generateNormal( theta, phi, func, mesh );
            }
        }

        makeConnectedMesh( slices, stacks, mesh );

        return mesh;
    }

    /**
        Cylindrical parametric surface of the form `V= F( theta, zc )`.

        Cylindrical coordinate [`rho`](https://en.wikipedia.org/wiki/Cylindrical_coordinate_system) is assumed constant and equals to `1.0`.

        Vector `V=[x,y,z]` is generated from parameters `zc` and `theta` in range:
            - `zc` from `0` to `1`
            - `theta` from `0` to `2*PI`
            - vertices at `zc` = 0 and `zc` = 1 are not considered to be the same
            - vertices at `theta` = 0 and `theta` = 2 * PI are considered to be the same

        `slices` is number of horizontal segments and `zc` goes from `0` to `1`.
        We generate `slices + 1` vertex stripes for this dimension since the surface is *not* connected at the edges in this direction

        `stacks` is number of vertical segments and `theta` goes from `0` to `2 * PI`.
        We generate `stacks` vertex stripes for this dimension since the surface *is* connected at the edges in this direction.
        `stacks` has to be at minimum > 1 for cylindrical surface to make sense.


        @param slices number of divisions of `zc` dimension domain
        @param stacks number of divisions of `theta` dimension domain
        @param func function that maps `theta, zc` values to `[x,y,z] Vec3` point
        @param mesh optional `Mesh` to use as output
    **/
    public static function csurface( slices : Int, stacks : Int, func : Float -> Float -> Vec3, ?mesh : Mesh ) : Mesh {
 
        if( slices < 1 )
            throw "not going to work";
        if( stacks < 2 )
            throw "not going to work";

        if( mesh == null ) {
            mesh = new Mesh();
        }

        mesh.vertices = [];
        mesh.uvs = [];
        mesh.normals = [];
        mesh.indices = [];

        // Sys.println('stacks (->theta) = ${stacks}, slices (->zc) = ${slices}');
        for( i in 0...(slices + 1) ) {
            var zc = i / slices; // v
            for( j in 0...(stacks) ) {
                var theta = j * 2.0 * Math.PI / stacks; // u
                // Sys.println('theta = ${theta}, zc = ${zc}');
                var p = func( theta, zc );
                // Sys.println('F( theta, zc ) = ${p}');
                mesh.vertices.push( p );
                // TODO texture mapping could be an issue because
                // v parameter (j/stacks) is never == 1.0
                // triangles at upper edge of theta range connect
                // to points from theta = 0
                generateUV( j / stacks, zc, mesh);
                generateNormal( theta, zc, func, mesh );
            }
        }

        makeConnectedMesh( slices, stacks, mesh );

       return mesh;
    }

    private static inline function generateUV( u : Float, v : Float, mesh : Mesh ) {

        // flip v coordinate since
        // texture coordinates start in upper-left corner and
        // OpenGL coordinates start in lower-left corner
        mesh.uvs.push([ u, 1.0 - v ]);
    }

    private static var EPSILON = 0.00001;
    private static function generateNormal( u : Float, v : Float, func : Float -> Float -> Vec3, mesh : Mesh ) {

        var u0 = u;
        var v0 = v;
        // in any case, function 'func' expects u and v parameters to be > 0
        // we subtract EPSILON from current u,v since the edge case
        // that is common to all three variants: planar, cylindrical and spherical,
        // is when u < 0 or v < 0.
        var u1 = u - EPSILON;
        var v1 = v - EPSILON;
        // in case when u < 0 or v < 0, we have to use a point in opposite direction
        // on that dimension. we also need to swap u and/or v values in these cases
        // since cross product expects vectors A and B to point from point p0
        // to points p1 and p2, respectively.
        // no need to handle all combinations of signs of u1 and v1 since u0
        // will travel around so that we end up with correct vector directions in all cases
        if( u1 < 0 ) { u0 = u + EPSILON; u1 = u; }
        if( v1 < 0 ) { v0 = v + EPSILON; v1 = v; }

        var p0 = func( u0, v0 );
        var p1 = func( u1, v0 );
        var p2 = func( u0, v1 );

        var A = p0.sub( p1 );
        var B = p0.sub( p2 );
        var normal = A.cross( B );
        // avoid creating additional array just to normalize
        normal.normalize( normal );

        mesh.normals.push( normal );
    }

    private static function makeOpenMesh( slices : Int, stacks : Int, mesh : Mesh ) {

        var v = 0;
        var stacksGenerated = stacks + 1;
        for( i in 0...slices ) { // # for each horizontal slice...
            for( j in 0...stacks ) { // # ...iterate through vertical wedges
                var next = (j + 1);
                /*
                  ^  p2 --- p3
                  |  |       |
                  v  p0 --- p1
                   u --->
                */
                var i0 = v + j;
                var i1 = v + next;
                var i2 = v + j + stacksGenerated;
                var i3 = v + next + stacksGenerated;

                switch( mesh.elementType ) {

                    case Quad:
                        // Sys.println('${i0}, ${i1}, ${i3}, ${i2}');
                        mesh.indices.push(i0);
                        mesh.indices.push(i1);
                        mesh.indices.push(i3);
                        mesh.indices.push(i2);

                    case Triangle:
                    // TODO eliminate zero-surface triangles
                        // Sys.println('${i0}, ${i1}, ${i2}');
                        // Sys.println('${i1}, ${i3}, ${i2}');
                        // tri A
                        mesh.indices.push(i0);
                        mesh.indices.push(i1);
                        mesh.indices.push(i2);
                        // tri B
                        mesh.indices.push(i1);
                        mesh.indices.push(i3);
                        mesh.indices.push(i2);

                    case Line:
                    // TODO eliminate zero-length lines
                        // Sys.println('${i0}, ${i1}');
                        mesh.indices.push(i0);
                        mesh.indices.push(i1);
                        // Sys.println('${i0}, ${i2}');
                        mesh.indices.push(i0);
                        mesh.indices.push(i2);
                        if( next == stacks ) {
                            // Sys.println('${i1}, ${i3}');
                            mesh.indices.push(i1);
                            mesh.indices.push(i3);
                        }
                        if( i == slices - 1 ) {
                            // Sys.println('${i2}, ${i3}');
                            mesh.indices.push(i2);
                            mesh.indices.push(i3);
                        }

                    case Point:
                    // TODO eliminate coincident points
                        // Sys.println('${i0}');
                        mesh.indices.push(i0);
                        if( next == stacks ) {
                            // Sys.println('${i1}');
                            mesh.indices.push(i1);
                        }
                        if( i == slices - 1 ) {
                            // Sys.println('${i2}');
                            mesh.indices.push(i2);
                            if( next == stacks ) {
                                // Sys.println('${i3}');
                                mesh.indices.push(i3);
                            }
                        }

                    default:
                }
            }
            v = v + stacksGenerated;
        }
    }

    private static function makeConnectedMesh( slices : Int, stacks : Int, mesh : Mesh ) {

        var v = 0;
        var stacksGenerated = stacks;
        for( i in 0...slices ) { // # for each horizontal slice...
            for( j in 0...stacks ) { // # ...iterate through vertical wedges
                var next = (j + 1) % stacksGenerated;
                /*
                  ^  p2 --- p3 ... (p2)
                  |  |       |      |
                  v  p0 --- p1 ... (p0)
                   u --->
                */
                var i0 = v + j;
                var i1 = v + next;
                var i2 = v + j + stacksGenerated;
                var i3 = v + next + stacksGenerated;

                switch( mesh.elementType ) {

                    case Quad:
                        // Sys.println('${i0}, ${i1}, ${i3}, ${i2}');
                        mesh.indices.push(i0);
                        mesh.indices.push(i1);
                        mesh.indices.push(i3);
                        mesh.indices.push(i2);

                    case Triangle:
                    // TODO eliminate zero-surface triangles
                        // Sys.println('${i0}, ${i1}, ${i2}');
                        // Sys.println('${i1}, ${i3}, ${i2}');
                        // tri A
                        mesh.indices.push(i0);
                        mesh.indices.push(i1);
                        mesh.indices.push(i2);
                        // tri B
                        mesh.indices.push(i1);
                        mesh.indices.push(i3);
                        mesh.indices.push(i2);

                    case Line:
                    // TODO eliminate zero-length lines
                        // Sys.println('${i0}, ${i1}');
                        mesh.indices.push(i0);
                        mesh.indices.push(i1);
                        // Sys.println('${i0}, ${i2}');
                        mesh.indices.push(i0);
                        mesh.indices.push(i2);
                        if( i == slices - 1 ) {
                            // Sys.println('${i2}, ${i3}');
                            mesh.indices.push(i2);
                            mesh.indices.push(i3);
                        }

                    case Point:
                    // TODO eliminate coincident points
                        // Sys.println('${i0}');
                        mesh.indices.push(i0);
                        if( i == slices - 1 ) {
                            // Sys.println('${i2}');
                            mesh.indices.push(i2);
                        }

                    default:
                }
            }
            v = v + stacksGenerated;
        }
    }
}

class PlanarGenerators {

    public static function xyplane( u : Float, v : Float ) : Vec3 {

        var x = u - 0.5;
        var y = v - 0.5;
        var z = 0.0;

        return [x, y, z];
    }

    public static function zxplane( u : Float, v : Float ) : Vec3 {

        var x = v - 0.5;
        var y = 0.0;
        var z = u - 0.5;

        return [x, y, z];
    }

    public static function yzplane( u : Float, v : Float ) : Vec3 {

        var x = 0.0;
        var y = u - 0.5;
        var z = v - 0.5;

        return [x, y, z];
    }
}

class SphericalGenerators {

    public static function sphere( theta : Float, phi : Float ) : Vec3 {

        var x = Math.cos(theta) * Math.sin(phi);
        var y = Math.sin(theta) * Math.sin(phi);
        // we have to invert z coordinate value, otherwise normals will not be correct
        var z = -Math.cos(phi);

        return [x, y, z];
    }
}

class CylindricalGenerators {

    public static function cylinder( theta : Float, zc : Float ) : Vec3 {

        var x = Math.cos( theta );
        var y = Math.sin( theta );
        var z = zc - 0.5;

        return [x, y, z];
    }

    public static function cone( theta : Float, zc : Float ) : Vec3 {

        var x = zc * Math.cos( theta );
        var y = zc * Math.sin( theta );
        var z = zc;

        return [x, y, z];
    }

    public static function parabola( theta : Float, zc : Float ) : Vec3 {

        var r = Math.sqrt( zc );
        var x = r * Math.cos( theta );
        var y = r * Math.sin( theta );
        var z = zc;

        return [x, y, z];
    }

    public static function circle( theta : Float, zc : Float ) : Vec3 {

        var x = zc * Math.cos( theta );
        var y = zc * Math.sin( theta );
        var z = 0.0;

        return [x, y, z];
    }
}
