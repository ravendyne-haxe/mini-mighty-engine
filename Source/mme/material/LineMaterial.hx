package mme.material;

import mme.core.Material;
import mme.core.RGBAColor;
import mme.g2d.material.Line2DMaterial.LineBlending;


class LineMaterial extends Material {

    public var thickness : Float;
    public var inner : Float;
    public var edgeColor : RGBAColor;
    public var lineBlending : LineBlending;
    public var opacity : Float;

    public function new( ?color : RGBAColor, thickness : Float = 2.0, ?edgeColor : RGBAColor ) {

        super();

        if( color == null ) color = 0x000000ff;

        emissiveColor = color;
        this.edgeColor = edgeColor == null ? color : edgeColor;

        this.thickness = thickness;
        inner = 0.7;

        lineBlending = Smooth;
        opacity = 1.0;
    }
}
