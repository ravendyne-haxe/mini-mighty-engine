package mme.material;

import mme.core.RGBAColor;


class NamedColor {
    /**
     * The color _aliceblue_ with an RGB value of #f0f8ff
     * <div style="border:1px solid black;width:40px;height:20px;background-color:#f0f8ff;float:right;margin: 0 10px 0 0"></div><br/><br/>
     */
    public static var aliceblue : RGBAColor = 0xf0f8ffff;
    /**
     * RGB value of #faebd7
     * <div style="border:1px solid black;width:40px;height:20px;background-color:#faebd7;float:right;margin: 0 10px 0 0"></div><br/><br/>
     */
    public static var antiquewhite : RGBAColor = 0xfaebd7ff;
    public static var aqua : RGBAColor = 0x00ffffff;
    public static var aquamarine : RGBAColor = 0x7fffd4ff;
    public static var azure : RGBAColor = 0xf0ffffff;
    public static var beige : RGBAColor = 0xf5f5dcff;
    public static var bisque : RGBAColor = 0xffe4c4ff;
    public static var black : RGBAColor = 0x000000ff;
    public static var blanchedalmond : RGBAColor = 0xffebcdff;
    public static var blue : RGBAColor = 0x0000ffff;
    public static var blueviolet : RGBAColor = 0x8a2be2ff;
    public static var brown : RGBAColor = 0xa52a2aff;
    public static var burlywood : RGBAColor = 0xdeb887ff;
    public static var cadetblue : RGBAColor = 0x5f9ea0ff;
    public static var chartreuse : RGBAColor = 0x7fff00ff;
    public static var chocolate : RGBAColor = 0xd2691eff;
    public static var coral : RGBAColor = 0xff7f50ff;
    public static var cornflowerblue : RGBAColor = 0x6495edff;
    public static var cornsilk : RGBAColor = 0xfff8dcff;
    public static var crimson : RGBAColor = 0xdc143cff;
    public static var cyan : RGBAColor = 0x00ffffff;
    public static var darkblue : RGBAColor = 0x00008bff;
    public static var darkcyan : RGBAColor = 0x008b8bff;
    public static var darkgoldenrod : RGBAColor = 0xb8860bff;
    public static var darkgray : RGBAColor = 0xa9a9a9ff;
    public static var darkgreen : RGBAColor = 0x006400ff;
    public static var darkgrey : RGBAColor = 0xa9a9a9ff;
    public static var darkkhaki : RGBAColor = 0xbdb76bff;
    public static var darkmagenta : RGBAColor = 0x8b008bff;
    public static var darkolivegreen : RGBAColor = 0x556b2fff;
    public static var darkorange : RGBAColor = 0xff8c00ff;
    public static var darkorchid : RGBAColor = 0x9932ccff;
    public static var darkred : RGBAColor = 0x8b0000ff;
    public static var darksalmon : RGBAColor = 0xe9967aff;
    public static var darkseagreen : RGBAColor = 0x8fbc8fff;
    public static var darkslateblue : RGBAColor = 0x483d8bff;
    public static var darkslategray : RGBAColor = 0x2f4f4fff;
    public static var darkslategrey : RGBAColor = 0x2f4f4fff;
    public static var darkturquoise : RGBAColor = 0x00ced1ff;
    public static var darkviolet : RGBAColor = 0x9400d3ff;
    public static var deeppink : RGBAColor = 0xff1493ff;
    public static var deepskyblue : RGBAColor = 0x00bfffff;
    public static var dimgray : RGBAColor = 0x696969ff;
    public static var dimgrey : RGBAColor = 0x696969ff;
    public static var dodgerblue : RGBAColor = 0x1e90ffff;
    public static var firebrick : RGBAColor = 0xb22222ff;
    public static var floralwhite : RGBAColor = 0xfffaf0ff;
    public static var forestgreen : RGBAColor = 0x228b22ff;
    public static var fuchsia : RGBAColor = 0xff00ffff;
    public static var gainsboro : RGBAColor = 0xdcdcdcff;
    public static var ghostwhite : RGBAColor = 0xf8f8ffff;
    public static var gold : RGBAColor = 0xffd700ff;
    public static var goldenrod : RGBAColor = 0xdaa520ff;
    public static var gray : RGBAColor = 0x808080ff;
    public static var green : RGBAColor = 0x008000ff;
    public static var greenyellow : RGBAColor = 0xadff2fff;
    public static var grey : RGBAColor = 0x808080ff;
    public static var honeydew : RGBAColor = 0xf0fff0ff;
    public static var hotpink : RGBAColor = 0xff69b4ff;
    public static var indianred : RGBAColor = 0xcd5c5cff;
    public static var indigo : RGBAColor = 0x4b0082ff;
    public static var ivory : RGBAColor = 0xfffff0ff;
    public static var khaki : RGBAColor = 0xf0e68cff;
    public static var lavender : RGBAColor = 0xe6e6faff;
    public static var lavenderblush : RGBAColor = 0xfff0f5ff;
    public static var lawngreen : RGBAColor = 0x7cfc00ff;
    public static var lemonchiffon : RGBAColor = 0xfffacdff;
    public static var lightblue : RGBAColor = 0xadd8e6ff;
    public static var lightcoral : RGBAColor = 0xf08080ff;
    public static var lightcyan : RGBAColor = 0xe0ffffff;
    public static var lightgoldenrodyellow : RGBAColor = 0xfafad2ff;
    public static var lightgray : RGBAColor = 0xd3d3d3ff;
    public static var lightgreen : RGBAColor = 0x90ee90ff;
    public static var lightgrey : RGBAColor = 0xd3d3d3ff;
    public static var lightpink : RGBAColor = 0xffb6c1ff;
    public static var lightsalmon : RGBAColor = 0xffa07aff;
    public static var lightseagreen : RGBAColor = 0x20b2aaff;
    public static var lightskyblue : RGBAColor = 0x87cefaff;
    public static var lightslategray : RGBAColor = 0x778899ff;
    public static var lightslategrey : RGBAColor = 0x778899ff;
    public static var lightsteelblue : RGBAColor = 0xb0c4deff;
    public static var lightyellow : RGBAColor = 0xffffe0ff;
    public static var lime : RGBAColor = 0x00ff00ff;
    public static var limegreen : RGBAColor = 0x32cd32ff;
    public static var linen : RGBAColor = 0xfaf0e6ff;
    public static var magenta : RGBAColor = 0xff00ffff;
    public static var maroon : RGBAColor = 0x800000ff;
    public static var mediumaquamarine : RGBAColor = 0x66cdaaff;
    public static var mediumblue : RGBAColor = 0x0000cdff;
    public static var mediumorchid : RGBAColor = 0xba55d3ff;
    public static var mediumpurple : RGBAColor = 0x9370dbff;
    public static var mediumseagreen : RGBAColor = 0x3cb371ff;
    public static var mediumslateblue : RGBAColor = 0x7b68eeff;
    public static var mediumspringgreen : RGBAColor = 0x00fa9aff;
    public static var mediumturquoise : RGBAColor = 0x48d1ccff;
    public static var mediumvioletred : RGBAColor = 0xc71585ff;
    public static var midnightblue : RGBAColor = 0x191970ff;
    public static var mintcream : RGBAColor = 0xf5fffaff;
    public static var mistyrose : RGBAColor = 0xffe4e1ff;
    public static var moccasin : RGBAColor = 0xffe4b5ff;
    public static var navajowhite : RGBAColor = 0xffdeadff;
    public static var navy : RGBAColor = 0x000080ff;
    public static var oldlace : RGBAColor = 0xfdf5e6ff;
    public static var olive : RGBAColor = 0x808000ff;
    public static var olivedrab : RGBAColor = 0x6b8e23ff;
    public static var orange : RGBAColor = 0xffa500ff;
    public static var orangered : RGBAColor = 0xff4500ff;
    public static var orchid : RGBAColor = 0xda70d6ff;
    public static var palegoldenrod : RGBAColor = 0xeee8aaff;
    public static var palegreen : RGBAColor = 0x98fb98ff;
    public static var paleturquoise : RGBAColor = 0xafeeeeff;
    public static var palevioletred : RGBAColor = 0xdb7093ff;
    public static var papayawhip : RGBAColor = 0xffefd5ff;
    public static var peachpuff : RGBAColor = 0xffdab9ff;
    public static var peru : RGBAColor = 0xcd853fff;
    public static var pink : RGBAColor = 0xffc0cbff;
    public static var plum : RGBAColor = 0xdda0ddff;
    public static var powderblue : RGBAColor = 0xb0e0e6ff;
    public static var purple : RGBAColor = 0x800080ff;
    public static var rebeccapurple : RGBAColor = 0x663399ff;
    public static var red : RGBAColor = 0xff0000ff;
    public static var rosybrown : RGBAColor = 0xbc8f8fff;
    public static var royalblue : RGBAColor = 0x4169e1ff;
    public static var saddlebrown : RGBAColor = 0x8b4513ff;
    public static var salmon : RGBAColor = 0xfa8072ff;
    public static var sandybrown : RGBAColor = 0xf4a460ff;
    public static var seagreen : RGBAColor = 0x2e8b57ff;
    public static var seashell : RGBAColor = 0xfff5eeff;
    public static var sienna : RGBAColor = 0xa0522dff;
    public static var silver : RGBAColor = 0xc0c0c0ff;
    public static var skyblue : RGBAColor = 0x87ceebff;
    public static var slateblue : RGBAColor = 0x6a5acdff;
    public static var slategray : RGBAColor = 0x708090ff;
    public static var slategrey : RGBAColor = 0x708090ff;
    public static var snow : RGBAColor = 0xfffafaff;
    public static var springgreen : RGBAColor = 0x00ff7fff;
    public static var steelblue : RGBAColor = 0x4682b4ff;
    public static var tan : RGBAColor = 0xd2b48cff;
    public static var teal : RGBAColor = 0x008080ff;
    public static var thistle : RGBAColor = 0xd8bfd8ff;
    public static var tomato : RGBAColor = 0xff6347ff;
    public static var turquoise : RGBAColor = 0x40e0d0ff;
    public static var violet : RGBAColor = 0xee82eeff;
    public static var wheat : RGBAColor = 0xf5deb3ff;
    public static var white : RGBAColor = 0xffffffff;
    public static var whitesmoke : RGBAColor = 0xf5f5f5ff;
    public static var yellow : RGBAColor = 0xffff00ff;
    public static var yellowgreen : RGBAColor = 0x9acd32ff;
}