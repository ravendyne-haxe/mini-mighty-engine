package mme.material;

import lime.graphics.Image;

import mme.core.Material;

class TextureMaterial extends Material {

    public function new( image : Image ) {

        super();

		textureImage = image;
    }
}
