package mme.material;

import lime.graphics.Image;

import mme.core.Material;

class FontAtlasMaterial extends TextureMaterial {

    public function new( atlasImage : Image ) {

        super( atlasImage );

        emissiveColor = NamedColor.black;
    }
}
