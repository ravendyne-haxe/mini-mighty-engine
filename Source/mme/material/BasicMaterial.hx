package mme.material;

import mme.core.Material;
import mme.core.RGBAColor;


class BasicMaterial extends Material {

	// default emissive color: fuchsia
    public function new( ?color : RGBAColor ) {

        if( color == null ) color = 0xff00ffff;

        super();

        emissiveColor = color;
    }
}
