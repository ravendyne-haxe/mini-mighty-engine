package mme.material;

import mme.core.Material;
import mme.core.RGBAColor;


class WireframeMaterial extends Material {

	// default emissive color: fuchsia
    public function new( ?color : RGBAColor ) {

        super();

        emissiveColor = color != null ? color : 0xff00ffff;
    }
}
