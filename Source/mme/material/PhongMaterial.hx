package mme.material;

import mme.core.Material;
import mme.core.RGBAColor;


class PhongMaterial extends Material {

	// default color: fuchsia
    public function new( ?color : RGBAColor, shininess : Float = 32.0 ) {

        super();

        if( color == null ) color = 0xff00ffff;

        emissiveColor = 0x000000ff;

        this.ka = color;
        this.kd = color;
        this.ks = [0.5, 0.5, 0.5];

        this.shininess = shininess;
    }
}
