package mme.g2d.material;

import mme.core.Material;
import mme.core.RGBAColor;


enum PointType {
    Circle;
    Square;
    Diamond;
}

class Point2DMaterial extends Material {

    public var edgeColor : RGBAColor;
    public var type ( default, null ) : PointType;

    public function new( ?color : RGBAColor, type : PointType = Circle, ?edgeColor : RGBAColor ) {

        super();

        if( color == null ) color = 0x000000ff;

        emissiveColor = color;
        this.edgeColor = edgeColor == null ? color : edgeColor;
        this.type = type;
    }
}
