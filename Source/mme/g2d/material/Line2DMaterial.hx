package mme.g2d.material;

import mme.util.geometry.line2d.LineStroker;
import mme.core.Material;
import mme.core.RGBAColor;


enum LineBlending {
    Sharp;
    Smooth;
    ThinLines;
}

class Line2DMaterial extends Material {

    public var inner : Float;
    public var edgeColor : RGBAColor;
    public var lineBlending : LineBlending;
    public var opacity : Float;

    public var lineCap : LineCapEnum;
    public var lineJoin : LineJoinEnum;
    public var lineInnerJoin : InnerJoinEnum;

    public var quickLine : Bool;

    public function new( ?color : RGBAColor, ?edgeColor : RGBAColor ) {

        super();

        if( color == null ) color = 0x000000ff;

        emissiveColor = color;
        this.edgeColor = edgeColor == null ? color : edgeColor;

        inner = 0.7;

        lineBlending = Smooth;
        opacity = 1.0;

        lineCap = butt_cap;
        lineJoin = miter_join;
        lineInnerJoin = inner_miter;

        quickLine = false;
    }
}
