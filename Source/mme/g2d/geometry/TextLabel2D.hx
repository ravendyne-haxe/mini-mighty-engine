package mme.g2d.geometry;

import mme.MMEngine;

import mme.core.RGBAColor;
import mme.core.SceneObject;
import mme.font.FontAtlas;


class TextLabel2D extends SceneObject {

    var fontAtlas : FontAtlas;

    public function new( text : String, vertical : Bool = false, ?color : RGBAColor, ?fontAtlas : FontAtlas ) {

        super();

        if( fontAtlas == null ) fontAtlas = MMEngine.currentFont;

        this.fontAtlas = fontAtlas;

        var txt = fontAtlas.renderText( text, vertical );
        mesh = txt.mesh;
        material = txt.material;

        if( color != null ) material.emissiveColor = color;
    }

    public function setText( text : String ) {

        var txt = fontAtlas.renderText( text );
        mesh.copyFrom( txt.mesh );

        needsUpdate();
    }
}
