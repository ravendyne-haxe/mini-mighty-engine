package mme.g2d.geometry;

import mme.math.glmatrix.Vec2;

import mme.core.SceneObject;
import mme.core.RGBAColor;
import mme.g2d.material.Point2DMaterial;


class PointCloud2D extends SceneObject {

    public var width : Float;
    public var height : Float;

    public function new( width : Float = 10.0, height : Float = 10.0, ?material : Point2DMaterial, ?color : RGBAColor ) {

        if( color != null ) {
            material = new Point2DMaterial( color );
        }
        if( material == null ) {
            material = new Point2DMaterial( 0x000000ff );
        }

        super( material );

        this.height = height;
        this.width = width;

        mesh.elementType = Point;
    }

    public function addPoint( point : Vec2 ) {

        mesh.vertices.push([ point.x, point.y, 0.0 ]);
        mesh.otherParams.push([ width, height ]);

        needsUpdate();

        return this;
    }

    public function addPoints( points : Array<Vec2> ) {

        for( p in points ) {
            mesh.vertices.push([ p.x, p.y, 0.0 ]);
            mesh.otherParams.push([ width, height ]);
        }

        needsUpdate();

        return this;
    }
}
