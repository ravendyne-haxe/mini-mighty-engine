package mme.g2d.geometry;

import mme.math.glmatrix.Vec2;

import mme.core.SceneObject;
import mme.core.RGBAColor;
import mme.g2d.material.Line2DMaterial;


class LineCollection2D extends SceneObject {

    public var lineMaterial ( default, null ) : Line2DMaterial;

    public var thickness ( get, set ) : Float;
    private var thickness_value : Float;

    public function new( ?lineColor : RGBAColor, thickness : Float = 2.0, ?edgeColor : RGBAColor, ?lineMaterial : Line2DMaterial ) {

        if( lineColor == null ) lineColor = 0x000000ff;

        if( lineMaterial == null )
            lineMaterial = new Line2DMaterial( lineColor, edgeColor );

        super( lineMaterial );

        this.lineMaterial = lineMaterial;

        mesh.elementType = Line;
        
        thickness_value = thickness;
    }

    private function set_thickness( value : Float ) : Float {

        thickness_value = value;
        this.needsUpdate();

        return thickness_value;
    }

    private function get_thickness() : Float {

        return thickness_value;
    }

    public function addLine( start : Vec2, end : Vec2 ) {

        mesh.vertices.push([ start.x, start.y, 0.0 ]);
        mesh.vertices.push([ end.x, end.y, 0.0 ]);

        needsUpdate();

        return this;
    }

    public function addLines( lines : Array< Array< Vec2 > > ) {

        for( lin in lines ) {
            mesh.vertices.push([ lin[ 0 ].x, lin[ 0 ].y, 0.0 ]);
            mesh.vertices.push([ lin[ 1 ].x, lin[ 1 ].y, 0.0 ]);
        }

        needsUpdate();

        return this;
    }
}
