package mme.g2d.geometry;

import mme.util.Utils;
import mme.util.PathUtils;
import mme.math.glmatrix.Vec2;

import mme.core.Group;
import mme.core.SceneObject;
import mme.core.RGBAColor;
import mme.g2d.material.Line2DMaterial;


class PolyLine2D extends SceneObject {

    public var isClosed : Bool;

    public var lineMaterial ( default, null ) : Line2DMaterial;

    public var thickness ( get, set ) : Float;
    private var thickness_value : Float;

    public function new( ?lineColor : RGBAColor, thickness : Float = 2.0, ?edgeColor : RGBAColor, ?lineMaterial : Line2DMaterial ) {

        if( lineColor == null ) lineColor = 0x000000ff;

        if( lineMaterial == null )
            lineMaterial = new Line2DMaterial( lineColor, edgeColor );

        super( lineMaterial );

        this.lineMaterial = lineMaterial;

        // as far as Mesh is concerned, our polyline
        // is just a collection of points.
        // Mesh.Line element type assumes two vertices per
        // line, which is not the case here, so we use Point
        mesh.elementType = Point;
        isClosed = false;
        
        thickness_value = thickness;
    }

    private function set_thickness( value : Float ) : Float {

        thickness_value = value;
        this.needsUpdate();

        return thickness_value;
    }

    private function get_thickness() : Float {

        return thickness_value;
    }

    public function addPoint( point : Vec2 ) {

        mesh.vertices.push([ point.x, point.y, 0.0 ]);

        needsUpdate();

        return this;
    }

    public function addPoints( points : Array<Vec2> ) {

        for( p in points )
            mesh.vertices.push([ p.x, p.y, 0.0 ]);

        needsUpdate();

        return this;
    }

    public function close() {

        isClosed = true;
        if( mesh.vertices.length == 2 ) isClosed = false;
    }

    public function encode() : String {

        var fixedToString = function( value : Float ) {
            // not the best implementation ever,
            // but we don't need subatomic precision here
            var precision = 100;
            var dec = Std.int( value );
            var frac = Std.int( Math.abs( (value - dec) * precision ) );
            return '$dec.$frac';
        }

        var sb = new StringBuf();

        sb.add( "NaN,1.0;" );
        if( isClosed )
            sb.add( "NaN,-1.0;" );

        for( v in mesh.vertices ) {
            sb.add( fixedToString( v.x ) );
            sb.add( "," );
            sb.add( fixedToString( v.y ) );
            sb.add( ";" );
        }

        return sb.toString();
    }

    public static function decode( value : String, thickness : Float = 2.0, ?lineMaterial : Line2DMaterial ) : Group {

        var lines = value.split(";");
        var points : Array<Vec2> = [];

        for( line in lines ) {
            if( line == "" ) continue;

            var xy = line.split( "," );
            var x = Std.parseFloat( xy[ 0 ] );
            var y = Std.parseFloat( xy[ 1 ] );

            points.push([ x, y ]);
        }

        return PathUtils.convertPathPoints( points, thickness, lineMaterial );
    }
}
