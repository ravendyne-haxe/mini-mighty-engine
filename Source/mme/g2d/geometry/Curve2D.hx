package mme.g2d.geometry;

import mme.math.glmatrix.GLMatrix;
import mme.math.glmatrix.Vec2;
import mme.util.geometry.curve2d.Bezier3;
import mme.util.geometry.curve2d.Bezier4;
import mme.util.geometry.AggMath;


enum Curve2DType {
    Circle;
    Bezier3;
    Bezier4;
    Arc;
}


class Curve2D extends PolyLine2D { 

    public var curveType : Curve2DType;

    public function circle( center : Vec2, radius : Float ) {

        buildMeshCircle( center, radius );

        curveType = Circle;
        isClosed = true;
        needsUpdate();
    }

    public function arc( center : Vec2, radius : Float, startAngleDeg : Float, endAngleDeg : Float ) {

        buildMeshArc( center, radius, startAngleDeg, endAngleDeg );

        curveType = Arc;
        isClosed = false;
        needsUpdate();
    }

    public function bezier3( p0 : Vec2, p1 : Vec2, p2 : Vec2 ) {

        buildMeshBezier3( p0, p1, p2 );

        curveType = Bezier3;
        isClosed = false;
        needsUpdate();
    }

    public function bezier4( p0 : Vec2, p1 : Vec2, p2 : Vec2, p3 : Vec2 ) {

        buildMeshBezier4( p0, p1, p2, p3 );

        curveType = Bezier4;
        isClosed = false;
        needsUpdate();
    }

    function buildMeshCircle( center : Vec2, radius : Float ) {

        var segments = 20;

        var TWO_PI = 2.0 * Math.PI;
        var step = TWO_PI / segments;
        var angle = 0.0;

        while( angle < TWO_PI ) {

            var xc = center.x + radius * Math.cos( angle );
            var yc = center.y + radius * Math.sin( angle );

            mesh.vertices.push([ xc, yc, 0.0 ]);

            angle += step;
        }
    }

    function buildMeshArc( center : Vec2, radius : Float, startAngleDeg : Float, endAngleDeg : Float ) {

        if( startAngleDeg > endAngleDeg ) {
            var tmp = startAngleDeg; startAngleDeg = endAngleDeg; endAngleDeg = tmp;
        }

        var segments = 20;
        var TWO_PI = 2.0 * Math.PI;

        var startAngleRad = GLMatrix.toRadian( startAngleDeg );
        var endAngleRad = GLMatrix.toRadian( endAngleDeg );
        var deltaAngle = endAngleRad - startAngleRad;
        if( deltaAngle > TWO_PI ) {
            deltaAngle = deltaAngle - Math.floor( deltaAngle / TWO_PI ) * TWO_PI;
        }
        // adjust # of segments based on angle
        segments = Std.int( deltaAngle / TWO_PI * segments );


        var step = deltaAngle / segments;
        var angle = startAngleRad;

        while( angle <= endAngleRad ) {

            var xc = center.x + radius * Math.cos( angle );
            var yc = center.y + radius * Math.sin( angle );

            mesh.vertices.push([ xc, yc, 0.0 ]);

            angle += step;
        }
    }

    function buildMeshBezier3( p0 : Vec2, p1 : Vec2, p2 : Vec2 ) {

        var b3 = new Bezier3();
        var vc = new SimpleVertexConsumer();
        b3.bezier( vc, p0, p1, p2 );

        for( p in vc.vertices ) {
            mesh.vertices.push([ p.x , p.y, 0.0 ]);
        }
    }

    function buildMeshBezier4( p0 : Vec2, p1 : Vec2, p2 : Vec2, p3 : Vec2 ) {

        var b4 = new Bezier4();
        var vc = new SimpleVertexConsumer();
        b4.bezier( vc, p0, p1, p2, p3 );

        for( p in vc.vertices ) {
            mesh.vertices.push([ p.x , p.y, 0.0 ]);
        }
    }
}
