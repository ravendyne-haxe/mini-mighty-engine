package mme.font;

import mme.core.Mesh;


class FontUtil {

    public static function addMeshQuad( textMesh : Mesh, quadPositionX : Float, quadPositionY : Float, width : Float, height : Float ) {

        textMesh.vertices.push([ quadPositionX,         quadPositionY,          0 ]);
        textMesh.vertices.push([ quadPositionX + width, quadPositionY,          0 ]);
        textMesh.vertices.push([ quadPositionX + width, quadPositionY + height, 0 ]);
        textMesh.vertices.push([ quadPositionX,         quadPositionY + height, 0 ]);
    }

    public static function addQuadUVs( textMesh : Mesh, atlasX : Float, atlasY : Float, width : Float, height : Float, atlasImageWidth : Float, atlasImageHeight : Float ) {

        var u = atlasX / atlasImageWidth;
        var v = atlasY / atlasImageHeight;
        var du = width / atlasImageWidth;
        var dv = height / atlasImageHeight;

        textMesh.uvs.push([ u,      v + dv ]);
        textMesh.uvs.push([ u + du, v + dv ]);
        textMesh.uvs.push([ u + du, v ]);
        textMesh.uvs.push([ u,      v ]);
    }

    public static function addQuadNormals( textMesh : Mesh ) {

        textMesh.normals.push([ 0, 0, 1 ]);
        textMesh.normals.push([ 0, 0, 1 ]);
        textMesh.normals.push([ 0, 0, 1 ]);
        textMesh.normals.push([ 0, 0, 1 ]);
    }
}