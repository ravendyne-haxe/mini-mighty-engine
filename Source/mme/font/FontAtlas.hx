package mme.font;

import haxe.io.Bytes;
import haxe.io.BytesInput;
import haxe.io.BytesOutput;

import lime.utils.Log;
import lime.utils.UInt8Array;
import lime.graphics.PixelFormat;
import lime.graphics.ImageBuffer;
import lime.math.Vector2;
import lime.graphics.ImageType;
import lime.graphics.Image;
import lime.text.Font;

import mme.core.BoundingBox;
import mme.core.Mesh;
import mme.geometry.MeshGeometry;
import mme.material.FontAtlasMaterial;
import mme.format.fontatlas.Reader;
import mme.format.fontatlas.Writer;
import mme.util.Lzw;


typedef GlyphData = {
    var xpos : Int;
    var ypos : Int;
    var point : String;
    // glyph dimensions
    var width : Float;
    var height : Float;
    // horizontal layout metrics
    var advanceX : Float;
    var horizontalBearingX : Float;
    var horizontalBearingY : Float;
    // vertical layout metrics
    var advanceY : Float;
    var verticalBearingX : Float;
    var verticalBearingY : Float;
}

class FontAtlas {

    public var atlasImage ( default, null ) : Image;
    public var fontSize ( default, null ) : Float;

    var cache : Map< Int, GlyphData >;

    private function new() {
        cache = [];
    }

    // https://www.freetype.org/freetype2/docs/glyphs/glyphs-3.html
    // https://www.freetype.org/freetype2/docs/tutorial/step1.html
    public function renderText( letters : String, vertical : Bool = false, ?bb : BoundingBox ) : MeshGeometry {

        var penPositionX = 0.0;
        var penPositionY = 0.0;

        // if( Log.level == DEBUG && bb == null ) bb = new BoundingBox();

        var textMesh = new Mesh();
        textMesh.elementType = Quad;

        for( idx in 0...letters.length ) {

            var point = letters.charAt(idx);
            // Log.debug( '==========> point "$point" <==========');

            var glyphData = cache.get( point.charCodeAt(0) );
            if( glyphData == null ) {
                // Log.debug( '==========> !!! ATLAS HAS NO GLYPH !!! <==========');
                continue;
            }

            var height = glyphData.height;
            var width = glyphData.width;
            // horizontal layout metrics
            var advanceX = glyphData.advanceX;
            var horizontalBearingX = glyphData.horizontalBearingX;
            var horizontalBearingY = glyphData.horizontalBearingY;
            // vertical layout metrics
            var advanceY = glyphData.advanceY;
            var verticalBearingX = glyphData.verticalBearingX;
            var verticalBearingY = glyphData.verticalBearingY;

            var quadPositionX = penPositionX;
            var quadPositionY = penPositionY;
            if( vertical ) {
                quadPositionX += verticalBearingX;
                quadPositionY += verticalBearingY;
            } else {
                quadPositionX += horizontalBearingX;
                // subtract height here, because our coordinates have origin in bottom-left corner
                quadPositionY += horizontalBearingY - height;
            }

            FontUtil.addMeshQuad( textMesh, quadPositionX, quadPositionY, width, height );
            FontUtil.addQuadUVs( textMesh, glyphData.xpos, glyphData.ypos, glyphData.width, glyphData.height, atlasImage.width, atlasImage.height );
            FontUtil.addQuadNormals( textMesh );

            if( bb != null ) {
                bb.extendToVertex([ quadPositionX, quadPositionY, 0 ]);
                bb.extendToVertex([ quadPositionX + width, quadPositionY + height, 0 ]);
            }

            // Log.debug( "---> glyph metrics" );
            // Log.debug( "height: " + height + ", width: " + width );
            // Log.debug( "advance.x: " + advanceX + ", advance.y: " + advanceY );
            // Log.debug( "horizontalBearing.x: " + horizontalBearingX + ", horizontalBearing.y: " + horizontalBearingY );
            // Log.debug( "verticalBearing.x: " + verticalBearingX + ", verticalBearing.y: " + verticalBearingY );
            // Log.debug( "penPositionX: " + penPositionX + ", quadPositionX: " + quadPositionX  + ", quadPositionY: " + quadPositionY );

            if( vertical )
                // subtract here, because our coordinates have origin in bottom-left corner
                penPositionY -= advanceY;
            else
                penPositionX += advanceX;
        }

        // if( bb != null ) Log.debug( "bounding box min: " + bb.minPosition + ", bounding box max: " + bb.maxPosition );

        return new MeshGeometry( textMesh, new FontAtlasMaterial( atlasImage ) );
    }

    public static function fromBytes( data : Bytes ) : FontAtlas {

        var fontAtlas = new FontAtlas();

        data = Lzw.decompress( data );

        var dataInput = new BytesInput( data );

        var reader = new Reader( dataInput );
        var atlasData = reader.read();

        // fill in cache
        for( glyph in atlasData.glyphs ) {
            fontAtlas.cache.set( glyph.point.charCodeAt(0), glyph );
        }

        // create atlas image and texture
        var dataArray = UInt8Array.fromBytes( atlasData.atlas );
        var imageBuffer = new ImageBuffer( dataArray, atlasData.atlasWidth, atlasData.atlasHeight, 32, PixelFormat.RGBA32 );

        fontAtlas.atlasImage = new Image( imageBuffer );

        fontAtlas.fontSize = atlasData.fontSize;

        return fontAtlas;
    }

    public function toBytes() : Bytes {

        var dataOutput = new BytesOutput();

        var writer = new Writer( dataOutput );

        var glyphs: Array< GlyphData > = [];
        for( glyph_id in cache.keys() ) {
            glyphs.push( cache.get( glyph_id ) );
        }

        writer.write({
            fontSize: fontSize,
            glyphs: glyphs,
            atlasWidth: atlasImage.width,
            atlasHeight: atlasImage.height,
            atlas: atlasImage.buffer.data.toBytes(),
        });

        return Lzw.compress( dataOutput.getBytes() );
    }

    public static function forFont( font : Font, content : String ) : FontAtlas {

        var fontAtlas = new FontAtlas();
        var fontSource = new FontSource( font );
        fontAtlas.fontSize = fontSource.fontSize;

        var sizeSpan = new BoundingBox();

        for( idx in 0...content.length ) {

            var point = content.charAt( idx );
            // Log.debug('gauging point "${point}"...');
            var pointGlyph = fontSource.getGlyphData( point );

            var width = pointGlyph.metrics.width;
            var height = pointGlyph.metrics.height;
            // Log.debug('width ${width}, height ${height}');

            sizeSpan.extendToVertex([ width, height, 0 ]);
        }

        Log.debug('min glyph size = ${sizeSpan.minPosition}');
        Log.debug('max glyph size = ${sizeSpan.maxPosition}');

        var atlasXCount = Std.int( Math.ceil( Math.sqrt( content.length ) ) );

        Log.debug('content.length = ${content.length}');
        Log.debug('atlasXCount = ${atlasXCount}');

        // pad with 2 pixels
        var atlasXStep = Std.int( sizeSpan.maxPosition.x ) + 2;
        var atlasYStep = Std.int( sizeSpan.maxPosition.y ) + 2;
        var atlasWidth = atlasXCount * atlasXStep;
        var atlasHeight = atlasXCount * atlasYStep;

        fontAtlas.atlasImage = new Image( 0, 0, atlasWidth, atlasHeight, ImageType.DATA );
        Log.debug('atlasImage [${atlasWidth}, ${atlasHeight}]');

        var xpos = 0;
        var ypos = 0;
        for( idx in 0...content.length ) {

            var point = content.charAt( idx );
            var pointGlyph = fontSource.getGlyphData( point );

            if( pointGlyph.glyphImage == null ) {

                var width = Std.int( pointGlyph.metrics.width );
                var height = Std.int( pointGlyph.metrics.height );

                if( width == 0 && height == 0 ) {
                    Log.debug('skipping no-image, no-size "${point}"...');
                    continue;
                }

                var color = 0x000000ff;
                if( point == " " ) color = 0x00000000;
                pointGlyph.glyphImage = new Image(0,0,width,height, color, ImageType.DATA);
                // this is no-image glyph and we are conjuring up an image for it
                // so we have to adjust bearings here
                pointGlyph.metrics.horizontalBearingY = height;
            }
            Log.debug('atlasify "${point}"...');

            fontAtlas.atlasImage.copyPixels( pointGlyph.glyphImage, pointGlyph.glyphImage.rect, new Vector2( xpos, ypos ) );
            pointGlyph.xpos = xpos;
            pointGlyph.ypos = ypos;

            Log.debug('... @[$xpos, $ypos] x ${pointGlyph.glyphImage.rect}');

            fontAtlas.cache.set( point.charCodeAt(0), {
                xpos: xpos,
                ypos: ypos,
                point: point,
                width: pointGlyph.metrics.width,
                height: pointGlyph.metrics.height,
                advanceX: pointGlyph.metrics.advanceX,
                advanceY: pointGlyph.metrics.advanceY,
                horizontalBearingX: pointGlyph.metrics.horizontalBearingX,
                horizontalBearingY: pointGlyph.metrics.horizontalBearingY,
                verticalBearingX: pointGlyph.metrics.verticalBearingX,
                verticalBearingY: pointGlyph.metrics.verticalBearingY,
            });

            // advance to next position in atlas image
            xpos += atlasXStep;
            if( xpos >= atlasWidth ) {
                xpos = 0;
                ypos += atlasYStep;
            }
        }

        return fontAtlas;
    }
}
