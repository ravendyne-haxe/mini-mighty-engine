package mme.font;

import haxe.ds.Map;

import lime.utils.Log;
import lime.text.GlyphMetrics;
import lime.graphics.PixelFormat;
import lime.math.RGBA;
import lime.graphics.ImageBuffer;
import lime.utils.UInt8Array;
import lime.graphics.Image;
import lime.text.Font;

import mme.math.glmatrix.Vec3;
import mme.g2d.geometry.LineCollection2D;
import mme.material.BasicMaterial;
import mme.material.NamedColor;
import mme.material.FontAtlasMaterial;
import mme.geometry.MeshGeometry;
import mme.geometry.Plane;
import mme.core.Mesh;
import mme.core.BoundingBox;
import mme.core.Group;


typedef FSGlyphMetrics = {
    // glyph dimensions
    var width : Float;
    var height : Float;
    // horizontal layout metrics
    var advanceX : Float;
    var horizontalBearingX : Float;
    var horizontalBearingY : Float;
    // vertical layout metrics
    var advanceY : Float;
    var verticalBearingX : Float;
    var verticalBearingY : Float;
}

typedef FSFontGlyphData = {
    var metrics : FSGlyphMetrics;
    var glyphImage : Image;
    // atlas position
    var xpos : Int;
    var ypos : Int;
}

class FontSource {

    var glyphCache : Map< Int, FSFontGlyphData >;
    var font : Font;
    public var fontSize ( default, null ) : Int;

    public var verbose : Bool;

    public function new( font : Font, fontSize : Int = 48 ) {
        this.font = font;
        this.fontSize = fontSize;
        glyphCache = [];
        verbose = false;

        // warmup, dummy throwaway glyph
        // metrics is always off for the very first one ¯\_(ツ)_/¯
        loadGlyph(".");
    }

    public function getCahced( point: String ) : FSFontGlyphData {
        return glyphCache.get( point.charCodeAt(0) );
    }

    public function getGlyphData( point: String ) : FSFontGlyphData {

        var key = point.charCodeAt(0);

        if( ! glyphCache.exists( key ) ) {
            var gr = loadGlyph( point );
            glyphCache.set( key, gr );
            if( verbose ) Log.debug('Font cache miss: "$point"...');
        }

        return glyphCache[ key ];
    }

    function loadGlyph( point: String ) : FSFontGlyphData {

        var glyph = font.getGlyph( point );
        var glyphMetrics = font.getGlyphMetrics( glyph );
        if( glyphMetrics == null ) {
            if( verbose ) Log.debug( "---> !!! NO GLYPH METRICS AVAILABLE !!!");
            return {
                xpos: 0,
                ypos: 0,
                metrics: null,
                glyphImage: null
            };
        }

        var fontGlyphImage = font.renderGlyph( glyph, fontSize );
        if( fontGlyphImage == null ) {
            if( verbose ) Log.debug( "---> !!! NO GLYPH IMAGE AVAILABLE !!!");

            return {
                xpos: 0,
                ypos: 0,
                metrics: convertMetrics( glyphMetrics ),
                glyphImage: null
            };
        }

        // TODO: use 8-bpp images ?
        // workaround
        // 32-bpp, RGBA pixels
        var data = new UInt8Array( fontGlyphImage.width * fontGlyphImage.height * 4 );
        var imgbuf = new ImageBuffer( data, fontGlyphImage.width, fontGlyphImage.height, 32, PixelFormat.RGBA32 );
        var glyphImage = new Image( imgbuf );
        glyphImage.transparent = true;
        glyphImage.premultiplied = false;


        // var chars = " .:ioVM@";
        for( y in 0...fontGlyphImage.height ) {
            for( x in 0...fontGlyphImage.width ) {

                var pixel = fontGlyphImage.data[ y * fontGlyphImage.width + x ];
                // Sys.print( chars.charAt( pixel >> 5 ) );

                var color = RGBA.create( 0, 0, 0, pixel );
                glyphImage.setPixel32( x, y, color, PixelFormat.RGBA32 );
            }
            // Sys.println( chars.charAt(0) );
        }

        return {
            xpos: 0,
            ypos: 0,
            metrics: convertMetrics( glyphMetrics, glyphImage ),
            glyphImage: glyphImage
        };
    }

    function convertMetrics( glyphMetrics : GlyphMetrics, ?glyphImage : Image ) : FSGlyphMetrics {

            // var height = glyphMetrics.height / 64.0;
            // horizontal layout metrics
            var advanceX = glyphMetrics.advance.x / 64.0;
            var horizontalBearingX = glyphMetrics.horizontalBearing.x / 64.0;
            var horizontalBearingY = glyphMetrics.horizontalBearing.y / 64.0;
            // vertical layout metrics
            var advanceY = glyphMetrics.advance.y / 64.0;
            var verticalBearingX = glyphMetrics.verticalBearing.x / 64.0;
            var verticalBearingY = glyphMetrics.verticalBearing.y / 64.0;

            var width = advanceX - horizontalBearingX;
            var height = advanceY - verticalBearingY;

            if( glyphImage != null ) {
                width = glyphImage.width;
                height = glyphImage.height;
            }

        return {
            width: width,
            height: height,
            advanceX: advanceX,
            horizontalBearingX: horizontalBearingX,
            horizontalBearingY: horizontalBearingY,
            advanceY: advanceY,
            verticalBearingX: verticalBearingX,
            verticalBearingY: verticalBearingY
        };
    }

    // https://www.freetype.org/freetype2/docs/glyphs/glyphs-3.html
    // https://www.freetype.org/freetype2/docs/tutorial/step1.html
    /**
        NOTE: This one UPDATES font source cache if a glyph is missing.
    **/
    public static function renderText( fontSource : FontSource, letters : String, vertical : Bool = false, verbose : Bool = false, ?bb : BoundingBox ) : Group {

        var penPositionX = 0.0;
        var penPositionY = 0.0;

        var textGroup = new Group();
        if( verbose && bb == null ) bb = new BoundingBox();

        var textMeshReference = new Mesh( Quad );
        var textMeshMaterial = new BasicMaterial( NamedColor.yellow );
        var textMeshGeometry = new MeshGeometry( textMeshReference, textMeshMaterial );
        textMeshGeometry.translateZ( -50 );
        if( verbose ) textGroup.add( textMeshGeometry );

        var lines = null;
        var bboxLines = null;
        if( verbose ) {
            lines = new LineCollection2D( NamedColor.red );
            lines.translateZ( 10 );
            textGroup.add( lines );
            bboxLines = new LineCollection2D( NamedColor.blue );
            bboxLines.translateZ( 5 );
            textGroup.add( bboxLines );
        }

        for( idx in 0...letters.length ) {
            var point = letters.charAt(idx);
            if( verbose ) Log.debug( '==========> point "$point" <==========');

            var glyphData = fontSource.getGlyphData( point );

            var glyphMetrics = glyphData.metrics;
            if( glyphMetrics == null ) {
                continue;
            }

            var textMesh = new Mesh( Quad );

            var height = glyphMetrics.height;
            // horizontal layout metrics
            var advanceX = glyphMetrics.advanceX;
            var horizontalBearingX = glyphMetrics.horizontalBearingX;
            var horizontalBearingY = glyphMetrics.horizontalBearingY;
            // vertical layout metrics
            var advanceY = glyphMetrics.advanceY;
            var verticalBearingX = glyphMetrics.verticalBearingX;
            var verticalBearingY = glyphMetrics.verticalBearingY;
            if( verbose ) {
                Log.debug( "---> glyph metrics" );
                Log.debug( "height: " + height );
                Log.debug( "advance.x: " + advanceX + ", advance.y: " + advanceY );
                Log.debug( "horizontalBearing.x: " + horizontalBearingX + ", horizontalBearing.y: " + horizontalBearingY );
                Log.debug( "verticalBearing.x: " + verticalBearingX + ", verticalBearing.y: " + verticalBearingY );
            }

            var quadPositionX = penPositionX;
            var quadPositionY = penPositionY;
            if( vertical ) {
                quadPositionX += verticalBearingX;
                quadPositionY += verticalBearingY;
            } else {
                quadPositionX += horizontalBearingX;
                // subtract height here, because our coordinates have origin in bottom-left corner
                quadPositionY += horizontalBearingY - height;
            }
            if( verbose ) Log.debug( "penPositionX: " + penPositionX + ", quadPositionX: " + quadPositionX  + ", quadPositionY: " + quadPositionY );

            var width = advanceX - horizontalBearingX;

            var glyphImage = glyphData.glyphImage;
            if( glyphImage != null ) {

                var imgWidth = glyphImage.width;
                var imgHeight = glyphImage.height;
                if( verbose ) {
                    Log.debug( "---> glyph image");
                    Log.debug( "w: " + imgWidth + ", h: " + imgHeight );
                    Log.debug( "bpp: " + glyphImage.buffer.bitsPerPixel + ", len: " + glyphImage.data.length );
                    Log.debug( "format: " + glyphImage.buffer.format + ", premul: " + glyphImage.premultiplied );
                    Log.debug( "stride: " + glyphImage.buffer.stride + ", transp: " + glyphImage.transparent );
                }

                // if( verbose ) lines.addLine([ quadPositionX, quadPositionY ], [ quadPositionX + imgWidth, quadPositionY + imgHeight ]);
                if( verbose ) addBox( lines, [ quadPositionX, quadPositionY, 0 ], [ quadPositionX + imgWidth, quadPositionY + imgHeight, 0 ] );

                FontUtil.addMeshQuad( textMesh, quadPositionX, quadPositionY, width, height );
                FontUtil.addQuadUVs( textMesh, 0, 0, 1, 1, 1, 1 );
                FontUtil.addQuadNormals( textMesh );
                var textQuad = new MeshGeometry( textMesh, new FontAtlasMaterial( glyphImage ) );
                textGroup.add( textQuad );

                // update width to glyph image width
                width = imgWidth;

                FontUtil.addMeshQuad( textMeshReference, quadPositionX, quadPositionY, width, height );
                FontUtil.addQuadUVs( textMeshReference, 0, 0, 1, 1, 1, 1 );
                FontUtil.addQuadNormals( textMeshReference );
            }

            if( bb != null ) {
                bb.extendToVertex([ quadPositionX, quadPositionY, 0 ]);
                bb.extendToVertex([ quadPositionX + width, quadPositionY + height, 0 ]);
            }

            if( vertical )
                // subtract here, because our coordinates have origin in bottom-left corner
                penPositionY -= advanceY;
            else
                penPositionX += advanceX;
        }

        if( verbose ) addBox( bboxLines, bb.minPosition, bb.maxPosition );

        return textGroup;
    }

    static function addBox( lines : LineCollection2D, leftbottom : Vec3, topright : Vec3 ) {

        var bbWidth = topright.x - leftbottom.x;
        var bbHeight = topright.y - leftbottom.y;
        lines.addLine(
            [ leftbottom.x, leftbottom.y ],
            [ leftbottom.x, leftbottom.y + bbHeight ]
        );
        lines.addLine(
            [ leftbottom.x, leftbottom.y + bbHeight ],
            [ leftbottom.x + bbWidth, leftbottom.y + bbHeight ]
        );
        lines.addLine(
            [ leftbottom.x, leftbottom.y ],
            [ leftbottom.x + bbWidth, leftbottom.y ]
        );
        lines.addLine(
            [ leftbottom.x + bbWidth, leftbottom.y ],
            [ topright.x, topright.y ]
        );
    }
}
