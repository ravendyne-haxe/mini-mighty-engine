package mme.util;

import mme.g2d.material.Line2DMaterial;
import mme.core.BoundingBox;
import mme.core.Group;
import mme.util.geometry.path2d.Path2D;

import mme.core.Mesh;
import mme.g2d.geometry.PolyLine2D;

import mme.math.glmatrix.Vec2;

import mme.util.geometry.Triangulate;

using mme.math.glmatrix.Vec2Tools;


class PathUtils {

    public static function convertPath2D( path : Path2D, flipY : Bool = false, thickness : Float = 2.0, ?lineMaterial : Line2DMaterial ) : Group {
        var pathPoints = path.convert();
        return convertPathPoints( pathPoints, flipY, thickness, lineMaterial );
    }

    /**
        Converts array of `Vec2` points to a `Group` of `Polyline2D` objects.

        Array of points has elements of which some have specific meaning:

        - ( float, float ) -> add this point to current sub-path (polyline)
        - ( NaN, +1 ) -> start sub-path
        - ( NaN, -1 ) -> close sub-path

        Each sub-path **MUST** start with "start sub-path" value and **MAY** end with "close sub-path" value.
    **/
    public static function convertPathPoints( pathPoints : Array<Vec2>, flipY : Bool = false, thickness : Float = 2.0, ?lineMaterial : Line2DMaterial ) : Group {

        var group = new Group();
        var lines : PolyLine2D = null;

        var startSubPath = false;
        var closeSubPath = false;
        var prevPoint : Vec2 = null;

        for( p in pathPoints ) {

            if( Math.isNaN( p.x ) ) {
                // special commands
                if( p.y > 0 )
                    startSubPath = true;
                if( p.y < 0 )
                    closeSubPath = true;
            }

            if( startSubPath ) {

                if( lines != null ) {

                    if( closeSubPath ) lines.close();
                    group.add( lines );
                }

                lines = new PolyLine2D( thickness, lineMaterial );
                startSubPath = false;
                closeSubPath = false;
                prevPoint = null;

            }
            else if( closeSubPath ) {
                continue;
            }
            else {

                var shouldAddThePoint = lines != null && ( prevPoint == null || !p.equals( prevPoint ) );
                if( shouldAddThePoint ) lines.addPoint([ p.x, p.y ]);
                prevPoint = p;
            }
        }

        if( lines != null ) {

            if( closeSubPath ) lines.close();
            group.add( lines );

            lines = null;
        }

        if( flipY ) {
            var bb = BoundingBox.getFor( group );
            Utils.applyToSceneObjects( group, function( obj ) {
                for( v in obj.mesh.vertices )
                    v.y = bb.sizeY - v.y;
            } );
        }

        return group;
    }

    public static function triangulatePolyline( polyline : PolyLine2D ) : Mesh {

        var contour : Array<Vec2> = [];

        for( v in polyline.mesh.vertices ) {
            contour.push([ v.x, v.y ]);
        }

        return triangulateContour( contour );
    }

    public static function triangulateContour( contour : Array<Vec2> ) : Mesh {

        var result : Array<Int> = [];

        if( ! Triangulate.process( contour, result ) ) {
            throw "triangulation was not successful";
            // return null;
        }

        var mesh = new Mesh();

        for( cp in contour ) {
            mesh.vertices.push([ cp.x, cp.y, 0 ]);
            mesh.uvs.push([ 0, 0 ]);
            mesh.normals.push([ 0, 0, 1 ]);
        }

        for( idx in result ) {
            mesh.indices.push( idx );
        }

        return mesh;
    }
}