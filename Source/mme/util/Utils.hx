package mme.util;

import lime.utils.Log;

import mme.core.SceneObject;
import mme.core.SceneNode;
import mme.core.Mesh;

import mme.math.glmatrix.Mat4;
import mme.math.glmatrix.Vec3;

using mme.math.glmatrix.Vec3Tools;
using mme.math.glmatrix.Mat4Tools;


class Utils {

    public static function printMesh( mesh : Mesh ) { 
        var idx;

        Log.info( 'element type: ${mesh.elementType}' );
        Log.info( 'indexed: ${mesh.isIndexed}' );
        Log.info( 'vertices:' );
        idx = 0;
        for( v in mesh.vertices )
            Log.info( '${idx++}: ${v}, ' );
        Log.info( '' );

        Log.info( 'uvs:' );
        idx = 0;
        for( v in mesh.uvs )
            Log.info( '${idx++}: ${v}, ' );
        Log.info( '' );

        Log.info( 'normals:' );
        idx = 0;
        for( v in mesh.normals )
            Log.info( '${idx++}: ${v}, ' );
        Log.info( '' );

        Log.info( 'indices: ${mesh.indices}' );
    }

    public static function applyToSceneObjects( root : SceneNode, fn : SceneObject -> Void ) {

        if( Std.is( root, SceneObject ) ) {
            var obj : SceneObject = cast root;
            fn( obj );
        }

        for( child in root.children ) {
            applyToSceneObjects( child, fn );
        }
    }

    public static function scaleAllTo( sceneNode : SceneNode, scale : Vec3 ) {

        applyToSceneObjects( sceneNode, function( obj ) obj.mesh.scaleTo( scale.x, scale.y, scale.z ) );
    }

    public static function moveAllTo( sceneNode : SceneNode, position : Vec3 ) {

        applyToSceneObjects( sceneNode, function( obj ) obj.mesh.moveTo( position ) );
    }

    public static function rotationFromTo( from : Vec3, to : Vec3 ) : Mat4 {

        if( from.equals( to ) ) {
            // aligned in same direction
            return Mat4Tools.identity();
        }

        if( from.equals( to.negate() ) ) {
            // aligned in opposite directions
            var neg = Mat4Tools.identity();
            neg[0] = -1;
            neg[5] = -1;
            neg[10] = -1;
            neg[15] = 1;
            return neg;
        }

        var nf = from.normalize();
        var nt = to.normalize();
        var dot = nf.dot( nt );

        var angle = Math.acos( dot );
        var axis = nf.cross( nt );

        return Mat4.fromRotation( angle, axis );
    }
}
