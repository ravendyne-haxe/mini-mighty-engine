package mme.util.loader;

import mme.math.glmatrix.Vec3;
import mme.format.stl.Data.Point3D;
import lime.utils.Bytes;
import lime.utils.Assets;

import mme.core.Mesh;


class StlProcessor {

    public var vertices : Array< Vec3 >;
    public var normals : Array< Vec3 >;

    private var currentVertexNormal : Vec3;
    private var currentFace : Array< Array<Float> >;

    public function new() {

        vertices = [];
        normals = [];

        currentVertexNormal = new Vec3();
        currentFace = [];
    }

    public function buildMesh() : Mesh {

        var mesh = new Mesh();

        mesh.vertices = vertices;
        mesh.normals = normals;

        return mesh;
    }

    public function processVertex( v : Array< Float > ) : Void {

        currentFace.push( v );

        if( currentFace.length == 3 ) {
            vertices.push( currentFace[ 0 ] );
            vertices.push( currentFace[ 1 ] );
            vertices.push( currentFace[ 2 ] );

            normals.push( currentVertexNormal );
            normals.push( currentVertexNormal );
            normals.push( currentVertexNormal );
        }
    }

    public function processVertexNormal( vn : Array< Float > ) : Void {

        currentVertexNormal = vn.copy();
        currentFace = [];
    }

    public function processVertexTexture( vt : Array< Float > ) : Void {}
    public function processFace( f : Array< Array< Null< Int > > >  ) : Void {}
    public function processGroup( g : Array< String > ) : Void {}
}


class StlLoader {

    public function new() {

    }

    public function load( assetPath : String ) : Mesh {

        var content : Bytes = Assets.getBytes( assetPath );

        var i = new haxe.io.BytesInput( content );
        var stl = new mme.format.stl.Reader( i ).read();

        var processor = new StlProcessor();

        for( idx in 0...stl.faces.length ) {

            var face = stl.faces[ idx ];

            processor.processVertexNormal( pointToArray( face.normal ) );
            processor.processVertex( pointToArray( face.p0 ) );
            processor.processVertex( pointToArray( face.p1 ) );
            processor.processVertex( pointToArray( face.p2 ) );
        }

        return processor.buildMesh();
    }

    inline function pointToArray( point : Point3D ) : Array<Float> {
        return [
            point.x,
            point.y,
            point.z,
        ];
    }
}
