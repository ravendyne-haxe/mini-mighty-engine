package mme.util.loader;

import mme.core.RGBAColor;
import mme.material.NamedColor;
import mme.material.PhongMaterial;
import mme.core.Material;
import haxe.io.Bytes;

import mme.math.glmatrix.Vec3;


class MtlProcessorImpl {

    var ka : Vec3;
    var kd : Vec3;
    var ks : Vec3;
    var ke : Vec3;
    var shininess : Float;
    var opacity : Float;

    var currentMaterialName : String;

    public var materials : Map< String, Material >;

    public function new() {

        currentMaterialName = null;
        materials = [];
        reset();
    }

    function reset() {

        shininess = 32.0;
        opacity = 1.0;
        ka = [1,1,1];
        kd = [1,1,1];
        ks = [1,1,1];
        ke = null;
    }

    function buildMaterial() : Material {

        var material = new PhongMaterial( NamedColor.white );
        material.shininess = shininess;
        material.ka = ka;
        material.kd = kd;
        material.ks = ks;
        if( ke != null ) {
            material.emissiveColor = ke;
        }

        return material;
    }

    public function processNewMtl( newmtl : String ) {

        if( newmtl == null ) {
            // final round
            materials[ currentMaterialName ] = buildMaterial();
            return;
        }

        if( currentMaterialName != null ) {

            materials[ currentMaterialName ] = buildMaterial();
            reset();
        }

        currentMaterialName = newmtl;
    }

    public function processKa( ka : Vec3 ) : Void {
        this.ka = ka;
    }

    public function processKd( kd : Vec3 ) : Void {
        this.kd = kd;
    }

    public function processKs( ks : Vec3 ) : Void {
        this.ks = ks;
    }

    public function processKe( ke : Vec3 ) : Void {
        this.ke = ke;
    }

    public function processShininess( shininess : Float ) : Void {
        this.shininess = shininess;
    }

    public function processOpacity( opacity : Float ) : Void {
        this.opacity = opacity;
    }
}


class WaveMtlLoader {

    public function new() {
    }

    public function load( content : Bytes ) :  Map< String, Material > {

        var i = new haxe.io.BytesInput( content );
        var mtl = new mme.format.mtl.Reader( i ).read();

        var processor = new MtlProcessorImpl();

        for( m in mtl ) {
            switch( m ) {

                case CMaterial( newmtl ):
                    processor.processNewMtl( newmtl );

                case CKa( ka ):
                    processor.processKa([ ka.r, ka.g, ka.b ]);

                case CKd( kd ):
                    processor.processKd([ kd.r, kd.g, kd.b ]);

                case CKs( ks ):
                    processor.processKs([ ks.r, ks.g, ks.b ]);

                case CKe( ke ):
                    processor.processKe([ ke.r, ke.g, ke.b ]);

                case CNs( ns ):
                    processor.processShininess( ns );

                case Cd( d ):
                    processor.processOpacity( d );

                // case CUnknown( line ):

                default:
            }
        }

        processor.processNewMtl( null );

        return processor.materials;
    }
}
