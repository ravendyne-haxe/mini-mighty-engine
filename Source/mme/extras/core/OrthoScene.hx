package mme.extras.core;

import lime.ui.Window;

import mme.core.Scene;
import mme.camera.OrthoCamera;
import mme.extras.camera.OrthoCameraController;


class OrthoScene extends Scene {

    var controller : OrthoCameraController;
    
    public function new( window : Window, depth : Float = 2000.0, addController : Bool = false ) {
        super();

        var camera = new OrthoCamera( 0, window.width, 0, window.height, depth );
        camera.translateZ( depth / 2 );
        addCamera( camera );

        controller = null;
        if( addController )
            controller = new OrthoCameraController( camera, window );
    }
}
