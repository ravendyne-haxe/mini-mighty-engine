package mme.extras.geometry;

import mme.math.Parametric;
import mme.math.glmatrix.Vec3;

import mme.core.SceneObject;
import mme.core.Material;

class Paraboloid extends SceneObject {

    public var radius : Float;
    public var height : Float;

    public var segmentsX : Int;
    public var segmentsY : Int;

    public function new( radius : Float, height : Float, segmentsW : Int = 10, segmentsH : Int = 5, ?material : Material ) {

        super( material );

        this.radius = radius;
        this.height = height;

        segmentsX = segmentsW < 10 ? 10 : segmentsW;
        segmentsY = segmentsH < 5 ? 5 : segmentsH;

        updateMesh();
    }

    public function updateMesh() : Void {

        // create conus section
        Parametric.csurface( segmentsY, segmentsX, function( theta, zc ) {

            var p = CylindricalGenerators.parabola( theta, zc );
            return [ radius * p.x, radius * p.y, height * ( p.z - 1.0 ) ];
        }, mesh );

        // create circular bottom
        var bmesh = Parametric.csurface( 1, segmentsX, function( theta, zc ) {

            var p = CylindricalGenerators.circle( theta, zc );
            return [ radius * p.x, radius * p.y, p.z ];
        } );
        // flip it around so that normals are pointing in Z+ direction
        // rotate about X-axis since there has to be a vertex that coincides with X-axis
        bmesh.rotateTo( 180, Vec3.X_AXIS );

        // merge the two
        mesh.merge( bmesh );
    }
}
