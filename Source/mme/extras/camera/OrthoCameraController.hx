package mme.extras.camera;

import lime.ui.MouseWheelMode;
import lime.ui.Window;

import mme.camera.OrthoCamera;
using mme.extras.camera.CameraTools;

enum OrhtoCameraFit {
    FillHeight;
    FillWidth;
    KeepRatios;
    FitToWindow;
}

class OrthoCameraController extends CameraController {

    // keep reference so we don't have to cast every time
    private var orthoCamera : OrthoCamera;

    private var billboardSizeHeight : Float;
    private var billboardAspect : Float; // width / height
    private var billboardUnitRatio : Float;

    private var zoomLevel : Float;
    private var initialX : Float;
    private var initialY : Float;

    public var stageWidth ( get, null ) : Float;
    public var stageHeight ( get, null ) : Float;
    private function get_stageWidth() : Float { return billboardSizeHeight * billboardAspect; }
    private function get_stageHeight() : Float { return billboardSizeHeight; }

    public var cameraFit : OrhtoCameraFit;

    public function new( camera : OrthoCamera, window : Window ) {

        super( camera, window );

        orthoCamera = camera;

        cameraFit = FitToWindow;

        setCameraTo( window );

        window.onMouseMoveRelative.add( onMouseMoveRelative );
        window.onMouseWheel.add( onMouseWheel );
        window.onResize.add( onWindowResize );
    }

    public function setCameraTo( window : Window ) {

        billboardSizeHeight = window.height;
        // assumes square pixels, so we have only one ratio
        billboardUnitRatio = window.height / billboardSizeHeight;
        billboardAspect = window.width / window.height;

        zoomLevel = 1.0;
        initialX = camera.positionX;
        initialY = camera.positionY;

        onWindowResize( window.width, window.height );
    }

    public function updateCamera( width : Float, height : Float ) {

        switch( cameraFit ) {
            case FillHeight:
                orthoCamera.updateCameraFillHeight( billboardSizeHeight, zoomLevel * width, zoomLevel * height );
            case FillWidth:
                orthoCamera.updateCameraFillWidth( billboardSizeHeight, zoomLevel * width, zoomLevel * height );
            case KeepRatios:
                orthoCamera.updateCameraKeepRatios( billboardUnitRatio, zoomLevel * width, zoomLevel * height );
            case FitToWindow:
                orthoCamera.updateCameraToWindowSize( zoomLevel * width, zoomLevel * height );
            default:
        }
    }

    public function updateCameraZoom( delta : Float ) {
        if( delta > 0.0 ) {
            zoomLevel *= 1.1;
            updateCamera( window.width, window.height );
        }
        if( delta < 0.0 ) {
            zoomLevel *= 0.9;
            updateCamera( window.width, window.height );
        }
    }

    public function updateCameraPan( x : Float, y : Float ) {
        camera.translateX( -x * zoomLevel );
        camera.translateY( y * zoomLevel );
    }

    public function updateCameraReset() {
        camera.position = [ initialX, initialY, 0.0 ];
        zoomLevel = 1.0;
        updateCamera( window.width, window.height );
    }



	private function onMouseMoveRelative( x : Float, y : Float ) : Void {
        if( primaryButtonDown ) {
            updateCameraPan( x, y );
        }
    }

	private function onMouseWheel( deltaX : Float, deltaY : Float, deltaMode : MouseWheelMode ) : Void {
        updateCameraZoom( deltaY );
    }

	private function onWindowResize( width : Int, height : Int ) : Void {

        var gl = window.context.webgl;
        // FIXME should we keep a reference of renderer?
        // renderer.viewport( gl, width, height );
        gl.viewport( 0, 0, width, height );

        updateCamera( width, height );
    }
}
