package mme.extras;

import mme.math.glmatrix.Vec3;

class ExtrasParametricGenerators {

    /**
        Klein bottle generator.

        This is *planar* generator becuase it [tarnsforms planar surface](https://www.ams.org/journals/notices/201208/rtx120801076p.pdf)
        into Kline bottle surface.
    **/
    public static function klein( u : Float, v : Float ) : Vec3 {

        u = u * 2.0 * Math.PI;
        v = v * 2.0 * Math.PI;

        var x, z;
        if( u < Math.PI ) {
            x = 3 * Math.cos(u) * (1 + Math.sin(u)) + (2 * (1 - Math.cos(u) / 2)) * Math.cos(u) * Math.cos(v);
            z = -8 * Math.sin(u) - 2 * (1 - Math.cos(u) / 2) * Math.sin(u) * Math.cos(v);
        } else {
            x = 3 * Math.cos(u) * (1 + Math.sin(u)) + (2 * (1 - Math.cos(u) / 2)) * Math.cos(v + Math.PI);
            z = -8 * Math.sin(u);
        }
        var y = -2 * (1 - Math.cos(u) / 2) * Math.sin(v);
        return [x, y, z];
    }
}
