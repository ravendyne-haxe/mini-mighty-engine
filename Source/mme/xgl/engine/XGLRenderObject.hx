package mme.xgl.engine;

import lime.graphics.opengl.GL;
import lime.graphics.WebGLRenderContext;

import mme.core.Scene;
import mme.core.SceneObject;


class XGLRenderObject {

	private var sceneObject : SceneObject;

    public var material : XGLMaterial;
    public var geometryBuffer : XGLGeometryBuffer;
    public var shader : XGLShader;

	public function new( gl : WebGLRenderContext, sceneObject : SceneObject ) {

		this.sceneObject = sceneObject;

		create( gl );
	}

    public function dispose( gl : WebGLRenderContext ) {

        sceneObject = null;

        material.dispose( gl );
        material = null;
        geometryBuffer.dispose( gl );
        geometryBuffer = null;
        shader.dispose( gl );
        shader = null;
    }

	public inline function isVisible() { return sceneObject.visible; }

	private function create( gl : WebGLRenderContext ) : Void { }

    public function update( gl : WebGLRenderContext ) : Void {

        geometryBuffer.update( gl );
    }

	public function render( gl : WebGLRenderContext, scene : Scene ) : Void {

		//
		// --- SETUP
		//


		//
		// --- RENDER
		//
		renderSetupGeometry( gl, scene );
        renderSetupAttributes( gl );
		renderSetupMaterial( gl, scene );
		renderDraw( gl );


		//
		// --- CLEANUP
		//

	}

	private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void { }

	private function renderSetupAttributes( gl : WebGLRenderContext ) : Void {

        //
        // Setup ATTRIBUTE values
        //
        gl.bindBuffer( GL.ARRAY_BUFFER, geometryBuffer.buffer.vertexBufferHandle );
        geometryBuffer.describeProgramAttributes( gl );

        if( geometryBuffer.elementBuffer != null )
            gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, geometryBuffer.elementBuffer.elementBufferHandle );

    }

	private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void { }
	
	private function renderDraw( gl : WebGLRenderContext ) : Void {

        //
        // Do DRAWING
        //
        if( geometryBuffer.elementBuffer != null )
            gl.drawElements( GL.TRIANGLES, geometryBuffer.elementBuffer.length, GL.UNSIGNED_SHORT, 0 );
        else
            gl.drawArrays( GL.TRIANGLES, 0, geometryBuffer.buffer.length );
    }
}
