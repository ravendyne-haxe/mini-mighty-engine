package mme.xgl.engine;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;
import lime.graphics.opengl.GLBuffer;

import lime.utils.Float32Array;
import lime.utils.ArrayBufferView;


class XGLVertexAttributeParams {
	// attribute buffer is an array of
	// attribute groups, each having same structure.
	// An attribute group contains one or more attribute values,
	// i.e. a group can contain one (X,Y) vertex position value and
	// one (U,V) texture coordinate value.
	// Each attribute can have 1, 2, 3 or 4 components.
	// Each attribute is defined within the group by:
	//		- number of components
	//		- attribute offset:
	// 		    where is the first value of attribute,
	//		    counting from where this group starts in the buffer


	// how many components there are in this attribute
	// i.e. for 3-component float vector, componentCount == 3
    public var componentCount : Int;
	// where does this attribute start, in bytes,
	// counting from the beginning of this attribute's group
    public var attributeOffset : Int;

    public function new( count : Int, offset : Int ) {
	    componentCount = count;
	    attributeOffset = offset;
	}
}


class XGLArrayBuffer {

	public var vertexBufferHandle : GLBuffer;
	public var geometryBufferData : Float32Array;

    public var length : Int;

	// used for vertexAttribPointer
	public var vertexAttributesSize : Int;
	public var attributeParams : Map< String, XGLVertexAttributeParams >;

    private var usage : Int;


	public function new( gl : WebGLRenderContext, attributeSize : Int, usage: Int ) {

		attributeParams = [];
		geometryBufferData = null;
        length = 0;
		vertexAttributesSize = attributeSize * Float32Array.BYTES_PER_ELEMENT;
        this.usage = usage;


		vertexBufferHandle = gl.createBuffer();
	}

    public function dispose( gl : WebGLRenderContext ) {
        
		attributeParams = [];
		geometryBufferData = null;
		vertexAttributesSize = 0;
        usage = 0;

        gl.deleteBuffer( vertexBufferHandle );
        vertexBufferHandle = null;
    }

	public function uploadVertexBuffer( gl : WebGLRenderContext, bufferData : Float32Array ) : Void {

		geometryBufferData = bufferData;
        length = Std.int( geometryBufferData.byteLength / vertexAttributesSize );

        // GL_ARRAY_BUFFER is for vertices
        // GL_ELEMENT_ARRAY_BUFFER is for geometry elements, etc.
        // gl.glBindBuffer( GL2ES2.GL_ARRAY_BUFFER, vertexBufferHandle );
		gl.bindBuffer( GL.ARRAY_BUFFER, vertexBufferHandle );


        //
        // this will de-allocate previous GPU memory held by this buffer and allocate new one for this data
        //
        // Use GL_STATIC_DRAW if the vertex data will be uploaded once and rendered many times
        // Use GL_DYNAMIC_DRAW if the vertex data will be created once, changed from time to time, but drawn many times more than that
        // Use GL_STREAM_DRAW if the vertex data will be uploaded once and drawn once, it will change with every frame
		gl.bufferData( GL.ARRAY_BUFFER, bufferData, usage );

        // when done, unbind
		gl.bindBuffer( GL.ARRAY_BUFFER, null );
	}

	// public function updateBuffer( gl : WebGLRenderContext, offset : Int = 0, bufferSubData : ArrayBufferView ) : Void {

    //     if( offset + bufferSubData.byteLength > geometryBufferData.byteLength )
    //         throw "MME: ------------> update goes beyond allocated buffer size";

    //     // http://docs.gl/es2/glBufferData
    //     // http://docs.gl/es2/glBufferSubData
	// 	gl.bindBuffer( GL.ARRAY_BUFFER, vertexBufferHandle );

    //     gl.bufferSubData( GL.ARRAY_BUFFER, offset, bufferSubData );

    //     // when done, unbind
	// 	gl.bindBuffer( GL.ARRAY_BUFFER, null );
	// }

	public function addAttribute( name : String, componentCount : Int, attributeOffset : Int ) : Void {

		attributeParams[ name ] = new XGLVertexAttributeParams( componentCount, attributeOffset * Float32Array.BYTES_PER_ELEMENT );
	}
}
