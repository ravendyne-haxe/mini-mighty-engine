package mme.xgl.engine;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;
import lime.graphics.opengl.GLBuffer;

import lime.utils.UInt16Array;
import lime.utils.ArrayBufferView;


class XGLElementArrayBuffer {

	public var elementBufferHandle : GLBuffer;
	public var elementBufferData : UInt16Array;
    public var length : Int;

    private var usage : Int;


	public function new( gl : WebGLRenderContext, usage: Int ) {

		elementBufferData = null;
        length = 0;
        this.usage = usage;

		elementBufferHandle = gl.createBuffer();
	}

    public function dispose( gl : WebGLRenderContext ) {
        
		elementBufferData = null;
        usage = 0;

        gl.deleteBuffer( elementBufferHandle );
        elementBufferHandle = null;
    }

	public function uploadElementBuffer( gl : WebGLRenderContext, bufferData : UInt16Array ) : Void {

		elementBufferData = bufferData;
        // number of elements = nubmer of bytes / sizeof(uin16) = # of bytes / 2
        // val >> 1: divide val by 2
        length = elementBufferData.byteLength >> 1;

        // GL_ARRAY_BUFFER is for vertices
        // GL_ELEMENT_ARRAY_BUFFER is for geometry elements, etc.
        // gl.glBindBuffer( GL2ES2.GL_ELEMENT_ARRAY_BUFFER, elementBufferHandle );
		gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, elementBufferHandle );


        //
        // this will de-allocate previous GPU memory held by this buffer and allocate new one for this data
        //
        // Use GL_STATIC_DRAW if the vertex data will be uploaded once and rendered many times
        // Use GL_DYNAMIC_DRAW if the vertex data will be created once, changed from time to time, but drawn many times more than that
        // Use GL_STREAM_DRAW if the vertex data will be uploaded once and drawn once, it will change with every frame
		gl.bufferData( GL.ELEMENT_ARRAY_BUFFER, bufferData, usage );

        // when done, unbind
		gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, null );
	}

	// public function updateBuffer( gl : WebGLRenderContext, offset : Int = 0, bufferSubData : ArrayBufferView ) : Void {

    //     if( offset + bufferSubData.byteLength > elementBufferData.byteLength )
    //         throw "MME: ------------> update goes beyond allocated buffer size";

    //     // http://docs.gl/es2/glBufferData
    //     // http://docs.gl/es2/glBufferSubData
	// 	gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, elementBufferHandle );

	// 	gl.bufferSubData( GL.ELEMENT_ARRAY_BUFFER, offset, bufferSubData );

    //     // when done, unbind
	// 	gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, null );
	// }
}
