package mme.xgl.engine;

import lime.graphics.WebGLRenderContext;
import lime.graphics.opengl.GL;

import mme.core.SceneObject;
import mme.core.Scene;

import mme.xgl.XGLRegistry;

import mme.core.RGBAColor;

import mme.xgl.engine.XGLRenderObject;

class SceneRendererData {

    /** Maps SceneObject -> XGLRenderObject so we can easily find and update
        XGLRenderObject when SceneObject changes in a way that affects
        its corresponding XGLRenderObject instance **/
    public var renderObjectList : Map< SceneObject, XGLRenderObject >;
    /** XGLRenderObject instances grouped by shader they use so renderer can
        optimize its work a bit. Kept here because this is the place that
        scene objects, and their render objects, are added/removed **/
    public var renderObjectGroups : Map< XGLShader, Array< XGLRenderObject > >;

	public function new() {

        renderObjectList = new Map();
        renderObjectGroups = new Map();
	}
}

class XGLRenderer {

	public var background : RGBAColor;

    private var renderObjectList : Map< SceneObject, XGLRenderObject >;
    private var renderObjectGroups : Map< XGLShader, Array< XGLRenderObject > >;

    public function new( gl : WebGLRenderContext, ?background : RGBAColor ) {

		this.background = background != null ? background : 0x3f7fc0ff; /* bluish */

        XGLRegistry.initialize( gl );

	    initGLState( gl );
	}

    public function dispose( gl : WebGLRenderContext ) {

        renderObjectGroups = [];

        for( ro in renderObjectList.keys() ) {
            renderObjectList.get( ro ).dispose( gl );
            renderObjectList.remove( ro );
        }

        renderObjectList = [];
    }

    private function initGLState( gl : WebGLRenderContext ) : Void {

		//
		// global GL context setup
		//
        // gl.viewport( 0, 0, width, height );

	    gl.clearColor( background.r, background.g, background.b, 1.0);
	    gl.enable( GL.DEPTH_TEST );
	    gl.clearDepth( 1.0 ); // set clear depth value to farthest

		#if desktop
	    gl.enable( GL.TEXTURE_2D );
		#end

        // gl.enable( GL.CULL_FACE );
        // gl.cullFace( GL.BACK );
        // gl.frontFace( GL.CCW );
	}

    public function render( gl : WebGLRenderContext, scene : Scene ) {

		renderSetup( gl );
		renderScene( gl, scene );
	}

    public function renderSetup( gl : WebGLRenderContext ) {

		//
		// SETUP
		//
	    gl.clear( GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT );
	}

    public function renderScene( gl : WebGLRenderContext, scene : Scene ) {

		if( scene.rendererTag == null ) {
			scene.rendererTag = new SceneRendererData();
		}

		var sceneTag : SceneRendererData = cast scene.rendererTag;
		renderObjectList = sceneTag.renderObjectList;
		renderObjectGroups = sceneTag.renderObjectGroups;

		updateRenderObjectListFromScene( gl, scene );
		renderGroups( gl, scene );

		renderObjectList = null;
		renderObjectGroups = null;
	}

    public function renderGroups( gl : WebGLRenderContext, scene : Scene ) {

		//
		// START
		//

		//
		// GROUPS
		//
	    for( shader in renderObjectGroups.keys() ) {

			//
			// ACTIVATE PROGRAM
			//
			gl.useProgram( shader.program.programHandle );

			//
			// Enable PROGRAM attributes
			//
			for( attr in shader.program.attributeHandles ) {
				gl.enableVertexAttribArray( attr );
			}

			//
			// Do setup tasks that are common for all objects
            // renderd using this PROGRAM, i.e set uniforms that have the
            // same value for all render objects in this group or
            // execute calls to glEnable() with to activate specific features etc.
			//
			shader.renderCommonProgramSetup( gl, scene );

			//
			// RENDER
			//
			for( renderObject in renderObjectGroups[ shader ] ){
				if( renderObject.isVisible() ) renderObject.render( gl, scene );
			}

			//
			// Do cleanup tasks that are common for all objects
            // rendered using this PROGRAM, i.e execute calls to
            // glDisable() to deactivate specific features etc.
            //
			shader.renderCommonProgramCleanup( gl, scene );

			//
			// Disable PROGRAM attributes
			//
			for( attr in shader.program.attributeHandles ) {
				gl.disableVertexAttribArray( attr );
			}
		}


		//
		// END
		//
	    gl.bindBuffer( GL.ARRAY_BUFFER, null );
	    gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, null );
	    gl.bindTexture( GL.TEXTURE_2D, null );
		// Commented out: DON'T disable programmable processors, keep 'em running
		// gl.useProgram( 0 );
	}

    //==============================================================================
    //
    // Render object list related stuff
    //
    //==============================================================================

    private function updateRenderObjectListFromScene( gl : WebGLRenderContext, scene : Scene  ) : Void {

        if( scene.objectScheduledForUpdate.length == 0 ) return;

        var registry = XGLRegistry.get( gl );

        for( sceneObject in scene.objectScheduledForUpdate ) {

			if( ! renderObjectList.exists( sceneObject ) ) {

				// create new
				var renderObject = registry.createRenderObjectFor( gl, sceneObject );

				// add it to the list of all render objects
				renderObjectList[ sceneObject ] = renderObject;

				// create shader group if it doesn't exist already
				if( ! renderObjectGroups.exists( renderObject.shader ) ) {
					renderObjectGroups[ renderObject.shader ] = [];
				}
				// add object to its render group
				renderObjectGroups[ renderObject.shader ].push( renderObject );

			} else {

                var renderObject = renderObjectList[ sceneObject ];
                renderObject.update( gl );
			}
        }

		scene.objectScheduledForUpdate = [];
    }
}
