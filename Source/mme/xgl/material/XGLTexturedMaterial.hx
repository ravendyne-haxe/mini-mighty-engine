package mme.xgl.material;

import lime.graphics.WebGLRenderContext;
import lime.graphics.Image;

import lime.graphics.opengl.GLTexture;

import mme.material.TextureMaterial;

import mme.xgl.engine.XGLMaterial;
import mme.xgl.engine.XGLTexture;


class XGLTexturedMaterial extends XGLMaterial {

    private var objectMaterial : TextureMaterial;

	public var textureImageLocation : String;

    /** Texture object containing uploaded texture handle and other data **/
	public var texture : XGLTexture;

	public function new( gl : WebGLRenderContext, material : TextureMaterial ) {

		super( gl, material );

        objectMaterial = material;

        #if (js && html5)
        // if this is not called for JS target, Image.buffer will not contain valid data
        objectMaterial.textureImage.data;
        #end
		texture = new XGLTexture( gl, objectMaterial.textureImage.buffer );
	}

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        texture.dispose( gl );
        texture = null;

        textureImageLocation = null;
        objectMaterial = null;
    }
}
