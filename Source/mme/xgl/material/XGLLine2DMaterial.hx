package mme.xgl.material;

import lime.graphics.WebGLRenderContext;

import mme.g2d.material.Line2DMaterial;

import mme.core.RGBAColor;

import mme.xgl.engine.XGLMaterial;


class XGLLine2DMaterial extends XGLMaterial {

    private var objectMaterial : Line2DMaterial;

    public var edgeColor ( get, null ) : RGBAColor;
    public var inner ( get, null ) : Float;
    public var blendStart ( get, null ) : Float;

    private var blendStart_value : Float;

	public function new( gl : WebGLRenderContext, material : Line2DMaterial ) {

		super( gl, material );

        objectMaterial = material;

        switch( material.lineBlending ) {
            case Sharp:
                blendStart_value = 1.0;
            case Smooth:
                blendStart_value = 0.7;
            case ThinLines:
                blendStart_value = 0.0;
        }
	}

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        objectMaterial = null;
    }

    private function get_edgeColor() : RGBAColor {

	    return objectMaterial.edgeColor;
    }

    private function get_inner() : Float {

        return objectMaterial.inner;
    }

    private function get_blendStart() : Float {

        return blendStart_value;
    }
}
