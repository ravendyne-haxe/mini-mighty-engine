package mme.xgl.material;

import mme.math.glmatrix.Vec3;
import lime.graphics.WebGLRenderContext;

import mme.material.PhongMaterial;

import mme.xgl.engine.XGLMaterial;


class XGLPhongColorMaterial extends XGLMaterial {

    private var objectMaterial : PhongMaterial;

    /** Material ambient light reflectance **/
    public var ka ( get, null ) : Vec3;
    /** Material diffuse light reflectance **/
    public var kd ( get, null ) : Vec3;
    /** Material specular light reflectance **/
    public var ks ( get, null ) : Vec3;

    public var shininess ( get, null ) : Float;

	public function new( gl : WebGLRenderContext, material : PhongMaterial ) {

		super( gl, material );

        objectMaterial = material;
	}

    override public function dispose( gl : WebGLRenderContext ) {

        super.dispose( gl );

        objectMaterial = null;
    }

    private function get_ka() : Vec3 {

        return objectMaterial.ka;
    }

    private function get_kd() : Vec3 {

        return objectMaterial.kd;
    }

    private function get_ks() : Vec3 {

        return objectMaterial.ks;
    }

    private function get_shininess() : Float {

        return objectMaterial.shininess;
    }
}
