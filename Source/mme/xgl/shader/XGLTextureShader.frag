// #if (!desktop || rpi)
#ifdef GL_ES
precision mediump float;
#endif
// #end

uniform sampler2D u_Texture;

varying vec2 v_TextureUV;

void main( void ) {
    gl_FragColor = texture2D( u_Texture, v_TextureUV );
}
