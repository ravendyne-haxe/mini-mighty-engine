// #if (!desktop || rpi)
#ifdef GL_ES
precision mediump float;
#endif
// #end

uniform vec3 u_PointColor;
uniform vec3 u_EdgeColor;
uniform int u_Type;

varying vec2 v_TextureUV;

vec4 circle() {

    // remap UV space to -1.0 to 1.0
    vec2 st = v_TextureUV.xy * 2.0 - 1.0;

    // circular distance field with center at (0.0, 0.0)
    float pct = length( st );
    
    // inner edge
    float stp1 = step( 0.8, pct );
    // outer edge, inverted
    float stp2 = step( pct, 1.0 );
    // with step(), this is same as logical AND
    pct = stp1 * stp2;

    // pct is 0 or 1 so mix will show
    // u_PointColor for 0 and u_EdgeColor for 1
    vec3 vmix = mix( u_PointColor, u_EdgeColor, pct );

    // use stp2 as alpha to mask out corners
    return vec4( vmix, stp2 );
}
vec4 square() {

    // remap UV space to -1.0 to 1.0
    vec2 st = v_TextureUV.xy * 2.0 - 1.0;

    // square distance field with center at (0.0, 0.0)
    st = abs( st );
    float pct = max( st.x, st.y );

    // inner edge
    float stp1 = step( 0.8, pct );
    // outer edge, inverted
    float stp2 = step( pct, 1.0 );
    // with step(), this is same as logical AND
    pct = stp1 * stp2;

    // pct is 0 or 1 so mix will show
    // u_PointColor for 0 and u_EdgeColor for 1
    vec3 vmix = mix( u_PointColor, u_EdgeColor, pct );

    return vec4( vmix, 1.0 );
}
vec4 diamond() {

    // remap UV space to -1.0 to 1.0
    vec2 st = v_TextureUV.xy * 2.0 - 1.0;


    // diamond distance field with center at (0.0, 0.0)
    st = abs(st);
    float p = 0.5; // diamond
    // float p = 1.0; // circle
    float pct = length( pow( st, vec2( p ) ) );

    
    // inner edge
    float stp1 = step( 0.8, pct );
    // outer edge, inverted
    float stp2 = step( pct, 1.0 );
    // with step(), this is same as logical AND
    pct = stp1 * stp2;

    // pct is 0 or 1 so mix will show
    // u_PointColor for 0 and u_EdgeColor for 1
    vec3 vmix = mix( u_PointColor, u_EdgeColor, pct );

    // use stp2 as alpha to mask out corners
    return vec4( vmix, stp2 );
}

void main() {
    vec4 color = vec4( 1.0 );

    if( u_Type == 1 )
        color = circle();
    if( u_Type == 2 )
        color = square();
    if( u_Type == 3 )
        color = diamond();

    gl_FragColor = color;
    if( color.a < 0.0005 ) discard;
}
