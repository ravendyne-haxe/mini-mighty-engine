package mme.xgl.shader;

import lime.graphics.WebGLRenderContext;

import mme.core.Scene;

import mme.xgl.engine.XGLShaderProgram;
import mme.xgl.engine.XGLShader;

import mme.util.Macros;


class XGLLineShader extends XGLShader {

    public static var ID ( default, never ) : Int = MMEngine.nextId();

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSourceA = Macros.getVertexShaderSource();

        var fragmentShaderSourceA = Macros.getFragmentShaderSource();

        program = new XGLShaderProgram( gl, vertexShaderSourceA, fragmentShaderSourceA );
    }

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        gl.enable(gl.BLEND);
        gl.blendEquation(gl.FUNC_ADD);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    }

}
