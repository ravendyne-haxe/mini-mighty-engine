package mme.xgl.shader;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLShaderProgram;

import mme.xgl.engine.XGLShader;

import mme.util.Macros;


class XGLTextureShader extends XGLShader {

    public static var ID ( default, never ) : Int = MMEngine.nextId();

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSource = Macros.getVertexShaderSource();

        var fragmentShaderSource = Macros.getFragmentShaderSource();

        program = new XGLShaderProgram( gl, vertexShaderSource, fragmentShaderSource );
    }
}