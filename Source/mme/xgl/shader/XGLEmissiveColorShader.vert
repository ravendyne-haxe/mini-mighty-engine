attribute vec3 a_Position;

uniform mat4 u_TransformMatrix;

void main (void) {
    gl_Position = u_TransformMatrix * vec4(a_Position, 1.0);
}
