package mme.xgl.shader;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLShaderProgram;

import mme.xgl.engine.XGLShader;

import mme.util.Macros;


class XGLEmissiveColorShader extends XGLShader {

    public static var ID ( default, never ) : Int = MMEngine.nextId();

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSourceA = Macros.getVertexShaderSource();

        var fragmentShaderSourceA =  Macros.getFragmentShaderSource();

        program = new XGLShaderProgram( gl, vertexShaderSourceA, fragmentShaderSourceA );
    }
}
