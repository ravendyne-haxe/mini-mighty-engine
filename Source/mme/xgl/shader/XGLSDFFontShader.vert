attribute vec3 a_Position;
attribute vec2 a_TextureUV;

uniform mat4 u_TransformMatrix;

varying vec2 v_TextureUV;

void main( void ) {
    v_TextureUV = a_TextureUV;
    gl_Position = u_TransformMatrix * vec4(a_Position, 1.0);
}
