attribute vec3 a_Position;
attribute float a_Side;
attribute float a_Distance;

uniform mat4 u_TransformMatrix;

varying float v_Edge;
varying float v_Distance;

void main() {

    v_Edge = a_Side;
    v_Distance = a_Distance;

    gl_Position = u_TransformMatrix * vec4( a_Position, 1.0 );
}
