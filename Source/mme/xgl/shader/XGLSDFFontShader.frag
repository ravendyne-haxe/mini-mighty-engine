// #if (!desktop || rpi)
#ifdef GL_ES
precision mediump float;
#endif
// #end
const float threshold = 0.5;

// use 'offset' to vary how 'thick' rendering is
const float offset = 0.1;
// blend this area or use different colot for it
const float edgeWidth = 0.4;


uniform vec3 u_TextColor;
uniform sampler2D u_Texture;

varying vec2 v_TextureUV;

void main( void ) {

    vec4 fieldSample = texture2D( u_Texture, v_TextureUV );

    float d = fieldSample.a;
    // float d = fieldSample.a + offset;

    // float e0 = threshold - edgeWidth / 2.0;
    // float e1 = threshold + edgeWidth / 2.0;

    //
    // OPTION 1
    // do smooth transition around threshold using linear interpolation
    // NOTE: this one is not going to work when edgeWidth == 0.0, that is the case for step()
    //
    // float dd = ( d - e0 ) / ( e1 - e0 );
    // float alpha = clamp( dd, 0.0, 1.0 );

    //
    // OPTION 2
    // do smooth transition around threshold using hermite interpolation
    //
    // float alpha = smoothstep( e0, e1, d );

    //
    // OPTION 3
    // basic stuff: just step at the treshold
    //
    float alpha = step( threshold, d );

    gl_FragColor = vec4( u_TextColor, alpha );

    // 'discard' effectively keeps whatever letters are rendered over and
    // also helps when rendering overlapping letters
    if( alpha < 0.0005 ) discard;
}
