// #version 120
// https://github.com/pyalot/webgl-easy-wireframe/blob/master/src/display.shader
/*
*/
// need 'derivatives' extension for fwidth(), when WebGL 1 or GLSL ES (a.k.a. version 100)
#ifdef GL_OES_standard_derivatives
#extension GL_OES_standard_derivatives : enable
#endif

// #if (!desktop || rpi)
#ifdef GL_ES
precision mediump float;
#endif
// #end

uniform vec3 u_EmissiveColor;
vec3 edgeColor = vec3(0.0);

varying vec3 v_VertexEdgeParameter;

float edgeCoefficient()
{
#if defined GL_OES_standard_derivatives || __VERSION__ != 100

    // abs(dFdx) + abs(dFdy)
    // parameter component values are in [0,1] range
    vec3 delta = fwidth( v_VertexEdgeParameter );

    // https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/smoothstep.xhtml
    // clamp at 0.0 - delta * 1.5, and smoothstep inbetween, result is in [0,1] range
    vec3 processedParameter = smoothstep( vec3( 0.0 ), delta * 1.5, v_VertexEdgeParameter );

    return min( min( processedParameter.x, processedParameter.y ), processedParameter.z );

#else

    // (ugly) fallback if GLSL == 1.00 (WebGL 1) and there's no OES_standard_derivatives extension
    float minEP =  min( min( v_VertexEdgeParameter.x, v_VertexEdgeParameter.y ), v_VertexEdgeParameter.z );
    minEP = smoothstep( 0.0, 0.1, minEP );

    return minEP;
#endif
}

void main()
{
    // aliased boolean decision
    /*
    // same as:
    // float minVal = min( min( v_VertexEdgeParameter.x, v_VertexEdgeParameter.y ), v_VertexEdgeParameter.z )
    // if( minVal < 0.02 ) {...
    if( any( lessThan( v_VertexEdgeParameter, vec3( 0.02 ) ) ) )
    {
        gl_FragColor = vec4( edgeColor, 1.0 );
    }
    else
    {
        gl_FragColor = vec4( u_EmissiveColor, 1.0 );
    }
    */

    // coloring by edge
    gl_FragColor.rgb = mix( edgeColor, u_EmissiveColor, edgeCoefficient() );
    gl_FragColor.a = 1.0;

    // alpha by edge
    // for this one to work, disable GL_DEPTH_TEST and GL_CULL_FACE
    /*
    if( gl_FrontFacing )
    {
        gl_FragColor = vec4( edgeColor, ( 1.0 - edgeCoefficient() ) * 0.95 );
    }
    else
    {
        gl_FragColor = vec4( edgeColor, ( 1.0 - edgeCoefficient() ) * 0.7 );
    }
    */

    // gl_FragColor = vec4( v_VertexEdgeParameter, 1.0 );

}
