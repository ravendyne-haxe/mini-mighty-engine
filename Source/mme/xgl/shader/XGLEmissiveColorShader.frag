// #if (!desktop || rpi)
#ifdef GL_ES
precision mediump float;
#endif
// #end

uniform vec3 u_EmissiveColor;

void main (void) {
    gl_FragColor = vec4(u_EmissiveColor, 1.0);
}
