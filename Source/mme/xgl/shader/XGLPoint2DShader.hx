package mme.xgl.shader;

import lime.graphics.WebGLRenderContext;

import mme.core.Scene;

import mme.xgl.engine.XGLShaderProgram;
import mme.xgl.engine.XGLShader;

import mme.util.Macros;


private class XGLPoint2DShader extends XGLShader {

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSource = Macros.getVertexShaderSource();

        var fragmentShaderSourceCircle = Macros.getFragmentShaderSource();

        program = new XGLShaderProgram( gl, vertexShaderSource, fragmentShaderSourceCircle );
    }

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        gl.enable(gl.BLEND);
        gl.blendEquation(gl.FUNC_ADD);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    }

}

class XGLPoint2DCircleShader extends XGLPoint2DShader {

    public static var ID ( default, never ) : Int = MMEngine.nextId();

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        super.renderCommonProgramSetup( gl, scene );

        gl.uniform1i( program.uniformHandles['u_Type'], 1 );
    }
}

class XGLPoint2DSquareShader extends XGLPoint2DShader {

    public static var ID ( default, never ) : Int = MMEngine.nextId();

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        super.renderCommonProgramSetup( gl, scene );

        gl.uniform1i( program.uniformHandles['u_Type'], 2 );
    }
}

class XGLPoint2DDiamondShader extends XGLPoint2DShader {

    public static var ID ( default, never ) : Int = MMEngine.nextId();

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        super.renderCommonProgramSetup( gl, scene );

        gl.uniform1i( program.uniformHandles['u_Type'], 3 );
    }
}
