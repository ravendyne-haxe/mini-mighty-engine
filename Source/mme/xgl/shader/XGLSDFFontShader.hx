package mme.xgl.shader;

import mme.material.NamedColor;
import mme.core.RGBAColor;
import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLShaderProgram;

import mme.xgl.engine.XGLShader;

import mme.core.Scene;

import mme.util.Macros;


class XGLSDFFontShader extends XGLShader {

    public static var ID ( default, never ) : Int = MMEngine.nextId();

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSource = Macros.getVertexShaderSource();

        var fragmentShaderSource = Macros.getFragmentShaderSource();

        program = new XGLShaderProgram( gl, vertexShaderSource, fragmentShaderSource );
    }

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        gl.enable(gl.BLEND);
        gl.blendEquation(gl.FUNC_ADD);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    }
}
