// #if (!desktop || rpi)
#ifdef GL_ES
precision mediump float;
#endif
// #end

uniform vec3 u_LineColor;
uniform vec3 u_EdgeColor;
uniform float u_Inner;
uniform float u_BlendStart;
uniform float u_Opacity;

varying float v_Edge;
varying float v_Distance;

float y_core( float x ) {

    // for https://fordhurley.com/glsl-grapher/, uncomment next line
    // float u_Inner = 0.5;

    float ll = u_Inner - 0.1;
    float ul = u_Inner + 0.1;

    float core = smoothstep( ll, ul, x );

    return core;
}

void main() {

    // v_Edge:
    //  -1.0 - at the very v_Edge of outline on one side
    //   0.0 - at the center line
    //  +1.0 - at the very v_Edge of outline on the other side

    // on the centre line: v == 0.0
    // on the outline v_Edge: v == 1.0
    float v = abs( v_Edge );

    // linear interpolation between 'u_LineColor' and 'u_EdgeColor'
    // to accent the v_Edge with 'u_EdgeColor' where point of transition
    // between 'u_LineColor' and 'u_EdgeColor' is determined by 'u_Inner' uniform
    float core_blend = y_core( v );

    // begin blending the outline to background at 90% thickness
    float alpha_blend = 1.0 - smoothstep( u_BlendStart, 1.0, v );

    gl_FragColor = vec4( mix( u_LineColor, u_EdgeColor, core_blend ), u_Opacity * alpha_blend );
}
