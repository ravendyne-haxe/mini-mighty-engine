attribute vec3 a_Position;
attribute vec2 a_TextureUV;

uniform mat4 u_TransformMatrix;

varying vec2 v_TextureUV;

void main() {

    v_TextureUV = a_TextureUV;

    gl_Position = u_TransformMatrix * vec4( a_Position.xy, 0.0, 1.0 );
}
