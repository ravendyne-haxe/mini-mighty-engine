attribute vec3 a_PrevPosition;
attribute vec3 a_CurPosition;
attribute vec3 a_NextPosition;
attribute float a_TopBottom;
attribute float a_StartEnd;

uniform mat4 u_TransformMatrix;
uniform float u_Thickness;
uniform float u_Aspect;


vec4 line_cap_butt( vec2 previousScreen, vec2 nextScreen ) {

    vec2 v0 = previousScreen;
    vec2 v1 = nextScreen;

    float len = length( v1 - v0 );

    // topbottom == +1 --> dx, dy sign unchanged
    // topbottom == -1 --> dx, dy sign flipped
    float dx_ = (v1.y - v0.y) / len * u_Thickness / 2.0 * a_TopBottom;
    float dy_ = (v1.x - v0.x) / len * u_Thickness / 2.0 * a_TopBottom;

    // butt cap
    // float dx = dx_;
    // float dy = dy_;
    // square cap
    float dx = dx_ + dy_ * a_TopBottom * a_StartEnd;
    float dy = dy_ - dx_ * a_TopBottom * a_StartEnd;

    dx /= u_Aspect;

    vec4 offset = vec4( -dx, dy, 0.0, 0.0 );

    return offset;
}

void main() {

    vec2 aspectVec = vec2( u_Aspect, 1.0 );

    vec4 previousProjected = u_TransformMatrix * vec4( a_PrevPosition, 1.0 );
    vec4 currentProjected = u_TransformMatrix * vec4( a_CurPosition, 1.0 );
    vec4 nextProjected = u_TransformMatrix * vec4( a_NextPosition, 1.0 );

    // get 2D screen space with W divide and aspect correction
    // vec2 currentScreen = currentProjected.xy / currentProjected.w * aspectVec;
    vec2 previousScreen = previousProjected.xy / previousProjected.w * aspectVec;
    vec2 nextScreen = nextProjected.xy / nextProjected.w * aspectVec;

    gl_Position = currentProjected + line_cap_butt( previousScreen, nextScreen );
}
