// #if (!desktop || rpi)
#ifdef GL_ES
precision mediump float;
#endif
// #end

#define MAX_NUMBER_OF_LIGHTS 8
struct PointLight {
    vec3 color;
    vec3 position;
    float attenuation;
};

// ambient light
uniform vec3 u_AmbientLightColor;
// point lights
uniform PointLight u_Lights[ MAX_NUMBER_OF_LIGHTS ];
// camera
uniform vec3 u_CameraPosition;
uniform float u_IsPerspectiveProjection;
// material
uniform vec3 u_EmissiveColor;
uniform vec3 u_Ka;
uniform vec3 u_Kd;
uniform vec3 u_Ks;
uniform float u_Shininess;

varying vec3 v_Normal;
varying vec3 v_ModelPosition;

vec3 Ambient_Lighting() {

    return u_AmbientLightColor * u_Ka;
}

vec3 Diffuse_Lighting( vec3 nNormal, vec3 lightDirection, vec3 lightColor ) {

    float dot_product = dot( nNormal, lightDirection );

    // light color x light intensity --> vec3( 1.0, 1.0, 1.0 )
    return max( dot_product, 0.0 ) * lightColor * u_Kd;
}

vec3 Specular_Lighting( vec3 viewDirection, vec3 lightDirection, vec3 nNormal, vec3 lightColor ) {

    float dot_product = dot( nNormal, lightDirection );
    vec3 reflectionDirection = reflect( - lightDirection, nNormal );

    // we do step(0...) because reflect() will give vector that yields positive dot()
    // even when light comes from opposite side of the surface normal direction
    // step( 0.0, dot_product ) == [0,1] ---> for negative dot product (surface's back face), specular == 0
    float specular = step( 0.0, dot_product ) * pow( max( dot( viewDirection, reflectionDirection ), 0.0 ), u_Shininess );

    return specular * lightColor * u_Ks;
}

void main (void) {

    vec3 nNormal = normalize( v_Normal );
    // for ortho projection viewer eye position is in infinity and viewDirection is constant for every fragment
    // for perspective projection viewer eye position is at camera position and viewDirection depends on fragment position
    vec3 viewDirection = normalize( u_CameraPosition - u_IsPerspectiveProjection * v_ModelPosition );

    //
    // ambient illumination component
    //
    vec3 ambientLighting = Ambient_Lighting();


    // we use this to render back face as black for diffuse component when no face culling is enabled
    float front_face_visible = step( 0.0, dot( nNormal, viewDirection ) ); // 0.0 -> back face visible, 1.0 -> front face visible

    vec3 diffuseAndSpecularLightingCumulative = vec3(0);

    for( int idx = 0; idx < MAX_NUMBER_OF_LIGHTS; idx++ ) {

        vec3 lightDirection = normalize( u_Lights[ idx ].position - v_ModelPosition );
        //
        // diffuse illumination component
        //
        vec3 diffuseLighting = Diffuse_Lighting( nNormal, lightDirection, u_Lights[ idx ].color );
        diffuseLighting = front_face_visible * diffuseLighting;

        //
        // specular illumination component
        //
        vec3 specularLighting = Specular_Lighting( viewDirection, lightDirection, nNormal, u_Lights[ idx ].color );

        //
        // attenuation
        //
        float distanceToLight = length( u_Lights[ idx ].position - v_ModelPosition );
        float attenuation = 1.0 / ( 1.0 + u_Lights[ idx ].attenuation * pow( distanceToLight, 2.0 ) );

        diffuseAndSpecularLightingCumulative += attenuation * ( diffuseLighting + specularLighting );
    }

    //
    // final illumination value
    //
    vec3 finalLighting = ambientLighting + diffuseAndSpecularLightingCumulative;
    finalLighting = finalLighting + u_EmissiveColor;

    //
    // gamma correction
    //
    // vec3 gamma = vec3( 1.0 / 2.2 );
    // vec3 finalLighting = pow( finalLighting, gamma );


    gl_FragColor = vec4( finalLighting, 1.0 );
    // gl_FragColor = vec4( u_EmissiveColor, 1.0 );
}
