package mme.xgl.shader;

import lime.graphics.WebGLRenderContext;

import mme.core.Scene;

import mme.core.RGBAColor;

import mme.xgl.engine.XGLShaderProgram;
import mme.xgl.engine.XGLShader;

import mme.util.Macros;

class XGLPhongColorShader extends XGLShader {

    public static var ID ( default, never ) : Int = MMEngine.nextId();

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSourceA = Macros.getVertexShaderSource();

        var fragmentShaderSourceA = Macros.getFragmentShaderSource();

        program = new XGLShaderProgram( gl, vertexShaderSourceA, fragmentShaderSourceA );
    }

    override public function renderCommonProgramSetup( gl : WebGLRenderContext, scene : Scene ) {

        var ambientLightColor = RGBAColor.getLightColor( scene.ambientLight );
        gl.uniform3f( program.uniformHandles['u_AmbientLightColor'],
            ambientLightColor.r,
            ambientLightColor.g,
            ambientLightColor.b
        );

        var isPerspective = scene.activeCamera.isPerspective ? 1.0 : 0.0;
        gl.uniform1f( program.uniformHandles['u_IsPerspectiveProjection'], isPerspective );

        for( idx in 0...scene.lightList.length ) {

            var scenePointLight = scene.lightList[ idx ];

            var pointLightColor = RGBAColor.getLightColor( scenePointLight );
            gl.uniform3f( program.uniformHandles['u_Lights[$idx].color'], pointLightColor.r, pointLightColor.g, pointLightColor.b );

            gl.uniform1f( program.uniformHandles['u_Lights[$idx].attenuation'], scenePointLight.attenuation );

            var pointLightPosition = scenePointLight.scenePosition;
            gl.uniform3f( program.uniformHandles['u_Lights[$idx].position'],
                pointLightPosition.x,
                pointLightPosition.y,
                pointLightPosition.z
            );
        }

        var cameraPosition = scene.activeCamera.scenePosition;
        gl.uniform3f( program.uniformHandles['u_CameraPosition'], 
            cameraPosition.x,
            cameraPosition.y,
            cameraPosition.z
        );

        gl.uniformMatrix4fv( program.uniformHandles['u_ProjectionMatrix'], false, scene.cameraTransform );
    }
}
