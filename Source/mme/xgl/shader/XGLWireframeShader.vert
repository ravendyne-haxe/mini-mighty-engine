attribute vec3 a_Position;
attribute vec3 a_Barycentric;

uniform mat4 u_TransformMatrix;

varying vec3 v_VertexEdgeParameter;

void main (void) {
    v_VertexEdgeParameter = a_Barycentric;
    gl_Position = u_TransformMatrix * vec4(a_Position, 1.0);
}
