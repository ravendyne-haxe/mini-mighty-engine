package mme.xgl.render;

import mme.core.RGBAColor;
import mme.xgl.geometry.XGLMeshConverter;
import mme.g2d.geometry.LineCollection2D;
import mme.g2d.geometry.PolyLine2D;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;

import mme.g2d.material.Line2DMaterial;

import mme.xgl.XGLRegistry;

import mme.xgl.material.XGLLine2DMaterial;
import mme.xgl.shader.XGLLine2DShader;

import mme.xgl.geometry.XGLGeometryBufferLine2D;
import mme.xgl.geometry.converter.XGLConvertPolyline2DToBufferLine2D;
import mme.xgl.geometry.converter.XGLConvertLineCollection2DToBufferLine2D;

import mme.xgl.engine.XGLRenderObject;


class XGLLine2DRenderObject extends XGLRenderObject {

    private var objectMaterial : Line2DMaterial;

    // TODO make these properties so we can get values from object material on every request
    private var blendStartValue : Float;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( sceneObject.material, Line2DMaterial ) ) {
            throw "!!BUG!!: XGLLine2DRenderObject requires Line2DMaterial.";
        }
        objectMaterial = cast sceneObject.material;
        switch( objectMaterial.lineBlending ) {
            case Sharp:
                blendStartValue = 1.0;
            case Smooth:
                blendStartValue = 0.7;
            case ThinLines:
                blendStartValue = 0.0;
        }
        // TODO remove this in the future, we're using `material` just for `material.color`
        material = new XGLLine2DMaterial( gl, objectMaterial );

        //
        // PROGRAM
        //

        shader = XGLRegistry.get( gl ).getShader( XGLLine2DShader.ID );

        //
        // GEOMETRY
        //

        var converter : XGLMeshConverter = null;
        if( Std.is( sceneObject, PolyLine2D ) ) {
            converter = new XGLConvertPolyline2DToBufferLine2D( cast sceneObject );
        }
        if( Std.is( sceneObject, LineCollection2D ) ) {
            converter = new XGLConvertLineCollection2DToBufferLine2D( cast sceneObject );
        }
        if( converter == null ) {
            throw "!!BUG!!: XGLLine2DRenderObject requires PolyLine2D or LineCollection2D.";
        }
        geometryBuffer = new XGLGeometryBufferLine2D( gl, shader, converter );
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform3f( shader.program.uniformHandles['u_LineColor'], material.color.r, material.color.g, material.color.b );
        gl.uniform3f( shader.program.uniformHandles['u_EdgeColor'], objectMaterial.edgeColor.r, objectMaterial.edgeColor.g, objectMaterial.edgeColor.b );

        gl.uniform1f( shader.program.uniformHandles['u_Inner'], objectMaterial.inner );
        gl.uniform1f( shader.program.uniformHandles['u_BlendStart'], blendStartValue );
        gl.uniform1f( shader.program.uniformHandles['u_Opacity'], objectMaterial.opacity );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );

        var thickness : Float = 2.0;
        if( Std.is( sceneObject, PolyLine2D ) ) {
            var object : PolyLine2D = cast sceneObject;
            thickness = object.thickness;
        }
        if( Std.is( sceneObject, LineCollection2D ) ) {
            var object : LineCollection2D = cast sceneObject;
            thickness = object.thickness;
        }
        gl.uniform1f( shader.program.uniformHandles['u_Thickness'], thickness );
    }
}
