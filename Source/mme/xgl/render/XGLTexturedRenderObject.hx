package mme.xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;

import mme.material.TextureMaterial;

import mme.xgl.XGLRegistry;

import mme.xgl.material.XGLTexturedMaterial;

import mme.xgl.geometry.XGLGeometryBufferUV;
import mme.xgl.geometry.converter.XGLConvertToBufferUV;
import mme.xgl.shader.XGLTextureShader;

import mme.xgl.engine.XGLRenderObject;


class XGLTexturedRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLTexturedMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( sceneObject.material, TextureMaterial ) ) {
            throw "!!BUG!!: XGLTexturedRenderObject requires TextureMaterial.";
        }
        var objectMaterial : TextureMaterial = cast sceneObject.material;
        renderMaterial = new XGLTexturedMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        shader = XGLRegistry.get( gl ).getShader( XGLTextureShader.ID );

        //
        // GEOMETRY
        //

        geometryBuffer = new XGLGeometryBufferUV( gl, shader, new XGLConvertToBufferUV( sceneObject.mesh ) );
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform1i( shader.program.uniformHandles['u_Texture'], renderMaterial.texture.textureUnit );

        //
        //  Setup TEXTURE
        //
        gl.activeTexture( GL.TEXTURE0 + renderMaterial.texture.textureUnit );
        gl.bindTexture( GL.TEXTURE_2D, renderMaterial.texture.textureHandle );
    }
    
    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );
    }
}

