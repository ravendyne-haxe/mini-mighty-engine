package mme.xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;

import mme.material.FontAtlasMaterial;

import mme.xgl.XGLRegistry;

import mme.xgl.material.XGLTexturedMaterial;

import mme.xgl.geometry.XGLGeometryBufferUV;
import mme.xgl.geometry.converter.XGLConvertToBufferUV;
import mme.xgl.shader.XGLSDFFontShader;

import mme.xgl.engine.XGLRenderObject;


class XGLFontAtlasRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLTexturedMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( sceneObject.material, FontAtlasMaterial ) ) {
            throw "!!BUG!!: XGLFontAtlasRenderObject requires FontAtlasMaterial.";
        }
        var objectMaterial : FontAtlasMaterial = cast sceneObject.material;
        renderMaterial = new XGLTexturedMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        shader = XGLRegistry.get( gl ).getShader( XGLSDFFontShader.ID );

        //
        // GEOMETRY
        //

        geometryBuffer = new XGLGeometryBufferUV( gl, shader, new XGLConvertToBufferUV( sceneObject.mesh ) );
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform1i( shader.program.uniformHandles['u_Texture'], renderMaterial.texture.textureUnit );

        // javascript doesn't support passing array[4] to uniform3fv, only array[3] will work
        // May be related to: https://www.khronos.org/registry/webgl/specs/latest/1.0/#5.14.10
        // "The length is invalid if it is too short for or is not an integer multiple of the assigned type."
        // ???
        gl.uniform3fv( shader.program.uniformHandles['u_TextColor'], material.color.toVec3() );
        // gl.uniform3f( shader.program.uniformHandles['u_TextColor'], material.color.r, material.color.g, material.color.b );

        //
        //  Setup TEXTURE
        //
        gl.activeTexture( GL.TEXTURE0 + renderMaterial.texture.textureUnit );
        gl.bindTexture( GL.TEXTURE_2D, renderMaterial.texture.textureHandle );
    }
    
    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );
    }
}

