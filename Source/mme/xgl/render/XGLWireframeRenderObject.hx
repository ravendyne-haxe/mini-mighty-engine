package mme.xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;

import mme.material.WireframeMaterial;

import mme.xgl.XGLRegistry;

import mme.xgl.material.XGLWireframeMaterial;

import mme.xgl.geometry.XGLGeometryBufferBarycentric;
import mme.xgl.geometry.converter.XGLConvertToBufferBarycentric;
import mme.xgl.shader.XGLWireframeShader;

import mme.xgl.engine.XGLRenderObject;


class XGLWireframeRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLWireframeMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( sceneObject.material, WireframeMaterial ) ) {
            throw "!!BUG!!: XGLWireframeRenderObject requires WireframeMaterial.";
        }
        var objectMaterial : WireframeMaterial = cast sceneObject.material;
        renderMaterial = new XGLWireframeMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        shader = XGLRegistry.get( gl ).getShader( XGLWireframeShader.ID );

        //
        // GEOMETRY
        //

        geometryBuffer = new XGLGeometryBufferBarycentric( gl, shader, new XGLConvertToBufferBarycentric( sceneObject.mesh ) );
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform3f( shader.program.uniformHandles['u_EmissiveColor'], material.color.r, material.color.g, material.color.b );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );
    }
}
