package mme.xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

using mme.math.glmatrix.Mat4Tools;

import mme.core.Scene;

import mme.g2d.material.Point2DMaterial;

import mme.xgl.XGLRegistry;

import mme.xgl.material.XGLPoint2DMaterial;

import mme.xgl.geometry.XGLGeometryBufferUV;
import mme.xgl.geometry.converter.XGLConvertPoint2DToBufferUV;
import mme.xgl.shader.XGLPoint2DShader;

import mme.xgl.engine.XGLRenderObject;


class XGLPoint2DRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLPoint2DMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( sceneObject.material, Point2DMaterial ) ) {
            throw "!!BUG!!: XGLPoint2DRenderObject requires Point2DMaterial.";
        }
        var objectMaterial : Point2DMaterial = cast sceneObject.material;
        renderMaterial = new XGLPoint2DMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        switch( objectMaterial.type ) {
            case Circle:
                shader = XGLRegistry.get( gl ).getShader( XGLPoint2DCircleShader.ID );
            case Square:
                shader = XGLRegistry.get( gl ).getShader( XGLPoint2DSquareShader.ID );
            case Diamond:
                shader = XGLRegistry.get( gl ).getShader( XGLPoint2DDiamondShader.ID );
            default:
                throw '!!BUG!!: unsupported Point2D type: ${objectMaterial.type}';
        }

        //
        // GEOMETRY
        //

        geometryBuffer = new XGLGeometryBufferUV( gl, shader, new XGLConvertPoint2DToBufferUV( sceneObject.mesh ) );
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform3f( shader.program.uniformHandles['u_PointColor'], material.color.r, material.color.g, material.color.b );
        gl.uniform3f( shader.program.uniformHandles['u_EdgeColor'], renderMaterial.edgeColor.r, renderMaterial.edgeColor.g, renderMaterial.edgeColor.b );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = scene.cameraTransform.mul( sceneObject.transform );
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, transform );
    }
}
