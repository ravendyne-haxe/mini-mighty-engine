package mme.xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import mme.core.Scene;

import mme.material.TexturedPhongMaterial;

import mme.xgl.XGLRegistry;

import mme.xgl.material.XGLTexturedPhongMaterial;

import mme.xgl.geometry.XGLGeometryBufferNormalUV;
import mme.xgl.geometry.converter.XGLConvertToBufferNormalUV;
import mme.xgl.shader.XGLTexturedPhongShader;

import mme.xgl.engine.XGLRenderObject;


class XGLTexturedPhongRenderObject extends XGLRenderObject {

    private var renderMaterial : XGLTexturedPhongMaterial;

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // MATERIAL
        //
        if( ! Std.is( sceneObject.material, TexturedPhongMaterial ) ) {
            throw "!!BUG!!: XGLTexturedPhongRenderObject requires TexturedPhongMaterial.";
        }
        var objectMaterial : TexturedPhongMaterial = cast sceneObject.material;
        renderMaterial = new XGLTexturedPhongMaterial( gl, objectMaterial );
        material = renderMaterial;

        //
        // PROGRAM
        //

        shader = XGLRegistry.get( gl ).getShader( XGLTexturedPhongShader.ID );

        //
        // GEOMETRY
        //

        geometryBuffer = new XGLGeometryBufferNormalUV( gl, shader, new XGLConvertToBufferNormalUV( sceneObject.mesh ) );
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //

        gl.uniform3fv( shader.program.uniformHandles['u_Ka'],renderMaterial.ka );
        gl.uniform3fv( shader.program.uniformHandles['u_Kd'],renderMaterial.kd );
        gl.uniform3fv( shader.program.uniformHandles['u_Ks'],renderMaterial.ks );

        gl.uniform1f( shader.program.uniformHandles['u_Shininess'], renderMaterial.shininess );

        gl.uniform1i( shader.program.uniformHandles['u_Texture'], renderMaterial.texture.textureUnit );

        //
        //  Setup TEXTURE
        //
        gl.activeTexture( GL.TEXTURE0 + renderMaterial.texture.textureUnit );
        gl.bindTexture( GL.TEXTURE_2D, renderMaterial.texture.textureHandle );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniformMatrix4fv( shader.program.uniformHandles['u_TransformMatrix'], false, sceneObject.transform );
        gl.uniformMatrix3fv( shader.program.uniformHandles['u_NormalTransform'], false, sceneObject.normalTransform );
    }
}
