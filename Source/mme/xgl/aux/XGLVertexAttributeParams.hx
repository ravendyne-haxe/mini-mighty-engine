package mme.xgl.aux;


class XGLVertexAttributeParams {
	// attribute buffer is an array of
	// attribute groups, each having same structure.
	// An attribute group contains one or more attribute values,
	// i.e. a group can contain one (X,Y) vertex position value and
	// one (U,V) texture coordinate value.
	// Each attribute can have 1, 2, 3 or 4 components.
	// Each attribute is defined within the group by:
	//		- number of components
	//		- attribute offset:
	// 		    where is the first value of attribute,
	//		    counting from where this group starts in the buffer


	// how many components there are in this attribute
	// i.e. for 3-component float vector, componentCount == 3
    public var componentCount : Int;
	// where does this attribute start, in bytes,
	// counting from the beginning of this attribute's group
    public var attributeOffset : Int;

    public function new( count : Int, offset : Int ) {
	    componentCount = count;
	    attributeOffset = offset;
	}
}
