package mme.xgl;

import lime.graphics.WebGLRenderContext;

import mme.core.SceneObject;

import mme.material.BasicMaterial;
import mme.material.TextureMaterial;
import mme.material.FontAtlasMaterial;
import mme.material.PhongMaterial;
import mme.material.LineMaterial;
import mme.material.TexturedPhongMaterial;
import mme.material.WireframeMaterial;
import mme.g2d.material.Line2DMaterial;
import mme.g2d.material.Point2DMaterial;

import mme.xgl.engine.XGLShader;
import mme.xgl.engine.XGLRenderObject;

import mme.xgl.render.XGLEmissiveColorRenderObject;
import mme.xgl.render.XGLWireframeRenderObject;
import mme.xgl.render.XGLPhongColorRenderObject;
import mme.xgl.render.XGLTexturedRenderObject;
import mme.xgl.render.XGLLine2DRenderObject;
import mme.xgl.render.XGLPoint2DRenderObject;
import mme.xgl.render.XGLLineRenderObject;
import mme.xgl.render.XGLTexturedPhongRenderObject;
import mme.xgl.render.XGLFontAtlasRenderObject;

import mme.xgl.shader.XGLEmissiveColorShader;
import mme.xgl.shader.XGLWireframeShader;
import mme.xgl.shader.XGLPhongColorShader;
import mme.xgl.shader.XGLTextureShader;
import mme.xgl.shader.XGLLine2DShader;
import mme.xgl.shader.XGLPoint2DShader.XGLPoint2DCircleShader;
import mme.xgl.shader.XGLPoint2DShader.XGLPoint2DSquareShader;
import mme.xgl.shader.XGLPoint2DShader.XGLPoint2DDiamondShader;
import mme.xgl.shader.XGLLineShader;
import mme.xgl.shader.XGLTexturedPhongShader;
import mme.xgl.shader.XGLSDFFontShader;


interface XGLRegistryExtension {
    function initialize( gl : WebGLRenderContext ) : Void;
    function dispose( gl : WebGLRenderContext ) : Void;
    function getShader( shaderName : Int ) : XGLShader;
    function createRenderObjectFor( gl : WebGLRenderContext, sceneObject : SceneObject ) : XGLRenderObject;
}

class XGLRegistry {

    private static var instances : Map<Int, XGLRegistry>;

    private var shaders : Map<Int, XGLShader>;

    private var extensions : Array< XGLRegistryExtension >;

    // TODO this sucks
    private static function getContextId( gl : WebGLRenderContext ) : Int {

        if( ! Reflect.hasField(gl, "__contextID") )
            return -1;
        
        return Reflect.field(gl, "__contextID");
    }

    public static function initialize( gl : WebGLRenderContext ) : Void {
        if( instances == null ) {
            instances = new Map();
        }

        var key = getContextId( gl );
        if( instances.exists( key ) )
            return;
        
        var registry = new XGLRegistry( gl );
        instances.set( key, registry );
    }

    public static function get( gl : WebGLRenderContext ) : XGLRegistry {
        if( instances == null ) return null;
        var key = getContextId( gl );
        return instances.get( key );
    }

    public function new( gl : WebGLRenderContext ) : Void {

        shaders = new Map();

        shaders.set( XGLEmissiveColorShader.ID, new XGLEmissiveColorShader( gl ) );
        shaders.set( XGLWireframeShader.ID, new XGLWireframeShader( gl ) );
        shaders.set( XGLPhongColorShader.ID, new XGLPhongColorShader( gl ) );
        shaders.set( XGLTextureShader.ID, new XGLTextureShader( gl ) );
        shaders.set( XGLLine2DShader.ID, new XGLLine2DShader( gl ) );
        shaders.set( XGLPoint2DCircleShader.ID, new XGLPoint2DCircleShader( gl ) );
        shaders.set( XGLPoint2DSquareShader.ID, new XGLPoint2DSquareShader( gl ) );
        shaders.set( XGLPoint2DDiamondShader.ID, new XGLPoint2DDiamondShader( gl ) );
        shaders.set( XGLLineShader.ID, new XGLLineShader( gl ) );
        shaders.set( XGLTexturedPhongShader.ID, new XGLTexturedPhongShader( gl ) );
        shaders.set( XGLSDFFontShader.ID, new XGLSDFFontShader( gl ) );

        extensions = [];
    }

    public function registerExtension( extension : XGLRegistryExtension ) {
        extensions.push( extension );
    }

    public function dispose( gl : WebGLRenderContext ) {

        for (key => shader in shaders) {
            shader.dispose( gl );
            shaders.remove( key );
        }

        for( ext in extensions ) {
            ext.dispose( gl );
        }
    }

    public function getShader( shaderId : Int ) : XGLShader {

        for( ext in extensions ) {
            var shader = ext.getShader( shaderId );
            if( shader != null ) return shader;
        }

        if( shaders.exists( shaderId ) ) {
            return shaders.get( shaderId );
        }

        return null;
    }

    public function createRenderObjectFor( gl : WebGLRenderContext, sceneObject : SceneObject ) : XGLRenderObject {

        for( ext in extensions ) {
            var ro = ext.createRenderObjectFor( gl, sceneObject );
            if( ro != null ) return ro;
        }

        if( Std.is( sceneObject.material, BasicMaterial ) ) {

            return new XGLEmissiveColorRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, WireframeMaterial ) ) {

            return new XGLWireframeRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, PhongMaterial ) ) {

            return new XGLPhongColorRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, FontAtlasMaterial ) ) {

            return new XGLFontAtlasRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, TextureMaterial ) ) {

            return new XGLTexturedRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, Line2DMaterial ) ) {

            return new XGLLine2DRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, Point2DMaterial ) ) {

            return new XGLPoint2DRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, LineMaterial ) ) {

            return new XGLLineRenderObject( gl, sceneObject );
        }

        if( Std.is( sceneObject.material, TexturedPhongMaterial ) ) {

            return new XGLTexturedPhongRenderObject( gl, sceneObject );
        }


        return null;
    }
}
