package mme.xgl.geometry;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLGeometryBuffer;
import mme.xgl.engine.XGLShader;


class XGLGeometryBufferLine extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLMeshConverter ) {

        // attribute format:
        // X, Y, Z, topBottom, startEnd -> number of elements == 5
        super( gl, shader, meshConverter, 5 );

        // this one is a bit tricky:
        // sum of all attributes' elements ( 3 + 3 + 1 + 1 + 3 = 11 ) can't actually fit
        // into number of elements we gave as parameter to super().
        // this is because we are using overlapping attributes.
        // buffer will have 4 extra attributes added so we will not
        // overshoot 'curPosition' and 'nextPosition' on the last element

        //
        // value from 'previous' attribute
        //
        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'a_PrevPosition', 3, 0 );
        // we now skip prevposition's top/bottom & start/end...
        // 3
        // + 1 ('a_TopBottom')
        // + 1 ('a_StartEnd')
        // ... + ONE ENTIRE copy of it:
        // + 5
        // -> offset = 10

        //
        // value from 'current', main attribute, which is of size 5
        //
        // X, Y, Z -> number of elements == 3, offset == 10
		buffer.addAttribute( 'a_CurPosition', 3, 10 );
        // top/bottom -> number of elements == 1, offset == 10 + 3
        // we wan't 'top/bottom' that comes with 'curPosition'
		buffer.addAttribute( 'a_TopBottom', 1, 10 + 3 );
        // start/end -> number of elements == 1, offset == 10 + 3 + 1
        // we wan't 'top/bottom' that comes with 'curPosition'
		buffer.addAttribute( 'a_StartEnd', 1, 10 + 3 + 1 );

        //
        // value from 'next' attribute
        //
        // we now skip curposition's ONE ENTIRE copy
        // 5 elements -> offset = 15 + 5 = 20
        // X, Y, Z -> number of elements == 3, offset == 20
		buffer.addAttribute( 'a_NextPosition', 3, 20 );
    }
}
