package mme.xgl.geometry;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLGeometryBuffer;
import mme.xgl.engine.XGLShader;


class XGLGeometryBufferLine2D extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLMeshConverter ) {

        // attribute format:
        // X, Y, Z, side, distance -> number of elements == 5
        super( gl, shader, meshConverter, 5 );

        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'a_Position', 3, 0 );
        // side -> number of elements == 1, offset == 3
		buffer.addAttribute( 'a_Side', 1, 3 );
        // distance -> number of elements == 1, offset == 4
		buffer.addAttribute( 'a_Distance', 1, 4 );
    }
}
