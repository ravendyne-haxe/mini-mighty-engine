package mme.xgl.geometry;

import lime.utils.UInt16Array;
import lime.utils.Float32Array;
import lime.graphics.opengl.GL;
import mme.core.Mesh;


class XGLMeshConverter {

    private var mesh : Mesh;

    public var attributeBuffer ( default, null ) : Float32Array;
    public var indicesBuffer ( default, null ) : UInt16Array;
    public var isIndexed ( default, null ) : Bool;

    public function new( mesh : Mesh ) {
        this.mesh = mesh;
        isIndexed = false;
    }

    public function dispose() {
        if( attributeBuffer != null ) attributeBuffer = null;
        if( indicesBuffer != null ) indicesBuffer = null;
    }

    public function setAttributeBufferData( data : Array< Float > ) {
        // TODO : attributeBuffer.set( data );
        attributeBuffer = new Float32Array( data );
    }

    public function setIndicesBufferData( data : Array< Int > ) {
        // TODO : indicesBuffer.set( data );
        indicesBuffer = new UInt16Array( data );
    }

    private function transformMesh( mesh : Mesh ) : Mesh { return mesh; }
    private function convertToArrays( mesh : Mesh ) : Void { convertToArraysGeneric( mesh ); }
    private function pushData( mesh : Mesh, vertexArray : Array<Float>, idx : Int ) : Void { }

    public function convert() : Void {
        var transformedMesh = transformMesh( mesh );
        convertToArrays( transformedMesh );
    }

    public function getUsage() : Int {

        switch( mesh.usage ) {

            case MeshStatic:
            return GL.STATIC_DRAW;

            case MeshDynamic:
            return GL.DYNAMIC_DRAW;

            case MeshStream:
            return GL.STREAM_DRAW;
        }

        return GL.STATIC_DRAW;
    }

    ////////////////////////////////////////////////////////////////////////////////
    //
    // Different mesh-to-arrays converting algorithms
    // which can be used in general cases
    //
    ////////////////////////////////////////////////////////////////////////////////

    private function convertToArraysGeneric( mesh : Mesh ) : Void {

        var vertexArray = [];
        var indicesArray = [];
        isIndexed = mesh.isIndexed;

        if( mesh.elementType == Quad ) {

            // we need to convert quads to triangles
            for( el in mesh ) {

                // T1
                pushData( mesh, vertexArray, el.i0 );
                pushData( mesh, vertexArray, el.i1 );
                pushData( mesh, vertexArray, el.i2 );
                // T2
                pushData( mesh, vertexArray, el.i0 );
                pushData( mesh, vertexArray, el.i2 );
                pushData( mesh, vertexArray, el.i3 );
                if( isIndexed ) {
                    // T1
                    indicesArray.push( el.i0 );
                    indicesArray.push( el.i1 );
                    indicesArray.push( el.i2 );
                    // T2
                    indicesArray.push( el.i0 );
                    indicesArray.push( el.i2 );
                    indicesArray.push( el.i3 );
                }
            }

        } else {

            // points, lines and triangles we can do trivially
            for( idx in 0...mesh.vertices.length ) {
                pushData( mesh, vertexArray, idx );
            }

            indicesArray = mesh.indices;
        }

        setAttributeBufferData( vertexArray );

        if( isIndexed ) {
            setIndicesBufferData( indicesArray );
        }
    }
}
