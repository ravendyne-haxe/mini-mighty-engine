package mme.xgl.geometry;

import lime.graphics.WebGLRenderContext;

import mme.xgl.engine.XGLGeometryBuffer;
import mme.xgl.engine.XGLShader;


class XGLGeometryBufferNormalUV extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, meshConverter : XGLMeshConverter ) {

        // attribute format:
        // X, Y, Z, Nx, Ny, Nz, U, V -> number of elements == 8
        super( gl, shader, meshConverter, 8 );

        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'a_Position', 3, 0 );
        // Nx, Ny, Nz -> number of elements == 3, offset == 3
		buffer.addAttribute( 'a_Normal', 3, 3 );
        // U, V -> number of elements == 2, offset == 6
		buffer.addAttribute( 'a_TextureUV', 2, 6 );
    }
}
