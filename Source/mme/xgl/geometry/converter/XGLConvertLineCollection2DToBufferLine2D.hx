package mme.xgl.geometry.converter;

import mme.util.Utils;
import mme.util.geometry.line2d.LineStroker;
import mme.g2d.material.Line2DMaterial;
import mme.xgl.geometry.converter.PolylineToMesh;
import mme.g2d.geometry.LineCollection2D;

import mme.core.Mesh;


class XGLConvertLineCollection2DToBufferLine2D extends XGLMeshConverter {

    var sceneObject : LineCollection2D;

    public var approximationScale : Float;

    public function new( sceneObject : LineCollection2D ) {
        super( sceneObject.mesh );
        this.sceneObject = sceneObject;
        // approximationScale = 1.0;
        // approximationScale = 0.125;
        approximationScale = 0.0625;
    }

    override private function pushData( mesh : Mesh, vertexArray : Array<Float>, idx : Int ) {

        vertexArray.push( mesh.vertices[ idx ].x );
        vertexArray.push( mesh.vertices[ idx ].y );
        vertexArray.push( mesh.vertices[ idx ].z );

        // side
        vertexArray.push( mesh.otherParams[ idx ][0] );
        // distance
        vertexArray.push( mesh.otherParams[ idx ][1] );
    }

    override private function transformMesh( mesh : Mesh ) : Mesh {

        var transformedMesh = new Mesh();
        var material : Line2DMaterial = cast sceneObject.material;
        var ms = new LineStroker();

        if( material.quickLine ) {

            var idx = 0;
            while( idx < sceneObject.mesh.vertices.length  ) {

                var v0 = sceneObject.mesh.vertices[ idx ];
                var v1 = sceneObject.mesh.vertices[ idx + 1 ];

                PolylineToMesh.createFromPointArrayQuick( [ v0, v1 ], ms, sceneObject.thickness, transformedMesh, false );

                idx += 2;
            }

        } else {

            ms.set_line_cap( material.lineCap );
            ms.set_inner_join( material.lineInnerJoin );
            ms.set_line_join( material.lineJoin );

            ms.set_approximation_scale( approximationScale );


            var idx = 0;
            while( idx < sceneObject.mesh.vertices.length  ) {

                var v0 = sceneObject.mesh.vertices[ idx ];
                var v1 = sceneObject.mesh.vertices[ idx + 1 ];

                PolylineToMesh.createFromPointArray( [ v0, v1 ], ms, sceneObject.thickness, transformedMesh, false );

                idx += 2;
            }
        }

        return transformedMesh;
    }
}
