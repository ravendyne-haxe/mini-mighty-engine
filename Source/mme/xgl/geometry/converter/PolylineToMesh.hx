package mme.xgl.geometry.converter;

import mme.util.geometry.AggMath;
import mme.core.Mesh;
import mme.math.glmatrix.Vec2;
using mme.math.glmatrix.Vec2Tools;
import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;

import mme.util.geometry.line2d.LineStroker;
import mme.util.geometry.line2d.LineStroker.LineCapEnum;


class PolylineToMesh {

    public static function createFromPointArrayQuick( points : Array<Vec3>, ms : LineStroker, width : Float, mesh : Mesh, isClosed : Bool ) : Void {

        if( points.length < 2 ) throw 'MME: ------------> this won\'t work: points.length is ${points.length}';

        var vc = new SimpleVertexConsumer();

        //
        // cap for start of line
        //
        ms.set_width( width );
        for( idx in 0...points.length - 1 ) {
            var prev = idx + 0;
            var curr = idx + 1;
            triangulateSegmentQuick( points, prev, curr, mesh, vc, ms );
        }
        if( isClosed ) {
            triangulateSegmentQuick( points, points.length - 1, 0, mesh, vc, ms );
        }
    }

    static inline function triangulateSegmentQuick( points : Array<Vec3>, prev : Int, curr : Int, mesh : Mesh, vc : SimpleVertexConsumer, ms : LineStroker ) {

            // curr - prev
            var lenCurrPrev = points[ curr ].sub( points[ prev ] ).len();

            //
            // START cap for current line segment
            //
            ms.calc_cap_quick( vc,
                points[ prev ], points[ curr ],
                lenCurrPrev
            );

            var outerStartIndex = mesh.vertices.length;
            addToMeshForQuick( mesh, vc.vertices, true, 0.0 );
            var outerEndIndex = mesh.vertices.length - 1;

            //
            // END cap for current line segment
            //
            ms.calc_cap_quick( vc,
                points[ curr ], points[ prev ], // reverse order!!! we want cap on end point!
                lenCurrPrev
            );

            var innerStartIndex = mesh.vertices.length;
            addToMeshForQuick( mesh, vc.vertices, false, 0.0 );
            var innerEndIndex = mesh.vertices.length - 1;

            //
            // triangulate outline for current point
            // connecting it to the previous one via 'quadLeftSide'
            //
            triangulateQuad( [ outerStartIndex, outerEndIndex ], [ innerStartIndex, innerEndIndex ], mesh );
    }

    static function addToMeshForQuick( mesh : Mesh, points : Array<Vec2>, isStart : Bool, currentLength : Float ) {

        mesh.vertices.push([ points[ 0 ].x, points[ 0 ].y, 0.0 ]);
        mesh.vertices.push([ points[ 1 ].x, points[ 1 ].y, 0.0 ]);

        if( isStart ) {
            mesh.otherParams.push([ -1, currentLength ]);
            mesh.otherParams.push([ 1, currentLength ]);
        } else {
            mesh.otherParams.push([ 1, currentLength ]);
            mesh.otherParams.push([ -1, currentLength ]);
        }

        mesh.normals.push([ 0, 0, 1 ]);
        mesh.normals.push([ 0, 0, 1 ]);
        // dummy UVs
        mesh.uvs.push([ 0, 0 ]);
        mesh.uvs.push([ 0, 0 ]);
    }

    public static function createFromPointArray( points : Array<Vec3>, ms : LineStroker, width : Float, mesh : Mesh, isClosed : Bool ) : Void {
        if( isClosed ) createFromPointArrayClosed( points, ms, width, mesh );
        else createFromPointArrayOpen( points, ms, width, mesh );
    }

    static function createFromPointArrayClosed( points : Array<Vec3>, ms : LineStroker, width : Float, mesh : Mesh ) : Void {

        if( points.length < 2 ) throw 'MME: ------------> this won\'t work: points.length is ${points.length}';

        var vc = new SimpleVertexConsumer();

        var currentLength = 0.0;

        var quadLeftSide = [ mesh.vertices.length, mesh.vertices.length + 1 ];

        // for( idx in 0...points.length - 2 ) {
        for( idx in 0...points.length + 1 ) {
            var prev = idx + 0;
            var curr = idx + 1;
            var next = idx + 2;
            if( prev >= points.length ) prev = prev - points.length;
            if( curr >= points.length ) curr = curr - points.length;
            if( next >= points.length ) next = next - points.length;

            // curr - prev
            var lenCurrPrev = points[ curr ].sub( points[ prev ] ).len();
            // next - curr
            var lenNextCurr = points[ next ].sub( points[ curr ] ).len();

            currentLength += lenCurrPrev;

            //
            // OUTER outline for current point
            //
            ms.set_width( width );
            ms.calc_join( vc,
                // prev, curr, next
                points[ prev ], points[ curr ], points[ next ],
                // curr - prev
                lenCurrPrev,
                // next - curr
                lenNextCurr
            );

            // ms.calc_cap() and calc_join() both call vc.remove_all() at start
            // vc.remove_all() will 'clear' vertices array by creating a new one
            // so we can just grab a reference here without copying content
            var outer = vc.vertices;

            //
            // INNER outline for current point
            //
            ms.set_width( -width );
            ms.calc_join( vc,
                // prev, curr, next
                points[ prev ], points[ curr ], points[ next ],
                // curr - prev
                lenCurrPrev,
                // next - curr
                lenNextCurr
            );

            // ms.calc_cap() and calc_join() both call vc.remove_all() at start
            // vc.remove_all() will 'clear' vertices array by creating a new one
            // so we can just grab a reference here without copying content
            var inner = vc.vertices;

            //
            // triangulate outline for current point
            // connecting it to the previous one via 'quadLeftSide'
            //
            triangulateQuadsAndFan( quadLeftSide, inner, outer, mesh, currentLength );
        }
    }

    static function createFromPointArrayOpen( points : Array<Vec3>, ms : LineStroker, width : Float, mesh : Mesh ) : Void {

        if( points.length < 2 ) throw 'MME: ------------> this won\'t work: points.length is ${points.length}';

        var vc = new SimpleVertexConsumer();

        //
        // cap for start of line
        //
        ms.set_width( width );
        var currentLength = points[ 1 ].sub(points[ 0 ]).len();
        ms.calc_cap( vc,
            points[ 0 ], points[ 1 ],
            currentLength
        );

        currentLength = 0.0;

        //
        // add new points to the mesh
        //
        var pointsStartIndex = mesh.vertices.length;
        var keyPoints = addToMeshEndPoint( mesh, vc.vertices, -1, currentLength, ms.get_line_cap() );
        var quadLeftSide = [ keyPoints[ 2 ], keyPoints[ 3 ] ];

        //
        // triangulate start of line
        //
        triangulateFan( pointsStartIndex, keyPoints[ 0 ], keyPoints[ 1 ], mesh, 1 );

        //
        // mid-line section outline
        //
        for( idx in 0...points.length - 2 ) {
            var prev = idx + 0;
            var curr = idx + 1;
            var next = idx + 2;

            // curr - prev
            var lenCurrPrev = points[ curr ].sub( points[ prev ] ).len();
            // next - curr
            var lenNextCurr = points[ next ].sub( points[ curr ] ).len();

            currentLength += lenCurrPrev;

            //
            // OUTER outline for current point
            //
            ms.set_width( width );
            ms.calc_join( vc,
                // prev, curr, next
                points[ prev ], points[ curr ], points[ next ],
                // curr - prev
                lenCurrPrev,
                // next - curr
                lenNextCurr
            );

            // ms.calc_cap() and calc_join() both call vc.remove_all() at start
            // vc.remove_all() will 'clear' vertices array by creating a new one
            // so we can just grab a reference here without copying content
            var outer = vc.vertices;

            //
            // INNER outline for current point
            //
            ms.set_width( -width );
            ms.calc_join( vc,
                // prev, curr, next
                points[ prev ], points[ curr ], points[ next ],
                // curr - prev
                lenCurrPrev,
                // next - curr
                lenNextCurr
            );

            // ms.calc_cap() and calc_join() both call vc.remove_all() at start
            // vc.remove_all() will 'clear' vertices array by creating a new one
            // so we can just grab a reference here without copying content
            var inner = vc.vertices;

            //
            // triangulate outline for current point
            // connecting it to the previous one via 'quadLeftSide'
            //
            triangulateQuadsAndFan( quadLeftSide, inner, outer, mesh, currentLength );
        }

        //
        // cap for end of line
        //
        ms.set_width( width );
        var lastLegLength = points[ points.length - 1 ].sub(points[ points.length - 2 ]).len();
        ms.calc_cap( vc,
            points[ points.length - 1 ], points[ points.length - 2 ], // reverse order!!! we want cap on end point!
            lastLegLength
        );
        currentLength += lastLegLength;

        //
        // add new points to the mesh
        //
        pointsStartIndex = mesh.vertices.length;
        var keyPoints = addToMeshEndPoint( mesh, vc.vertices, 1, currentLength, ms.get_line_cap() );
        var quadRightSide = [ keyPoints[ 2 ], keyPoints[ 3 ] ];

        //
        // triangulate end of line
        //
        triangulateFan( pointsStartIndex, keyPoints[ 0 ], keyPoints[ 1 ], mesh, 1 );

        //
        // triangulate section between end of line and previous point
        //
        triangulateQuad( quadLeftSide, quadRightSide, mesh );
    }

    static function addToMeshEndPoint( mesh : Mesh, points : Array<Vec2>, position : Float, currentLength : Float, lineCap : LineCapEnum ) : Array<Int> {

        if( lineCap == round_cap ) {
            var p0 = points[ 0 ];
            var pe = points[ points.length - 1 ];

            // pivot point in middle of the line for round_cap only
            var midVertex = p0.add( pe ).scale( 0.5 );
            mesh.vertices.push([ midVertex.x, midVertex.y, 0.0 ]);
            mesh.otherParams.push([ 0.0, currentLength ]);
        }

        // next point is start of triangle fan
        var fanFirstIndex = mesh.vertices.length;

        var pos = position;
        var halfwayIdx = points.length / 2;
        var currentIdx = 0;
        for( p in points ) {

            mesh.vertices.push([ p.x, p.y, 0.0 ]);

            if( lineCap != round_cap ) {

                if( currentIdx < halfwayIdx ) pos = position;
                else pos = - position;

            } else {

                pos = 1.0;
            }

            mesh.otherParams.push([ pos, currentLength ]);

            mesh.normals.push([ 0, 0, 1 ]);
            // dummy UVs
            mesh.uvs.push([ 0, 0 ]);

            currentIdx++;
        }

        var fanLastIndex = mesh.vertices.length - 1;

        var quadFirstIndex = fanFirstIndex;
        var quadLastIndex = fanLastIndex;

        if( lineCap == round_cap ) {
            var p0 = points[ 0 ];
            var pe = points[ points.length - 1 ];

            // duplicate first and last point so we can render cap
            // independently of line body
            //
            mesh.vertices.push([ p0.x, p0.y, 0.0 ]);
            mesh.otherParams.push([ position, currentLength ]);
            quadFirstIndex = mesh.vertices.length - 1;
            mesh.vertices.push([ pe.x, pe.y, 0.0 ]);
            mesh.otherParams.push([ - position, currentLength ]);
            quadLastIndex = mesh.vertices.length - 1;
        }

        return [ fanFirstIndex, fanLastIndex, quadFirstIndex, quadLastIndex ];
    }

    static function addToMesh( mesh : Mesh, points : Array<Vec2>, position : Float, currentLength : Float ) {

        for( p in points ) {

            mesh.vertices.push([ p.x, p.y, 0.0 ]);

            mesh.otherParams.push([ position, currentLength ]);

            mesh.normals.push([ 0, 0, 1 ]);
            // dummy UVs
            mesh.uvs.push([ 0, 0 ]);
        }
    }

    static function triangulateQuad( quadLeftSide : Array<Int>, quadRightSide : Array<Int>, mesh : Mesh ) {

        // quadLeftSide:
        // 0 - left-top
        // 1 - left-bottom
        var i0 = quadLeftSide[ 0 ];
        var i1 = quadLeftSide[ 1 ];
        // quadRightSide:
        // 0 - right-bottom
        // 1 - right-top
        var i2 = quadRightSide[ 0 ];
        var i3 = quadRightSide[ 1 ];

        mesh.indices.push( i0 );
        mesh.indices.push( i1 );
        mesh.indices.push( i2 );

        mesh.indices.push( i2 );
        mesh.indices.push( i3 );
        mesh.indices.push( i0 );
    }

    /**
        Generate indexed triangles that all have common vertex at `pivotPointIndex` and are
        spanning across all vertices starting at `fanPointsStartIndex` and ending at `fanPointsEndIndex`.

        We use `fanPosition` to keep triangle orientation correct.
    **/
    static function triangulateFan( pivotPointIndex : Int, fanPointsStartIndex : Int, fanPointsEndIndex : Int, mesh : Mesh, fanPosition : Int ) {

        for( idx in fanPointsStartIndex...fanPointsEndIndex ) {
            switch( fanPosition ) {
                case -1:
                    mesh.indices.push( pivotPointIndex );
                    mesh.indices.push( idx + 1 );
                    mesh.indices.push( idx );
                case 1:
                    mesh.indices.push( pivotPointIndex );
                    mesh.indices.push( idx );
                    mesh.indices.push( idx + 1 );
                default:
            }
        }
    }

    static function triangulateQuadsAndFan( quadLeftSide : Array<Int>, inner : Array<Vec2>, outer : Array<Vec2>, mesh : Mesh, currentLength : Float ) {

        //
        // add both inner and outer points to the mesh
        // and mark their starting indexes
        //
        var innerStartIndex = mesh.vertices.length;
        addToMesh( mesh, inner, -1, currentLength );
        var innerEndIndex = mesh.vertices.length - 1;

        var outerStartIndex = mesh.vertices.length;
        addToMesh( mesh, outer, 1, currentLength );
        var outerEndIndex = mesh.vertices.length - 1;

        //
        // triangulate quads until either inner or outer vertex array is gone
        //
        var quadCount = inner.length < outer.length ? inner.length : outer.length;

        var idx = 0;
        while( idx < quadCount ) {
            var innerIdx = innerStartIndex + idx;
            var outerIdx = outerStartIndex + idx;
            triangulateQuad( quadLeftSide, [ outerIdx, innerIdx ], mesh);
            quadLeftSide[ 0 ] = innerIdx;
            quadLeftSide[ 1 ] = outerIdx;
            idx++;
        }


        //
        // whatever vertices are left, either in inner or outer array,
        // create triangle fan
        //
        var pivotPointIndex : Int;
        var fanPointsStartIndex : Int;
        var fanPointsEndIndex : Int;
        var fanPosition : Int;
        if( quadCount == inner.length ) {
            //
            // all inner points are done
            // we have more points in outer array
            //
            pivotPointIndex = quadLeftSide[ 0 ]; // pivot around last inner point
            fanPointsStartIndex = quadLeftSide[ 1 ]; // triangulate starting with last outer point
            fanPointsEndIndex = outerEndIndex;
            fanPosition = 1;
            // update quadLeftSide
            quadLeftSide[ 0 ] = pivotPointIndex;
            quadLeftSide[ 1 ] = outerEndIndex;
        } else {
            //
            // all outer points are done
            // we have more points in inner array
            //
            fanPointsStartIndex = quadLeftSide[ 0 ]; // triangulate starting with last inner point
            pivotPointIndex = quadLeftSide[ 1 ]; // pivot around last outer point
            fanPointsEndIndex = innerEndIndex;
            fanPosition = -1;
            // update quadLeftSide
            quadLeftSide[ 0 ] = innerEndIndex;
            quadLeftSide[ 1 ] = pivotPointIndex;
        }
        triangulateFan( pivotPointIndex, fanPointsStartIndex, fanPointsEndIndex, mesh, fanPosition );
    }
}
