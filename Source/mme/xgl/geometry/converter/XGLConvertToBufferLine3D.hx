package mme.xgl.geometry.converter;

import mme.core.Mesh;


class XGLConvertToBufferLine3D extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override private function pushData( mesh : Mesh, vertexArray : Array<Float>, idx : Int ) {

        vertexArray.push( mesh.vertices[ idx ].x );
        vertexArray.push( mesh.vertices[ idx ].y );
        vertexArray.push( mesh.vertices[ idx ].z );

        // top/bottom
        vertexArray.push( mesh.otherParams[ idx ][0] );
        // start/end
        vertexArray.push( mesh.otherParams[ idx ][1] );
    }

    override private function transformMesh( mesh : Mesh ) : Mesh {

        var transformedMesh = new Mesh();
        var path = mesh.vertices;
        var vTop = 1;
        var vBottom = -1;
        var vStart = 1;
        var vEnd = -1;

        // each line consists of two points.
        // we will be rendering lines using quads and for
        // that we need 4 points. to get a quad for a line
        // we simply duplicate each of the line's two points and add
        // a parameter (+1/-1 value) to each. one parameter tells us
        // on what side of the line point is (top/bottom) and the other
        // parameter tells if the point is start or end point of the line.

		for( idx in 0...path.length ) {

            // duplicate each point in the path
            // marking one to be offset in one direction ( vTop )
            // and the other in the opposite direction ( vBottom )
            transformedMesh.vertices.push( path[ idx ] );
            transformedMesh.otherParams.push([ vTop, vEnd ]);
            transformedMesh.vertices.push( path[ idx ] );
            transformedMesh.otherParams.push([ vBottom, vEnd ]);

            // make another copy as a start of next line
            transformedMesh.vertices.push( path[ idx ] );
            transformedMesh.otherParams.push([ vTop, vStart ]);
            transformedMesh.vertices.push( path[ idx ] );
            transformedMesh.otherParams.push([ vBottom, vStart ]);
		}

        transformedMesh.indices = createIndices( path.length );

        return transformedMesh;
    }

    function createIndices( length : Int ) : Array<Int> {

        var indices = new Array<Int>();
        var c = 0, index = 0;

        for ( j in 0...length-1 ) {

            var i = index;

            // T1
            indices[c++] = i + 0;
            indices[c++] = i + 1;
            indices[c++] = i + 2;
            // T2
            indices[c++] = i + 2;
            indices[c++] = i + 1;
            indices[c++] = i + 3;

            index += 4;
        }

        return indices;
    }
}
