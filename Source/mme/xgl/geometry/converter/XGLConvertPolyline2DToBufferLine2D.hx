package mme.xgl.geometry.converter;

import mme.util.geometry.line2d.LineStroker;
import mme.g2d.material.Line2DMaterial;
import mme.xgl.geometry.converter.PolylineToMesh;
import mme.g2d.geometry.PolyLine2D;

import mme.core.Mesh;


class XGLConvertPolyline2DToBufferLine2D extends XGLMeshConverter {

    var sceneObject : PolyLine2D;

    public var approximationScale : Float;

    public function new( sceneObject : PolyLine2D ) {
        super( sceneObject.mesh );
        this.sceneObject = sceneObject;
        // approximationScale = 1.0;
        // approximationScale = 0.125;
        approximationScale = 0.0625;
    }

    override private function pushData( mesh : Mesh, vertexArray : Array<Float>, idx : Int ) {

        vertexArray.push( mesh.vertices[ idx ].x );
        vertexArray.push( mesh.vertices[ idx ].y );
        vertexArray.push( mesh.vertices[ idx ].z );

        // side
        vertexArray.push( mesh.otherParams[ idx ][0] );
        // distance
        vertexArray.push( mesh.otherParams[ idx ][1] );
    }

    override private function transformMesh( mesh : Mesh ) : Mesh {

        var transformedMesh = new Mesh();
        var material : Line2DMaterial = cast sceneObject.material;
        var ms = new LineStroker();

        if( material.quickLine ) {

            PolylineToMesh.createFromPointArrayQuick( sceneObject.mesh.vertices, ms, sceneObject.thickness, transformedMesh, sceneObject.isClosed );

        } else {

            ms.set_line_cap( material.lineCap );
            ms.set_inner_join( material.lineInnerJoin );
            ms.set_line_join( material.lineJoin );

            ms.set_approximation_scale( approximationScale );

            PolylineToMesh.createFromPointArray( sceneObject.mesh.vertices, ms, sceneObject.thickness, transformedMesh, sceneObject.isClosed );
        }

        return transformedMesh;
    }
}
