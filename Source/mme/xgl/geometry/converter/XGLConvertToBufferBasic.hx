package mme.xgl.geometry.converter;

import mme.core.Mesh;


class XGLConvertToBufferBasic extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override private function pushData( mesh : Mesh, vertexArray : Array<Float>, idx : Int ) {
        vertexArray.push( mesh.vertices[ idx ].x );
        vertexArray.push( mesh.vertices[ idx ].y );
        vertexArray.push( mesh.vertices[ idx ].z );
    }
}
