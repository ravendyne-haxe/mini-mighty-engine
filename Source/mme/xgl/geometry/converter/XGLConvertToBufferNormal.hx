package mme.xgl.geometry.converter;

import mme.core.Mesh;


class XGLConvertToBufferNormal extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override private function pushData( mesh : Mesh, vertexArray : Array<Float>, idx : Int ) {
        vertexArray.push( mesh.vertices[ idx ].x );
        vertexArray.push( mesh.vertices[ idx ].y );
        vertexArray.push( mesh.vertices[ idx ].z );
        vertexArray.push( mesh.normals[ idx ].x );
        vertexArray.push( mesh.normals[ idx ].y );
        vertexArray.push( mesh.normals[ idx ].z );
    }
}
