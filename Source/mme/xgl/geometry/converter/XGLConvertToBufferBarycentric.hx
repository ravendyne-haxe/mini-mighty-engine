package mme.xgl.geometry.converter;

import mme.math.glmatrix.Vec3;
import mme.core.Mesh;


class XGLConvertToBufferBarycentric extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override private function pushData( mesh : Mesh, vertexArray : Array<Float>, idx : Int ) {

        vertexArray.push( mesh.vertices[ idx ].x );
        vertexArray.push( mesh.vertices[ idx ].y );
        vertexArray.push( mesh.vertices[ idx ].z );
        vertexArray.push( mesh.otherParams[ idx ][ 0 ] );
        vertexArray.push( mesh.otherParams[ idx ][ 1 ] );
        vertexArray.push( mesh.otherParams[ idx ][ 2 ] );
    }

    override private function transformMesh( mesh : Mesh ) : Mesh {

        var transformedMesh = new Mesh();

        for( el in mesh ) {

            addVertex( mesh, transformedMesh, el.i0, 0 );
            if( el.elementCount > 1 ) {
                addVertex( mesh, transformedMesh, el.i1, 1 );
            }
            if( el.elementCount > 2 ) {
                addVertex( mesh, transformedMesh, el.i2, 2 );
            }
            if( el.elementCount > 3 ) {
                addVertex( mesh, transformedMesh, el.i0, 0 );
                addVertex( mesh, transformedMesh, el.i2, 1 );
                addVertex( mesh, transformedMesh, el.i3, 2 );
            }
        }

        return transformedMesh;
    }

    var bc0 : Vec3 = [ 1, 0, 0 ];
    var bc1 : Vec3 = [ 0, 1, 0 ];
    var bc2 : Vec3 = [ 0, 0, 1 ];

    private function addVertex( mesh : Mesh, transformedMesh : Mesh, idx : Int, elementPoint : Int ) {

        var bc : Vec3 = null;
        switch ( elementPoint ) {
            case 0: bc = bc0;
            case 1: bc = bc1;
            case 2: bc = bc2;
            default:
        }

        transformedMesh.vertices.push( mesh.vertices[ idx ] );
        transformedMesh.otherParams.push( cast bc.toArray() );
    }
}
