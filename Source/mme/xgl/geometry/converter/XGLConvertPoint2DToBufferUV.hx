package mme.xgl.geometry.converter;

import mme.math.glmatrix.Vec3;

import mme.geometry.Plane;
import mme.core.Mesh;


class XGLConvertPoint2DToBufferUV extends XGLMeshConverter {

    public function new( mesh : Mesh ) {
        super( mesh );
    }

    override private function pushData( mesh : Mesh, vertexArray : Array<Float>, idx : Int ) {

        vertexArray.push( mesh.vertices[ idx ].x );
        vertexArray.push( mesh.vertices[ idx ].y );
        vertexArray.push( mesh.vertices[ idx ].z );

        vertexArray.push( mesh.uvs[ idx ][0] );
        vertexArray.push( mesh.uvs[ idx ][1] );
    }

    override private function transformMesh( mesh : Mesh ) : Mesh {

        var points = mesh.vertices;
        var params = mesh.otherParams;

        var p = points[ 0 ];
        var par : Array<Float> = cast params[ 0 ];
        // a hack to make sure that we end up with indexed mesh
        // Plane.makeXY creates indexed mesh and
        // Mesh.merge will assume a mesh is non-indexed if it is
        // initially empty
        var transformedMesh = Plane.makeXY( par[ 0 ], par[ 1 ] );
        transformedMesh.moveTo( p );

        for( idx in 1...points.length ) {

            p = points[ idx ];
            par = cast params[ idx ];

            makeAPoint( transformedMesh, p, par[ 0 ], par[ 1 ] );
        }

        return transformedMesh;
    }

    private function makeAPoint( transformedMesh : Mesh, p : Vec3, width : Float, height : Float ) {

        var pointMesh = Plane.makeXY( width, height );
        pointMesh.moveTo( p );
        transformedMesh.merge( pointMesh );
    }
}
