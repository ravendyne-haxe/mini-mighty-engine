package mme;

import lime.utils.Assets;
import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

import mme.xgl.XGLRegistry;

import mme.font.FontAtlas;


class MMEngine {

    static var defaultFontAtlas ( default, null ) : FontAtlas;
    static var currentId : Int = 0;

    public static var currentFont ( get, null ) : FontAtlas;

    public static function initialize() {
        loadDefaultFont();
    }
    public static function addRegistryExtension( gl : WebGLRenderContext, registryExtension : XGLRegistryExtension ) {
        if( XGLRegistry.get( gl ) == null ) XGLRegistry.initialize( gl );
        XGLRegistry.get( gl ).registerExtension( registryExtension );
    }
    public static function nextId() : Int {
        currentId++; return currentId;
    }

    static function loadDefaultFont() {
        var fontAtlasBinary = Assets.getBytes('defaultFontAtlas');
        defaultFontAtlas = FontAtlas.fromBytes( fontAtlasBinary );

        //
        // load from TTF asset (only on desktops)
        //
        // var font = Assets.getFont('defaultFont');
        // defaultFontAtlas = FontAtlas.forFont( font, " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,/;?:\"!@#$%^&*()-=_+" );

        //
        // load TTF asset, convert to binary atlas data and save to file
        // the file can then be used to load font atlas as we do above for default.fontatlas
        //
        // var font = Assets.getFont("defaultFont");
        // var fontAtlas = FontAtlas.forFont( font, " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,/;?:\"!@#$%^&*()-=_+" );
        // var fontAtlasBytes = fontAtlas.toBytes();
        // sys.io.File.write("defaultFont.fontatlas").writeFullBytes( fontAtlasBytes, 0, fontAtlasBytes.length );
    }

    static function get_currentFont() : FontAtlas { return defaultFontAtlas; }
}
