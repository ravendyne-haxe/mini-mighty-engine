package mme.camera;

import mme.core.Camera;

import mme.camera.CameraUtils;


class OrthoCamera extends Camera {

    public var left ( get, set ) : Float;
    public var right ( get, set ) : Float;
    public var bottom ( get, set ) : Float;
    public var top ( get, set ) : Float;
    public var near ( get, set ) : Float;
    public var far ( get, set ) : Float;

    // have to have vars like this, otherwise constructor calls
    // setters and everythig goes haywire
    private var _left : Float;
    private var _right : Float;
    private var _bottom : Float;
    private var _top : Float;
    private var _near : Float;
    private var _far : Float;

    public function new( left : Float, right : Float, bottom : Float, top : Float, depth : Float ) {

        if( depth <= 0.0 )
            throw "MME: ------------> OrthoCamera 'depth' parameter has to be > 0";

        super();

        _left = left;
        _right = right;
        _bottom = bottom;
        _top = top;
        // zNear has to be at the camera eye for lighting to work correctly
        // also, zFar has to be placed in front of the camera for lighting to work correctly
        // which means it has to be placed in the opposite direction of the direction of Z-axis
        _near = 0;
        _far = - depth;

        buildProjectionMatrix();
    }

    private function buildProjectionMatrix() {
        cameraProjectionMatrix = CameraUtils.ortho( _left, _right, _bottom, _top, _near, _far );
    }

    private function get_left() : Float {
        return _left;
    }
    private function get_right() : Float {
        return _right;
    }
    private function get_bottom() : Float {
        return _bottom;
    }
    private function get_top() : Float {
        return _top;
    }
    private function get_near() : Float {
        return _near;
    }
    private function get_far() : Float {
        return _far;
    }

    private function set_left( left : Float ) : Float {
        _left = left;
        buildProjectionMatrix();
        return _left;
    }
    private function set_right( right : Float ) : Float {
        _right = right;
        buildProjectionMatrix();
        return _right;
    }
    private function set_bottom( bottom : Float ) : Float {
        _bottom = bottom;
        buildProjectionMatrix();
        return _bottom;
    }
    private function set_top( top : Float ) : Float {
        _top = top;
        buildProjectionMatrix();
        return _top;
    }
    private function set_near( near : Float ) : Float {
        _near = near;
        buildProjectionMatrix();
        return _near;
    }
    private function set_far( far : Float ) : Float {
        _far = far;
        buildProjectionMatrix();
        return _far;
    }

    override private function get_aspect() : Float {
        return ( right - left ) / ( top - bottom );
    }
}
