package mme.core;

import mme.math.glmatrix.Vec3;
import lime.graphics.Image;

import mme.core.RGBAColor;


class Material {

    public var textureImage : Image;

    public var emissiveColor : RGBAColor;

    /** Material ambient light reflectance **/
    public var ka : Vec3;
    /** Material diffuse light reflectance **/
    public var kd : Vec3;
    /** Material specular light reflectance **/
    public var ks : Vec3;

    public var shininess : Float;

    public function new() {

        textureImage = null;

        emissiveColor = 0xff00ffff; // NamedColors.fuchsia

        ka = [ 0.0, 0.0, 0.0 ];
        kd = [ 0.0, 0.0, 0.0 ];
        ks = [ 0.0, 0.0, 0.0 ];

        shininess = 32.0;
    }

    public function toString() : String {
        return 'Mat( e: $emissiveColor, ka: $ka, kd: $kd, ks: $ks, s: $shininess )';
    }
}
