package mme.core;

import mme.core.SceneNode;
import mme.core.RGBAColor;


class Light extends SceneNode {

    public var color : RGBAColor;
    public var intensity : Float;

    public function new( color : RGBAColor, intensity : Float = 1.0 ) {
        super();

        this.color = color;
        this.intensity = intensity;
    }
}
