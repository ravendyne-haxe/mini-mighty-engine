package mme.core;

import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;


class BoundingBox {

    public var minPosition ( default, null ) : Vec3;
    public var maxPosition ( default, null ) : Vec3;
    public var sizeX ( get, null ) : Float;
    public var sizeY ( get, null ) : Float;
    public var sizeZ ( get, null ) : Float;

    public function new( ?minPosition : Vec3, ?maxPosition : Vec3 ) {

        this.minPosition = minPosition == null ? Vec3.fromValues( Math.POSITIVE_INFINITY, Math.POSITIVE_INFINITY, Math.POSITIVE_INFINITY ) : minPosition;
        this.maxPosition = maxPosition == null ? Vec3.fromValues( Math.NEGATIVE_INFINITY, Math.NEGATIVE_INFINITY, Math.NEGATIVE_INFINITY ) : maxPosition;
    }

    public function extendToVertex( vertex : Vec3 ) {

        Vec3Tools.min( minPosition, vertex, minPosition );
        Vec3Tools.max( maxPosition, vertex, maxPosition );
    }

    public function extendToBoundingBox( other : BoundingBox ) {

        extendToVertex( other.minPosition );
        extendToVertex( other.maxPosition );
    }

    public static function getFor( sceneNode : SceneNode ) : BoundingBox {

        var bb = new BoundingBox();

        if( Std.is( sceneNode, SceneObject ) ) {
            var obj : SceneObject = cast sceneNode;
            bb.extendToBoundingBox( getForMesh( obj.mesh ) );
        }

        for( child in sceneNode.children ) {
            bb.extendToBoundingBox( getFor( child ) );
        }

        return bb;
    }

    public static function getForMesh( mesh : Mesh ) : BoundingBox {

        var min : Vec3 = [Math.POSITIVE_INFINITY, Math.POSITIVE_INFINITY, Math.POSITIVE_INFINITY];
        var max : Vec3 = [Math.NEGATIVE_INFINITY, Math.NEGATIVE_INFINITY, Math.NEGATIVE_INFINITY];

        for( vertex in mesh.vertices ) {

            Vec3Tools.min( min, vertex, min );
            Vec3Tools.max( max, vertex, max );
        }

        return new BoundingBox( min, max );
    }

    function get_sizeX() : Float {
        return maxPosition.x - minPosition.x;
    }

    function get_sizeY() : Float {
        return maxPosition.y - minPosition.y;
    }

    function get_sizeZ() : Float {
        return maxPosition.z - minPosition.z;
    }

    public function toString() : String {

        return '[${minPosition} - ${maxPosition}]';
    }
}