package mme.core;

import mme.math.glmatrix.Vec3;
import mme.math.glmatrix.Mat4;
using mme.math.glmatrix.Mat4Tools;
using mme.math.glmatrix.Mat3Tools;
using mme.math.glmatrix.Vec3Tools;

import mme.core.SceneNode;


class Camera extends SceneNode {

    private var cameraProjectionMatrix : Mat4;

    public var cameraTransform ( get, null ) : Mat4;
    public var projectionTransform ( get, null ) : Mat4;
    public var viewTransform ( get, null ) : Mat4;

    public var cameraUp ( get, null ): Vec3;
    public var cameraLook ( get, null ): Vec3;
    /**
        For some shaders (i.e. Phong shader) we need to be able to tell if
        camera is perspective or ortho. Reason is that the vector that
        points from a surface to the viever (camera *eye*) position, the `view vector`,
        is a *constant value* for any surface, for orthographic projection.
        For perspective projection, `view vector` changes for each surface
        depending on its position relative to camera *eye*.

        For orthographic projection, camera *eye* lies in infinity. That's why
        `view vector` is constant. For perspective projection, camera *eye*
        lies at the point where we positioned the camera, which makes
        `view vector` variable value dependent on relative position between
        camera and a surface being rendered.

        To correctly calculate `view vector` in a shader when needed, we
        use `isPerspective` property.
     **/
    public var isPerspective ( default, null ) : Bool;

    public var aspect ( get, set ): Float;

    public function new() {

        super();

        cameraProjectionMatrix = new Mat4();

        isPerspective = false;
    }

    private function get_projectionTransform() : Mat4 {

        return cameraProjectionMatrix;
    }

    private function get_viewTransform() : Mat4 {

        // Do costly operation only if really needed.
        if( parent != null ) {

            // Camera node is attached to something in the scene.
            // We have to account for transforms of all the parent nodes.

            // The costly operation
            var viewMatrix = transform.invert();

            return viewMatrix;
        }

        var viewMatrix = nodeRotation.transpose().mul( Mat4.fromTranslation( nodePosition.negate() ) );

        return viewMatrix;
    }

    private function get_cameraTransform() : Mat4 {

        var cameraTransformMatrix = projectionTransform.mul( viewTransform );

        return cameraTransformMatrix;
    }

    private function get_cameraUp() : Vec3 {

        var cameraUp = Vec3.Y_AXIS;

        return normalTransform.multiplyVec3( cameraUp, cameraUp );
    }

    private function get_cameraLook() : Vec3 {

        var cameraLook = Vec3.Z_AXIS;
        cameraLook.scale( -1, cameraLook );

        return normalTransform.multiplyVec3( cameraLook, cameraLook );
    }

    private function get_aspect() : Float {
        return 1.0;
    }

    private function set_aspect( value : Float ) : Float {
        return 1.0;
    }
}
