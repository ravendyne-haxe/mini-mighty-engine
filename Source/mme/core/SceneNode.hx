package mme.core;

import mme.math.glmatrix.GLMatrix;
import mme.math.glmatrix.Mat3;
import mme.math.glmatrix.Mat4;
using mme.math.glmatrix.Mat4Tools;
import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;


typedef AnimatedNode = {
    function animationProgress( deltaTime : Int ) : Void;
}

class SceneNode {

    // We don't want to accumulate translations, rotations and scaling to one matrix.
    // Instead, we want to have separate position, rotation and scaleing so we can
    // apply scaling first, rotation next and translation last to this node at the time
    // modelTransform is requested.
    private var nodePosition : Vec3;
    private var nodeRotation : Mat4;
    private var nodeScaling : Vec3;

    public var positionX ( get, set ) : Float;
    public var positionY ( get, set ) : Float;
    public var positionZ ( get, set ) : Float;
    public var scenePosition ( get, null ) : Vec3;

    public var position ( get, set ) : Vec3;
    public var rotationMatrix ( get, set ) : Mat3;
    public var scaling ( get, null ) : Vec3;

    /** Value of this matrix is used to set a shader uniform, a.k.a. this node's world transform **/
    public var transform ( get, null ) : Mat4;
    /** matrix created from model position and model rotation **/
    public var modelTransform ( get, null ) : Mat4;
    /** 3x3 matrix created from world rotation matrix for use in shaders **/
    public var normalTransform ( get, null ) : Mat3;
    /** Accumulates all parent transforms when they happen so we don't have to recalculate it on every render pass **/
    private var parentTransform : Mat4;

    public var parent : SceneNode;
    public var children : Array< SceneNode >;
    public var scene : Scene;
    public var visible ( default, set ) : Bool;

    public function new() {

        nodePosition = new Vec3();
        nodeRotation = new Mat4();
        nodeScaling = [ 1.0, 1.0, 1.0 ];

        parentTransform = new Mat4();

        parent = null;
        children = [];
        scene = null;
        visible = true;
    }

    public function toString() : String {
        var str = Type.getClassName( Type.getClass( this ) );
        return 'SceneNode[ $str ]';
    }

    //==============================================================================
    //
    // Properties
    //
    //==============================================================================

    private function set_visible( value : Bool ) : Bool {

        if( visible != value ) {

            visible = value;

            for( child in children ) {
                child.visible = value;
            }
        }

        return value;
    }

    //==============================================================================
    //
    // Transforms
    //
    //==============================================================================

    private function get_modelTransform() : Mat4 {

        var matrix = nodeRotation.clone();
        // nodeRotation is pure rotation matrix
        // and we're applying scaling first then rotation then translation,
        // so we can just add scaling and translation like this

        // scaling
        // x
		matrix[0] = matrix[0] * nodeScaling.x;
		matrix[1] = matrix[1] * nodeScaling.x;
		matrix[2] = matrix[2] * nodeScaling.x;
        // y
		matrix[4] = matrix[4] * nodeScaling.y;
		matrix[5] = matrix[5] * nodeScaling.y;
		matrix[6] = matrix[6] * nodeScaling.y;
        // z
		matrix[8] = matrix[8] * nodeScaling.z;
		matrix[9] = matrix[9] * nodeScaling.z;
		matrix[10] = matrix[10] * nodeScaling.z;

        // translation
		matrix[12] = matrix[12] + nodePosition.x;
		matrix[13] = matrix[13] + nodePosition.y;
		matrix[14] = matrix[14] + nodePosition.z;

        return matrix;
    }

    private function getRotationMatrix( mat4 : Mat4 ) : Mat3 {

        var mat3 = Mat3.fromMat4( mat4 );

        return mat3;
    }

    private function get_normalTransform() : Mat3 {

        return getRotationMatrix( transform );
    }

    private function get_transform() : Mat4 {

        if( parent != null ) {
            return parentTransform.mul( modelTransform );
        }

        return modelTransform;
    }

    private function get_rotationMatrix() : Mat3 {

        return getRotationMatrix( nodeRotation );
    }

    private function set_rotationMatrix( rotations : Mat3 ) : Mat3 {

        nodeRotation = Mat4.fromMat3( rotations );
        updateChildren();

        return rotations;
    }

    private function get_scenePosition() : Vec3 {

        if( parent != null ) {
            return parentTransform.multiplyVec3( nodePosition );
        }

        return nodePosition.clone();
    }

    private function get_position() : Vec3 {

        return nodePosition.clone();
    }

    private function set_position( position : Vec3 ) : Vec3 {

        nodePosition.x = position.x;
        nodePosition.y = position.y;
        nodePosition.z = position.z;
        updateChildren();

        return position;
    }

    private function get_positionX() : Float {

        return nodePosition.x;
    }

    private function set_positionX( x : Float ) : Float {

        nodePosition.x = x;
        updateChildren();

        return x;
    }

    private function get_positionY() : Float {

        return nodePosition.y;
    }

    private function set_positionY( y : Float ) : Float {

        nodePosition.y = y;
        updateChildren();

        return y;
    }

    private function get_positionZ() : Float {

        return nodePosition.z;
    }

    private function set_positionZ( z : Float ) : Float {

        nodePosition.z = z;
        updateChildren();

        return z;
    }

    private function get_scaling() : Vec3 {

        return nodeScaling.clone();
    }

    public function translate( deltaVector : Vec3 ) : SceneNode {

        nodePosition.add( deltaVector, nodePosition );
        updateChildren();

        return this;
    }

    public function translateX( deltaX : Float ) : SceneNode {

        nodePosition.x += deltaX;
        updateChildren();

        return this;
    }

    public function translateY( deltaY : Float ) : SceneNode {

        nodePosition.y += deltaY;
        updateChildren();

        return this;
    }

    public function translateZ( deltaZ : Float ) : SceneNode {

        nodePosition.z += deltaZ;
        updateChildren();

        return this;
    }

    public function rotate( deltaAngleDegrees : Float, ?axis : Vec3 ) : SceneNode {

        if( axis == null ) {
            axis = Vec3.Z_AXIS;
        }

        var m = Mat4.fromRotation( GLMatrix.toRadian( deltaAngleDegrees ), axis );
        m.mul( nodeRotation, nodeRotation );

        updateChildren();

        return this;
    }

    public function scale( scaleX : Float, scaleY : Float, scaleZ : Float ) : SceneNode {

        nodeScaling.mul( [scaleX, scaleY, scaleZ], nodeScaling );
        updateChildren();

        return this;
    }

    //==============================================================================
    //
    // Parent-child hierarchy
    //
    //==============================================================================

    public function add( child : SceneNode ) : SceneNode {

        children.push( child );
        child.parent = this;

        child.updateFromParent();

        if( scene != null ) {
            scene.setOwnership( child );
        }

        return this;
    }

    private function updateFromParent() : Void {

        if( parent == null ) return;

        // get latest from parent
        parentTransform = parent.transform;

        // tell children grandma has changed
        updateChildren();
    }

    private function updateChildren() : Void {

        // tell children we have changed
        for( child in children ) {
            child.updateFromParent();
        }
    }
}
