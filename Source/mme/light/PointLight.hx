package mme.light;

import mme.core.Light;
import mme.core.RGBAColor;


class PointLight extends Light {

    public var attenuation : Float;

    public function new( ?color : RGBAColor, intensity : Float = 1.0 ) {

        if( color == null ) color = 0xffffffff;

        super( color, intensity );

        attenuation = 0.0;
    }
}
