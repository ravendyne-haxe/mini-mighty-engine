package mme.light;

import mme.core.Light;
import mme.core.RGBAColor;


class AmbientLight extends Light {

    public function new( ?color : RGBAColor, intensity : Float = 0.1 ) {

        if( color == null ) color = 0xffffffff;

        super( color, intensity );
    }
}