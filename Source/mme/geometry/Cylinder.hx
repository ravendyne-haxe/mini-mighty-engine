package mme.geometry;

import mme.math.Parametric;
import mme.math.glmatrix.Vec3;

import mme.core.SceneObject;
import mme.core.Material;

class Cylinder extends SceneObject {

    public var radius : Float;
    public var height : Float;

    public var segmentsX : Int;
    public var isOpen ( default, null ) : Bool;

    public function new( radius : Float, height : Float, segments : Int = 3, open : Bool = false, ?material : Material ) {

        super( material );

        this.radius = radius;
        this.height = height;
        isOpen = open;

        segmentsX = segments < 3 ? 3 : segments;

        updateMesh();
    }

    public function updateMesh() : Void {

        // create cylindrical section
        Parametric.csurface( 1, segmentsX, function( theta, zc ) {

            var p = CylindricalGenerators.cylinder( theta, zc );
            return [ radius * p.x, radius * p.y, height * p.z ];
        }, mesh );

        if( ! isOpen ) {

            // create circular bottom
            var bmesh = Parametric.csurface( 1, segmentsX, function( theta, zc ) {

                var p = CylindricalGenerators.circle( theta, zc );
                return [ radius * p.x, radius * p.y, p.z ];
            } );
            // no need to flip this one
            // translate to coincide with bottom
            bmesh.moveTo([ 0, 0, - height / 2 ]);


            // create circular top
            var tmesh = Parametric.csurface( 1, segmentsX, function( theta, zc ) {

                var p = CylindricalGenerators.circle( theta, zc );
                return [ radius * p.x, radius * p.y, p.z ];
            } );
            // flip it around so that normals are pointing in Z+ direction
            // rotate about X-axis since there has to be a vertex that coincides with X-axis
            tmesh.rotateTo( 180, Vec3.X_AXIS );
            // translate to coincide with top
            tmesh.moveTo([ 0, 0, height / 2 ]);

            // merge the three
            mesh.merge( tmesh );
            mesh.merge( bmesh );
        }
    }
}
