package mme.geometry;

import mme.math.Parametric;
import mme.math.glmatrix.Vec3;

import mme.core.SceneObject;
import mme.core.Material;

class Circle extends SceneObject {

    public var radius : Float;

    public var segmentsX : Int;

    public function new( radius : Float, segments : Int = 3, ?material : Material ) {

        super( material );

        this.radius = radius;

        segmentsX = segments < 3 ? 3 : segments;

        updateMesh();
    }

    public function updateMesh() : Void {

        Parametric.csurface( 1, segmentsX, function( theta, zc ) {

            var p = CylindricalGenerators.circle( theta, zc );
            return [ radius * p.x, radius * p.y, p.z ];
        }, mesh );

        // flip it around so that normals are pointing in Z+ direction
        // rotate about X-axis since there has to be a vertex that coincides with X-axis
        mesh.rotateTo( 180, Vec3.X_AXIS );
    }
}
