package mme.geometry;

import mme.core.Material;

import mme.geometry.Rectangle;


class Square extends Rectangle {

    public var side : Float;

    public function new( side : Float = 1.0, ?material : Material ) {

        super( side, side, material );
        this.side = side;
    }
}
