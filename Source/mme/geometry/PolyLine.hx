package mme.geometry;

import mme.math.glmatrix.Vec3;

import mme.core.SceneObject;
import mme.core.RGBAColor;
import mme.material.LineMaterial;


class PolyLine extends SceneObject {

    public var isClosed : Bool;

    public var lineMaterial ( default, null ) : LineMaterial;

    public function new( ?lineColor : RGBAColor, thickness : Float = 2.0, ?edgeColor : RGBAColor, ?lineMaterial : LineMaterial ) {

        if( lineColor == null ) lineColor = 0x000000ff;

        if( lineMaterial == null )
            lineMaterial = new LineMaterial( lineColor, thickness, edgeColor );

        super( lineMaterial );

        this.lineMaterial = lineMaterial;

        // as far as Mesh is concerned, our polyline
        // is just a collection of points.
        // Mesh.Line element type assumes two vertices per
        // line, which is not the case here, so we use Point
        mesh.elementType = Point;
        isClosed = false;
    }

    public function addPoint( point : Vec3 ) {

        mesh.vertices.push( point );

        needsUpdate();

        return this;
    }

    public function addPoints( points : Array<Vec3> ) {

        for( p in points )
            mesh.vertices.push( p );

        needsUpdate();

        return this;
    }

    public function close() {

        isClosed = true;
        if( mesh.vertices.length == 2 ) isClosed = false;
    }
}
