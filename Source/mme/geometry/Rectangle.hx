package mme.geometry;

import mme.core.Material;


class Rectangle extends Plane {

    public function new( width : Float = 1.0, height : Float = 1.0, ?material : Material ) {

        super( width, height, 1, 1, material );
    }
}
