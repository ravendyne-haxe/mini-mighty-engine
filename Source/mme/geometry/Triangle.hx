package mme.geometry;

import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;

import mme.core.SceneObject;
import mme.core.Material;


class Triangle extends SceneObject {

    public var p0 : Vec3;
    public var p1 : Vec3;
    public var p2 : Vec3;

    public function new( p0 : Vec3, p1 : Vec3, p2 : Vec3, ?material : Material ) {

        super( material );

        this.p0 = p0;
        this.p1 = p1;
        this.p2 = p2;

        updateMesh();
    }

    public function updateMesh() : Void {

        mesh.vertices = [];
        mesh.uvs = [];

        mesh.vertices.push( [ p0.x, p0.y, p0.z ] );
        mesh.uvs.push( [ 0., 1. ] );

        mesh.vertices.push( [ p1.x, p1.y, p1.z ] );
        mesh.uvs.push( [ 0., 0. ] );

        mesh.vertices.push( [ p2.x, p2.y, p2.z ] );
        mesh.uvs.push( [ 1., 1. ] );

        var a = p1.subtract( p0 );
        var b = p2.subtract( p1 );
        var cross = a.cross( b );
        cross.normalize( cross );

        mesh.normals.push( [ cross.x, cross.y, cross.z ] );
        mesh.normals.push( [ cross.x, cross.y, cross.z ] );
        mesh.normals.push( [ cross.x, cross.y, cross.z ] );
    }
}
