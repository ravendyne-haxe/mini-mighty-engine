package mme.core;

import lime.math.RGBA;

import mme.core.SceneNode;


class Light extends SceneNode {

    public var color : RGBA;
    public var intensity : Float;

    public function new( color : RGBA, intensity : Float = 1.0 ) {
        super();

        this.color = color;
        this.intensity = intensity;
    }
}
