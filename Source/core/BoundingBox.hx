package mme.core;

import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;


class BoundingBox {

    public var minPosition ( default, null ) : Vec3;
    public var maxPosition ( default, null ) : Vec3;

    public function new( ?minPosition : Vec3, ?maxPosition : Vec3 ) {

        this.minPosition = minPosition == null ? new Vec3() : minPosition;
        this.maxPosition = maxPosition == null ? new Vec3() : maxPosition;
    }

    public function extendToVertex( vertex : Vec3 ) {

        Vec3Tools.min( minPosition, vertex, minPosition );
        Vec3Tools.max( maxPosition, vertex, maxPosition );
    }

    public function extendToBoundingBox( other : BoundingBox ) {

        extendToVertex( other.minPosition );
        extendToVertex( other.maxPosition );
    }

    public static function getFor( sceneNode : SceneNode ) : BoundingBox {

        var bb = new BoundingBox();

        if( Std.is( sceneNode, SceneObject ) ) {
            var obj : SceneObject = cast sceneNode;
            bb.extendToBoundingBox( getForMesh( obj.mesh ) );
        }

        for( child in sceneNode.children ) {
            bb.extendToBoundingBox( getFor( child ) );
        }

        return bb;
    }

    public static function getForMesh( mesh : Mesh ) : BoundingBox {

        var min : Vec3 = [0, 0, 0];
        var max : Vec3 = [0, 0, 0];

        for( vertex in mesh.vertices ) {

            Vec3Tools.min( min, vertex, min );
            Vec3Tools.max( max, vertex, max );
        }

        return new BoundingBox( min, max );
    }

    public function toString() : String {

        return '[${minPosition} - ${maxPosition}]';
    }
}