package mme.core;

import lime.graphics.WebGLRenderContext;

import mme.core.Scene;

import mme.xgl.engine.XGLRenderer;

class Renderer {

    private var renderer : XGLRenderer;

    public function new( gl : WebGLRenderContext ) {

        renderer = new XGLRenderer( gl );
    }

    public function renderSetup( gl : WebGLRenderContext ) : Void {
        renderer.renderSetup( gl );
    }

    public function renderScene( gl : WebGLRenderContext, scene : Scene ) : Void {
        renderer.renderScene( gl, scene );
    }

    public function render( gl : WebGLRenderContext, scene : Scene ) : Void {
        renderer.render( gl, scene );
    }

    public function viewport( gl : WebGLRenderContext, width : Int, height : Int ) {
        gl.viewport( 0, 0, width, height );
    }
}
