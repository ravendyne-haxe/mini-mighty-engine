package mme.core;

import mme.math.glmatrix.GLMatrix;
import mme.math.glmatrix.Vec2;
using mme.math.glmatrix.Vec2Tools;
import mme.math.glmatrix.Mat4;
using mme.math.glmatrix.Mat4Tools;
import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;
import mme.math.glmatrix.Vec4;
using mme.math.glmatrix.Vec4Tools;

/**
    - Use `MeshStatic` if the vertex data will be uploaded once and rendered many times.
    - Use `MeshDynamic` if the vertex data will be created once, changed from time to time, but drawn many times more than that.
    - Use `MeshStream` if the vertex data will be uploaded once and drawn once, it will change with every frame.
*/
enum MeshUsage {

    MeshStatic;
    MeshDynamic;
    MeshStream;
}

enum MeshElementType {
    Quad;
    Triangle;
    Line;
    Point;
}

class Mesh {

    // [ [x,y,z], ... ]
    public var vertices : Array< Vec3 >;
    // [ [u,v], ... ]
    public var uvs : Array< Vec2 >;
    // [ [nx,ny,nz], ... ]
    public var normals : Array< Vec3 >;
    // [ idx0, idx1, ... ]
    public var indices : Array< Int >;
    // [ [ ... ], ... ]
    public var otherParams : Array< Array< Any > >;

    public var isIndexed ( get, never ) : Bool;
    public var usage ( default, null ) : MeshUsage;
    public var elementType : MeshElementType;

    public function new( usage : MeshUsage = MeshStatic ) {

        this.usage = usage;
        elementType = Triangle;

        vertices = [];
        uvs = [];
        normals = [];
        indices = [];
        otherParams = [];
    }

    private function get_isIndexed() {
        return indices.length > 0;
    }

    //==============================================================================
    //
    // Transforms for mesh values
    //
    //==============================================================================

    public function moveTo( position : Vec3 ) : Mesh {

        for( vertex in vertices ) {

            vertex[ 0 ] += position.x;
            vertex[ 1 ] += position.y;
            vertex[ 2 ] += position.z;
        }

        return this;
    }

    public function rotateTo( angleDegrees : Float, ?axis : Vec3 ) : Mesh {

        if( axis == null ) {
            axis = Vec3.Z_AXIS;
        }

        var rotationMatrix = Mat4.fromRotation( GLMatrix.toRadian( angleDegrees ), axis );

        for( vertex in vertices ) {

            rotationMatrix.multiplyVec3( vertex, vertex );
        }

        for( normal in normals ) {

            rotationMatrix.multiplyVec3( normal, normal );
        }

        return this;
    }

    public function scaleTo( xScale : Float, yScale : Float, zScale : Float ) : Mesh {

        var scaleMatrix = new Mat4();
        scaleMatrix.scale( [ xScale, yScale, zScale ], scaleMatrix );

        for( vertex in vertices ) {

            scaleMatrix.multiplyVec3( vertex, vertex );
        }

        return this;
    }

    public function flipNormals() {

        for( normal in normals ) {

            normal[ 0 ] = - normal[ 0 ];
            normal[ 1 ] = - normal[ 1 ];
            normal[ 2 ] = - normal[ 2 ];
        }
    }

    public function toNonIndexed() {
        if( ! isIndexed ) return;

        var niVertices : Array<Vec3> = [];
        var niUvs : Array<Vec2> = [];
        var niNormals : Array<Vec3> = [];

        var idx = 0;
        while( idx < indices.length ) {

            var i0 = indices[idx+0];
            var i1 = indices[idx+1];
            var i2 = indices[idx+2];

            niVertices.push( vertices[ i0 ].clone() );
            niVertices.push( vertices[ i1 ].clone() );
            niVertices.push( vertices[ i2 ].clone() );

            niUvs.push( uvs[ i0 ].clone() );
            niUvs.push( uvs[ i1 ].clone() );
            niUvs.push( uvs[ i2 ].clone() );

            niNormals.push( normals[ i0 ].clone() );
            niNormals.push( normals[ i1 ].clone() );
            niNormals.push( normals[ i2 ].clone() );

            idx += 3;
        }

        vertices = niVertices;
        normals = niNormals;
        uvs = niUvs;
        indices = [];
    }

    public function copyFrom( otherMesh : Mesh ) {

        this.indices = otherMesh.indices.copy();

        for( idx in 0...otherMesh.vertices.length ) {

            this.vertices.push( otherMesh.vertices[ idx ].clone() );
            this.uvs.push( otherMesh.uvs[ idx ].clone() );
            this.normals.push( otherMesh.normals[ idx ].clone() );
        }
    }

    public function merge( otherMesh : Mesh ) {

        if( this.isIndexed  ) {

            if( ! otherMesh.isIndexed ) {
                otherMesh.toIndexed();
            }

            var idxOffset = this.vertices.length;

            for( idx in 0...otherMesh.indices.length ) {

                var index = otherMesh.indices[idx];

                this.indices.push( idxOffset + index );
            }

            for( idx in 0...otherMesh.vertices.length ) {

                this.vertices.push( otherMesh.vertices[ idx ] );
                this.uvs.push( otherMesh.uvs[ idx ] );
                this.normals.push( otherMesh.normals[ idx ] );
            }

            return;
        }

        if( ! this.isIndexed  ) {

            if( otherMesh.isIndexed ) {
                otherMesh.toNonIndexed();
            }

            for( index in 0...otherMesh.vertices.length ) {

                this.vertices.push( otherMesh.vertices[ index ] );
                this.uvs.push( otherMesh.uvs[ index ] );
                this.normals.push( otherMesh.normals[ index ] );
            }

            return;
        }
    }

    private function toIndexed() {

        indices = [];

        for( idx in 0...vertices.length ) {
            indices.push( idx );
        }
    }

    //==============================================================================
    //
    // Mesh iterator. Iterates over elements: quads, triangles, lines or points
    //
    //==============================================================================

    public function iterator() : Iterator< MeshElement > {
        return new MeshElementIterator( this );
    }
}

//==============================================================================
//
// Mesh iterator support code
//
//==============================================================================

typedef MeshElement = {
    var elementType : MeshElementType;
    var i0 : Int;
    var i1 : Int;
    var i2 : Int;
    var i3 : Int;
}

class MeshElementIterator {

    var currentIndex : Int;

    var mesh : Mesh;
    var elementType : MeshElementType;
    var elementCount : Int;
    var isIndexed : Bool;
    var vertices : Array< Vec3 >;
    var indices : Array< Int >;

    public function new( mesh : Mesh ) {

        this.mesh = mesh;
        currentIndex = 0;

        elementType = mesh.elementType;
        isIndexed = mesh.isIndexed;

        vertices = mesh.vertices;
        indices = mesh.indices;

        switch( elementType ) {
            case Quad: elementCount = 4;
            case Triangle: elementCount = 3;
            case Line: elementCount = 2;
            case Point: elementCount = 1;
        }
    }

    public function hasNext() : Bool {

        if( isIndexed ) {
            return currentIndex + elementCount <= indices.length;
        }

        return currentIndex + elementCount <= vertices.length;
    }

    public function next() : MeshElement {

        if( ! hasNext() ) return null;

        var idx0 = -1;
        var idx1 = -1;
        var idx2 = -1;
        var idx3 = -1;

        if( isIndexed ) {

            idx0 = indices[ currentIndex ];
            if( elementCount > 1 )
                idx1 = indices[ currentIndex + 1 ];
            if( elementCount > 2 )
                idx2 = indices[ currentIndex + 2 ];
            if( elementCount > 3 )
                idx3 = indices[ currentIndex + 3 ];

        } else {

            idx0 = currentIndex;
            if( elementCount > 1 )
                idx1 = currentIndex + 1;
            if( elementCount > 2 )
                idx2 = currentIndex + 2;
            if( elementCount > 3 )
                idx3 = currentIndex + 3;
        }

        currentIndex += elementCount;

        return {
            elementType: elementType,
            i0: idx0,
            i1: idx1,
            i2: idx2,
            i3: idx3,
        };
    }
}