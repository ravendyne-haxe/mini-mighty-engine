package mme.core;

import haxe.ds.Vector;
import mme.math.glmatrix.Mat4;

import mme.core.SceneNode;
import mme.core.SceneNode.AnimatedNode;
import mme.core.SceneObject;
import mme.core.Camera;
import mme.core.Light;

import mme.light.AmbientLight;
import mme.light.PointLight;

using mme.util.Utils;


class Scene {

    //
    // Renderer stuff
    //
    public var rendererTag : Any;
    public var objectScheduledForUpdate : Array< SceneObject >;

    //
    // Nodes
    //
    private var root : SceneNode;
    public var children ( get, null ) : Iterator< SceneNode >;
    private var animatedNodes : Array< AnimatedNode >;

    //
    // Cameras
    //
    public var cameraTransform ( get, null ) : Mat4;
    private var cameraList : Array< Camera >;

    public var activeCamera ( get, set ) : Camera;
    private var activeCameraRef: Camera;

    //
    // Lights
    //
    public static var MAX_NUMBER_OF_LIGHTS  = 8;
    // TODO light can be changed from outside here since we are exposing a *container*
    public var lightList ( default, null ) : Vector< PointLight >;
    public var ambientLight ( default, null ) : AmbientLight;

    private var defaultAmbientLight : AmbientLight;
    private var defaultPointLight : PointLight;


    public function new() {

        root = new SceneNode();
        setOwnership( root );

        cameraList = [];
        activeCameraRef = null;

        defaultAmbientLight = new AmbientLight( 0x000000ff, 0.0 );
        defaultPointLight = new PointLight( 0x000000ff, 0.0 );

        lightList = new Vector( MAX_NUMBER_OF_LIGHTS );
        for( idx in 0...lightList.length ) {
            lightList[ idx ] = defaultPointLight;
        }
        ambientLight = defaultAmbientLight;

        rendererTag = null;
        objectScheduledForUpdate = [];
        animatedNodes = [];
    }

    //==============================================================================
    //
    // Add/update nodes
    //
    //==============================================================================

    public function add( object : SceneNode ) {

        root.add( object );
        setOwnership( object );
    }

    public function addCamera( camera : Camera ) {

        cameraList.push( camera );

        activeCameraRef = camera;
    }

    public function addLight( light : Light ) {

        if( Std.is( light, AmbientLight ) ) {

            ambientLight = cast light;

        } else if( Std.is( light, PointLight ) ) {

            var pointLight : PointLight = cast light;

            var idx = 0;
            while( lightList[ idx ] != defaultPointLight ) idx++;

            if( idx < lightList.length )
                lightList[ idx ] = pointLight;
        }
    }

    public function setOwnership( node : SceneNode ) {

        node.scene = this;

        for( child in node.children ) {
            child.scene = this;
        }

        scheduleAllForUpdate( node );
    }

    public function scheduleForUpdate( object : SceneObject ) {

        if( objectScheduledForUpdate.indexOf( object ) == -1 ) {
            objectScheduledForUpdate.push( object );
        }
    }

    private function scheduleAllForUpdate( node : SceneNode ) {

        node.applyToSceneObjects( function( obj ) scheduleForUpdate( obj ) );
    }

    //==============================================================================
    //
    // Animating nodes
    //
    //==============================================================================
    public function addAnimated( node : AnimatedNode ) {

        animatedNodes.push( node );
    }

    public function removeAnimated( node : AnimatedNode ) {

        animatedNodes.remove( node );
    }

    public function progressAnimations( deltaTime : Int ) {

        for( node in animatedNodes ) {

            node.animationProgress( deltaTime );
        }
    }

    //==============================================================================
    //
    // Getters/setters
    //
    //==============================================================================

    private function get_cameraTransform() : Mat4 {

        if( activeCamera == null ) {

            return new Mat4();
        }

        return activeCamera.cameraTransform;
    }

    private function get_activeCamera() : Camera {

        return activeCameraRef;
    }

    private function set_activeCamera( camera : Camera ) : Camera {

        if( cameraList.indexOf( camera ) < 0 ) {

            addCamera( camera );
        }
 
        activeCameraRef = camera;

        return activeCameraRef;
    }

    private function get_children() : Iterator< SceneNode > {
        return root.children.iterator();
    }
}
