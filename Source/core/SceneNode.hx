package mme.core;

import mme.math.glmatrix.GLMatrix;
import mme.math.glmatrix.Mat3;
import mme.math.glmatrix.Mat4;
using mme.math.glmatrix.Mat4Tools;
import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;


typedef AnimatedNode = {
    function animationProgress( deltaTime : Int ) : Void;
}

class SceneNode {

    // We don't want to accumulate translations and rotations to one matrix.
    // Instead, we want to have separate position and rotation so we can
    // apply rotation first and translation next to this node at the time
    // modelTransform is requested.
    private var nodePosition : Vec3;
    private var nodeRotation : Mat4;

    public var positionX ( get, set ) : Float;
    public var positionY ( get, set ) : Float;
    public var positionZ ( get, set ) : Float;
    public var scenePosition ( get, null ) : Vec3;

    public var position ( get, set ) : Vec3;
    public var rotationMatrix ( get, set ) : Mat3;

    /** Value of this matrix is used to set a shader uniform, a.k.a. this node's world transform **/
    public var transform ( get, null ) : Mat4;
    /** matrix created from model position and model rotation **/
    public var modelTransform ( get, null ) : Mat4;
    /** 3x3 matrix created from world rotation matrix for use in shaders **/
    public var normalTransform ( get, null ) : Mat3;
    /** Accumulates all parent transforms when they happen so we don't have to recalculate it on every render pass **/
    private var parentTransform : Mat4;

    public var parent : SceneNode;
    public var children : Array< SceneNode >;
    public var scene : Scene;

    public function new() {

        nodePosition = new Vec3();
        nodeRotation = new Mat4();

        parentTransform = new Mat4();

        parent = null;
        children = [];
        scene = null;
    }

    //==============================================================================
    //
    // Transforms
    //
    //==============================================================================

    private function get_modelTransform() : Mat4 {

        // TODO: no need to clone() with glmatrix
        var matrix = nodeRotation.clone();
        // TODO: use Quat for tracking rotations and use fromRotationTranslation() to create transform
        // matrix.appendTranslation( nodePosition.x, nodePosition.y, nodePosition.z );
		matrix[12] = matrix[12] + nodePosition.x;
		matrix[13] = matrix[13] + nodePosition.y;
		matrix[14] = matrix[14] + nodePosition.z;

        return matrix;
    }
/**
	Creates `Float32Array` which represents a 3x3 transformation matrix
    constructed out of top-left part of 4x4 transfromation matrix, or
    in other words, of only rotation components for it.

    Mapping from `Mat4` to this array is done like this

    Mat4 element indexes:
	```
	[ 0, 4,  8, 12 ]
	[ 1, 5,  9, 13 ]
	[ 2, 6, 10, 14 ]
	[ 3, 7, 11, 15 ]
	```

    `Mat4` to 3x3 matrix array element indexes:
	```
	[ 0 -> 0, 4 -> 3,  8 -> 6 ]
	[ 1 -> 1, 5 -> 4,  9 -> 7 ]
	[ 2 -> 2, 6 -> 5, 10 -> 8 ]
	```
**/
    private function getRotationMatrix( mat4 : Mat4 ) : Mat3 {

        var mat3 = Mat3.fromMat4( mat4 );

        return mat3;
    }

    private function get_normalTransform() : Mat3 {

        return getRotationMatrix( transform );
    }

    private function get_transform() : Mat4 {

        if( parent != null ) {
            return parentTransform.mul( modelTransform );
        }

        return modelTransform;
    }

    private function get_rotationMatrix() : Mat3 {

        return getRotationMatrix( nodeRotation );
    }

    private function set_rotationMatrix( rotations : Mat3 ) : Mat3 {

        nodeRotation = Mat4.fromMat3( rotations );
        updateChildren();

        return rotations;
    }

    private function get_scenePosition() : Vec3 {

        if( parent != null ) {
            return parentTransform.multiplyVec3( nodePosition );
        }

        return nodePosition.clone();
    }

    private function get_position() : Vec3 {

        return nodePosition.clone();
    }

    private function set_position( position : Vec3 ) : Vec3 {

        nodePosition.x = position.x;
        nodePosition.y = position.y;
        nodePosition.z = position.z;
        updateChildren();

        return position;
    }

    private function get_positionX() : Float {

        return nodePosition.x;
    }

    private function set_positionX( x : Float ) : Float {

        nodePosition.x = x;
        updateChildren();

        return x;
    }

    private function get_positionY() : Float {

        return nodePosition.y;
    }

    private function set_positionY( y : Float ) : Float {

        nodePosition.y = y;
        updateChildren();

        return y;
    }

    private function get_positionZ() : Float {

        return nodePosition.z;
    }

    private function set_positionZ( z : Float ) : Float {

        nodePosition.z = z;
        updateChildren();

        return z;
    }

    public function translate( deltaVector : Vec3 ) : SceneNode {

        // nodePosition.incrementBy( deltaVector );
        nodePosition.add( deltaVector, nodePosition );
        updateChildren();

        return this;
    }

    public function translateX( deltaX : Float ) : SceneNode {

        nodePosition.x += deltaX;
        updateChildren();

        return this;
    }

    public function translateY( deltaY : Float ) : SceneNode {

        nodePosition.y += deltaY;
        updateChildren();

        return this;
    }

    public function translateZ( deltaZ : Float ) : SceneNode {

        nodePosition.z += deltaZ;
        updateChildren();

        return this;
    }

    public function rotate( deltaAngleDegrees : Float, ?axis : Vec3 ) : SceneNode {

        if( axis == null ) {
            axis = Vec3.Z_AXIS;
        }

        var m = Mat4.fromRotation( GLMatrix.toRadian( deltaAngleDegrees ), axis );
        m.mul( nodeRotation, nodeRotation );

        updateChildren();

        return this;
    }

    //==============================================================================
    //
    // Parent-child hierarchy
    //
    //==============================================================================

    public function add( child : SceneNode ) : SceneNode {

        children.push( child );
        child.parent = this;

        child.updateFromParent();

        if( scene != null ) {
            scene.setOwnership( child );
        }

        return this;
    }

    private function updateFromParent() : Void {

        if( parent == null ) return;

        // get latest from parent
        parentTransform = parent.transform;

        // tell children grandma has changed
        updateChildren();
    }

    private function updateChildren() : Void {

        // tell children we have changed
        for( child in children ) {
            child.updateFromParent();
        }
    }
}
