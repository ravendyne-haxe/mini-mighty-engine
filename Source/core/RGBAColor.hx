package mme.core;

import lime.math.RGBA;

import mme.math.glmatrix.Vec3;
import mme.math.glmatrix.Vec4;
using mme.math.glmatrix.Vec4Tools;

import mme.core.Light;


#if lime
import lime.utils.Float32Array;
abstract RGBAColor(Float32Array) from Float32Array to Float32Array {
#else
import haxe.ds.Vector;
abstract RGBAColor(Vector<Float>) from Vector<Float> to Vector<Float> {
#end

    public inline function new() {
        #if lime
        this = new Float32Array(4);
        #else
        this = new Vector<Float>(4);
        this[0] = 0.0;
        this[1] = 0.0;
        this[2] = 0.0;
        this[3] = 0.0;
        #end
    }
    @:arrayAccess
    public inline function getk(key:Int) {
    return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v;
        return v;
    }

    public var r ( get, set ) : Float;
    public var g ( get, set ) : Float;
    public var b ( get, set ) : Float;
    public var a ( get, set ) : Float;
    private inline function get_r() : Float {
        return this[0];
    }
    private inline function set_r( r : Float) : Float {
        return this[0] = r < 0.0 ? 0.0 : r > 1.0 ? 1.0 : r;
    }
    private inline function get_g() : Float {
        return this[1];
    }
    private inline function set_g( g : Float) : Float {
        return this[1] = g < 0.0 ? 0.0 : g > 1.0 ? 1.0 : g;
    }
    private inline function get_b() : Float {
        return this[2];
    }
    private inline function set_b( b : Float) : Float {
        return this[2] = b < 0.0 ? 0.0 : b > 1.0 ? 1.0 : b;
    }
    private inline function get_a() : Float {
        return this[3];
    }
    private inline function set_a( a : Float) : Float {
        return this[3] = a < 0.0 ? 0.0 : a > 1.0 ? 1.0 : a;
    }

    public var ri ( get, set ) : Int;
    public var gi ( get, set ) : Int;
    public var bi ( get, set ) : Int;
    public var ai ( get, set ) : Int;
    private inline function get_ri() : Int {
        return Std.int( this[0] * 255 );
    }
    private inline function set_ri( r : Int) : Int {
        this[0] = r < 0 ? 0.0 : r > 255 ? 1.0 : r / 255;
        return ri;
    }
    private inline function get_gi() : Int {
        return Std.int( this[1] * 255 );
    }
    private inline function set_gi( g : Int) : Int {
        this[1] = g < 0 ? 0.0 : g > 255 ? 1.0 : g / 255;
        return gi;
    }
    private inline function get_bi() : Int {
        return Std.int( this[2] * 255 );
    }
    private inline function set_bi( b : Int) : Int {
        this[2] = b < 0 ? 0.0 : b > 255 ? 1.0 : b / 255;
        return bi;
    }
    private inline function get_ai() : Int {
        return Std.int( this[3] * 255 );
    }
    private inline function set_ai( a : Int) : Int {
        this[3] = a < 0 ? 0.0 : a > 255 ? 1.0 : a / 255;
        return ai;
    }

    public static inline function create( r : Float, g : Float, b : Float, a : Float ) : RGBAColor {

	    var rgbaColor = new RGBAColor();

	    rgbaColor.r = r;
		rgbaColor.g = g;
		rgbaColor.b = b;
		rgbaColor.a = a;

	    return rgbaColor;
	}

    public static inline function createi( r : Int, g : Int, b : Int, a : Int ) : RGBAColor {

	    var rgbaColor = new RGBAColor();

	    rgbaColor.ri = r;
		rgbaColor.gi = g;
		rgbaColor.bi = b;
		rgbaColor.ai = a;

	    return rgbaColor;
	}

	public static function getLightColor( light : Light ) : RGBAColor {

		var v : Vec4 = [
			light.color.r / 255.,
			light.color.g / 255.,
			light.color.b / 255.,
			1.0,
        ];
		v.scale( light.intensity, v );
        v.w = light.color.a / 255.;

		return RGBAColor.fromVec4( v );
	}

    public function setFromRGBA( rgba : RGBA ) {

	    ri = rgba.r;
	    gi = rgba.g;
	    bi = rgba.b;
	    ai = rgba.a;
	}

    @:from
    public static function fromInt( value : Int ) : RGBAColor {

	    var rgbaColor = new RGBAColor();

	    rgbaColor.setFromRGBA( new RGBA( value ) );

	    return rgbaColor;
	}
    @:to
    public function toInt() : Int {

	    var value : Int = ri << 24 | gi << 16 | bi << 8 | ai;

	    return value;
	}

    @:from
    public static function fromRGBA( rgba : RGBA ) : RGBAColor {

	    var rgbaColor = new RGBAColor();

	    rgbaColor.setFromRGBA( rgba );

	    return rgbaColor;
	}
    @:to
    public function toRGBA() : RGBA {

	    var rgba = new RGBA();

        rgba.r = ri;
        rgba.g = gi;
        rgba.b = bi;
        rgba.a = ai;

	    return rgba;
	}

    @:from
    public static function fromVec3( vector : Vec3 ) : RGBAColor {

	    var rgbaColor = new RGBAColor();

	    rgbaColor.r = vector.x;
		rgbaColor.g = vector.y;
		rgbaColor.b = vector.z;
		rgbaColor.a = 1.0;

	    return rgbaColor;
    }
    @:to
    public function toVec3() : Vec3 {

	    var vector = new Vec3();

	    vector.x = r;
		vector.y = g;
		vector.z = b;

	    return vector;
    }

    @:from
    public static function fromVec4( vector : Vec4 ) : RGBAColor {

	    var rgbaColor = new RGBAColor();

	    rgbaColor.r = vector.x;
		rgbaColor.g = vector.y;
		rgbaColor.b = vector.z;
		rgbaColor.a = vector.w;

	    return rgbaColor;
    }
    @:to
    public function toVec4() : Vec4 {

	    var vector = new Vec4();

	    vector.x = r;
		vector.y = g;
		vector.z = b;
		vector.w = a;

	    return vector;
    }

    @:from
    static public function fromArray( a : Array<Float> ) {

		var xc = new RGBAColor();

		xc[0] = a[0];
		xc[1] = a[1];
		xc[2] = a[2];
		xc[3] = a[3];

        return xc;
    }
    @:to
    public function toArray() {
        return [this[0], this[1], this[2], this[4]];
    }

    public function toString() : String {
        var vi = StringTools.hex( toInt(), 8 );
        return 'RGBAColor(${r},${g},${b},${a})#0x$vi';
    }
}
