package mme.core;

import mme.core.Mesh;
import mme.core.Material;
import mme.core.SceneNode;

import mme.material.BasicMaterial;

class SceneObject extends SceneNode {

    public var mesh : Mesh;
    public var material : Material;

    public function new( ?material : Material ) {

        super();

        mesh = new Mesh();

        if( material != null ) {
            this.material = material;
        } else {
            this.material = new BasicMaterial();
        }
    }

    public function needsUpdate() {

        scene.scheduleForUpdate( this );
    }
}
