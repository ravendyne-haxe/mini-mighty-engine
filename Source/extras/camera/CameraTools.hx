package mme.extras.camera;

import mme.core.Camera;
import mme.camera.PerspectiveCamera;
import mme.camera.OrthoCamera;

import mme.math.glmatrix.Vec3;
using mme.math.glmatrix.Vec3Tools;
using mme.math.glmatrix.Mat3Tools;


typedef CameraRay = {
    origin : Vec3,
    direction : Vec3,
}

class CameraTools {

    public static function updateCameraFillHeight( camera : OrthoCamera, cameraViewSizeHeight : Float, windowWidth : Float, windowHeight : Float ) {

        var cameraViewAspect = windowWidth / windowHeight;

        var newCameraViewWidth = cameraViewSizeHeight * cameraViewAspect;
        camera.right = camera.left + newCameraViewWidth;
    }

    public static function updateCameraFillWidth( camera : OrthoCamera, cameraViewSizeHeight : Float, windowWidth : Float, windowHeight : Float ) {

        var cameraViewAspect = windowWidth / windowHeight;

        var newCameraViewHeight = cameraViewSizeHeight / cameraViewAspect;
        camera.top = camera.bottom + newCameraViewHeight;
    }

    public static function updateCameraKeepRatios( camera : OrthoCamera, cameraViewUnitRatio : Float, windowWidth : Float, windowHeight : Float ) {

        // assumes square pixel
        var newCameraViewWidth = windowWidth / cameraViewUnitRatio;
        var newCameraViewHeight = windowHeight / cameraViewUnitRatio;

        camera.right = camera.left + newCameraViewWidth;
        camera.top = camera.bottom + newCameraViewHeight;
    }

    public static function updateCameraToWindowSize( camera : OrthoCamera, windowWidth : Float, windowHeight : Float ) {

        // assumes square pixel
        updateCameraKeepRatios( camera, 1.0, windowWidth, windowHeight );
    }

    public static function updateCameraResize( camera : Camera, windowWidth : Float, windowHeight : Float ) {

        if( Std.is( camera, OrthoCamera ) ) {

            var ocamera : OrthoCamera = cast camera;
            // ocamera.top - ocamera.bottom --> means keep whatever height in the world is set for camera
            updateCameraFillHeight( ocamera, ocamera.top - ocamera.bottom, windowWidth, windowHeight );

        } else if( Std.is( camera, PerspectiveCamera ) ) {

            var pcamera : PerspectiveCamera = cast camera;
            pcamera.aspect = windowWidth / windowHeight;
        }
    }

    public static function ray( camera : Camera, mouseX : Int, mouseY : Int, viewportWidth : Int, viewportHeight : Int ) : CameraRay {

        // invert mouse y so that coordinates start at lower-left
        mouseY = viewportHeight - mouseY;


        var rayOrigin = new Vec3();
        var rayDirection = new Vec3();

        // for perspective projection:
        //      - ray origin is at camera position
        //      - we have to adjust ray direction
        // for ortho projection:
        //      - ray direction is parallel to camera view direction
        //      - we have to adjust ray origin relative to camera position

        // after scaling down to normalized device coordinates
        // projection plane is at z = -1

        if( Std.is( camera, PerspectiveCamera ) ) {
            // perspective projection

            // assumes camera is at plane center
            var projectionPlaneX = ( mouseX / viewportWidth ) * 2.0 - 1.0;
            var projectionPlaneY = ( mouseY / viewportHeight ) * 2.0 - 1.0;

            var planePointVector : Vec3 = [ projectionPlaneX, projectionPlaneY, -1 ];

            planePointVector.normalize( planePointVector );

            // rotate to where camera is looking in the world
            camera.rotationMatrix.multiplyVec3( planePointVector, planePointVector );

            // the ray
            rayOrigin = camera.scenePosition;
            rayDirection = planePointVector;

        } else if( Std.is( camera, OrthoCamera ) ) {
            // ortho projection
            var ocamera : OrthoCamera = cast camera;

            var cameraWorldWidth = ocamera.right - ocamera.left;
            var cameraWorldHeight = ocamera.top - ocamera.bottom;

            // assumes camera is at plane lower-left
            var projectionPlaneX = mouseX / viewportWidth;
            var projectionPlaneY = mouseY / viewportHeight;

            var planePointVector : Vec3 = [ 0, 0, -1 ];
            var offset : Vec3 = [ ocamera.left + projectionPlaneX * cameraWorldWidth, ocamera.bottom + projectionPlaneY * cameraWorldHeight, 0 ];

            planePointVector.normalize( planePointVector );

            // rotate to where camera is looking in the world
            camera.rotationMatrix.multiplyVec3( planePointVector, planePointVector );
            camera.rotationMatrix.multiplyVec3( offset, offset );

            // ray
            rayOrigin = camera.scenePosition.add( offset );
            rayDirection = planePointVector;
        }

        return {
            origin: rayOrigin,
            direction: rayDirection
        }
    }
}
