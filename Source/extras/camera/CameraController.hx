package mme.extras.camera;

import lime.ui.MouseButton;
import mme.core.Camera;
import mme.math.glmatrix.Mat3;
import mme.math.glmatrix.Vec3;
import lime.ui.Window;
import mme.core.Group;

class CameraController extends Group {

    var window : Window;
    var cameraInitialPosition : Vec3;
    var cameraInitialOrientation : Mat3;
    var controllerInitialPosition : Vec3;
    var controllerInitialOrientation : Mat3;

    public var camera ( default, null ) : Camera;

    // TODO have renderer as a parameter instead of window and fetch the window from renderer
    public function new( camera : Camera, window : Window ) {

        // save references
        this.camera = camera;
        this.window = window;

        super();

        add( camera );

        cameraInitialPosition = camera.position;
        cameraInitialOrientation = camera.rotationMatrix;
        controllerInitialPosition = this.position;
        controllerInitialOrientation = this.rotationMatrix;

        window.onMouseDown.add( _onMouseDown_cc );
        window.onMouseUp.add( _onMouseUp_cc );
    }

    public function reset() {
        camera.position = cameraInitialPosition;
        camera.rotationMatrix = cameraInitialOrientation;
        this.position = controllerInitialPosition;
        this.rotationMatrix = controllerInitialOrientation;
    }

    var primaryButtonDown : Bool = false;
    var middleButtonDown : Bool = false;
    var secondaryButtonDown : Bool = false;

    private function setButtonDown( button : MouseButton ) {
        switch( button ) {
            case 0:
                primaryButtonDown = true;
            case 1:
                middleButtonDown = true;
            case 2:
                secondaryButtonDown = true;
            default:
        }
    }

    private function setButtonUp( button : MouseButton ) {
        switch( button ) {
            case 0:
                primaryButtonDown = false;
            case 1:
                middleButtonDown = false;
            case 2:
                secondaryButtonDown = false;
            default:
        }
    }

    private function _onMouseDown_cc( x : Float, y : Float, button : MouseButton ) : Void {
        setButtonDown( button );
    }

	private function _onMouseUp_cc( x : Float, y : Float, button : MouseButton ) : Void {
        setButtonUp( button );
    }

}