package mme.extras.camera;

import mme.math.glmatrix.Vec3;
import mme.camera.OrthoCamera;
import lime.utils.Log;
import lime.ui.MouseWheelMode;
import lime.ui.Window;

import mme.core.Camera;

using mme.math.glmatrix.Vec3Tools;


class OrbitCameraController extends CameraController {

    public var zoomDistanceDelta : Float;
    public var zoomDistanceLow : Float;
    public var zoomDistanceHigh : Float;

    public function new( camera : Camera, window : Window ) {

        super( camera, window );

        zoomDistanceDelta = 0.1;
        zoomDistanceLow = 1000;
        zoomDistanceHigh = 5000;

        window.onMouseMoveRelative.add( onMouseMoveRelative );
        window.onMouseWheel.add( onMouseWheel );
        window.onResize.add( onWindowResize );

        // fix for ortho camera so we orbit around viewport center
        if( Std.is( camera, OrthoCamera ) ) {
            var ocamera : OrthoCamera = cast camera;
            var offset : Vec3 = [ ( ocamera.left + ocamera.right ) / 2, ( ocamera.bottom + ocamera.top ) / 2, 0 ];
            translate( offset );
            camera.translate( offset.scale( -1 ) );
        }
    }

    public function orbitPan( angle : Float ) {
        rotate( angle, camera.cameraUp );
    }

    public function orbitTilt( angle : Float ) {
        var axis = camera.cameraLook.cross( camera.cameraUp );
        rotate( angle, axis );
    }

    public function updateCameraZoom( delta : Float ) {
        var deltaZ = camera.positionZ * zoomDistanceDelta;
        // Log.info('delta ${delta}');
        // Log.info('camera.positionZ ${camera.positionZ}');
        // Log.info('deltaZ ${deltaZ}');
        if( delta > 0.0 ) {
            if( camera.positionZ - deltaZ < zoomDistanceLow ) return;
            camera.translate([ 0, 0, - deltaZ ]);
        }
        if( delta < 0.0 ) {
            if( camera.positionZ + deltaZ > zoomDistanceHigh ) return;
            camera.translate([ 0, 0, deltaZ ]);
        }
    }

    var rotation = 5.0 / 50.0; // in degrees per pixel
	private function onMouseMoveRelative( x : Float, y : Float ) : Void {
        if( primaryButtonDown ) {
            orbitPan( - rotation * x );
            orbitTilt( - rotation * y );
        }
    }

	private function onMouseWheel( deltaX : Float, deltaY : Float, deltaMode : MouseWheelMode ) : Void {
        updateCameraZoom( deltaY );
    }

	private function onWindowResize( width : Int, height : Int ) : Void {

        var gl = window.context.webgl;
        // FIXME should we keep a reference of renderer?
        // renderer.viewport( gl, width, height );
        gl.viewport( 0, 0, width, height );

        CameraTools.updateCameraResize( camera, width, height );
    }
}
