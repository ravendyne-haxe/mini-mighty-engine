package mme.light;

import lime.math.RGBA;

import mme.core.Light;


class AmbientLight extends Light {

    public function new( color : RGBA = 0xffffffff, intensity : Float = 0.1 ) {

        super( color, intensity );
    }
}