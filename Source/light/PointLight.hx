package mme.light;

import lime.math.RGBA;

import mme.core.Light;


class PointLight extends Light {

    public var attenuation : Float;

    public function new( color : RGBA = 0xffffffff, intensity : Float = 1.0 ) {

        super( color, intensity );

        attenuation = 0.0;
    }
}
