package mme.camera;

import mme.core.Camera;

import mme.camera.CameraUtils;


class PerspectiveCamera extends Camera {

    public var fov ( get, set ) : Float;
    public var aspect ( get, set ) : Float;
    public var near ( get, set ) : Float;
    public var far ( get, set ) : Float;

    // have to have vars like this, otherwise constructor calls
    // setters and everythig goes haywire
    private var _fov : Float;
    private var _aspect : Float;
    private var _near : Float;
    private var _far : Float;

    public function new( fov : Float, aspect : Float, zNear : Float, zFar : Float ) {

        super();
        isPerspective = true;

        _fov = fov;
        _aspect = aspect;
        _near = zNear < 1 ? 1 : zNear;
        _far = zFar < _near ? _near + 1000.0 : zFar;

        cameraProjectionMatrix = CameraUtils.perspective( _fov, _aspect, _near, _far );
    }

    private function get_fov() : Float {
        return _fov;
    }
    private function set_fov( fov : Float ) : Float {
        _fov = fov;
        cameraProjectionMatrix = CameraUtils.perspective( _fov, _aspect, _near, _far );
        return _fov;
    }

    private function get_aspect() : Float {
        return _aspect;
    }
    private function set_aspect( aspect : Float ) : Float {
        _aspect = aspect;
        cameraProjectionMatrix = CameraUtils.perspective( _fov, _aspect, _near, _far );
        return _aspect;
    }

    private function get_near() : Float {
        return _near;
    }
    private function set_near( zNear : Float ) : Float {
        _near = zNear;
        cameraProjectionMatrix = CameraUtils.perspective( _fov, _aspect, _near, _far );
        return _near;
    }

    private function get_far() : Float {
        return _far;
    }
    private function set_far( zFar : Float ) : Float {
        _far = zFar;
        cameraProjectionMatrix = CameraUtils.perspective( _fov, _aspect, _near, _far );
        return _far;
    }
}
