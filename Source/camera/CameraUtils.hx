package mme.camera;

import mme.math.glmatrix.Mat4;


class CameraUtils {

    /**
        Create matrix with values for an orthographic projection.
        Camera plane is parallel to XY-plane and coordinate system is assumed to be right-handed.
        zNear and zFar are named so because it is assumed that camera is oriented to look from
        *positive* towards the *negative* Z-axis values. Having that in mind, it can be counterintuitive
        to specify (zNear, zFar) as (500, -500). However, flipping the values around will not mess up
        the scene except for the Z-order of scene objects, so specifying (zNear, zFar) as (-500, 500)
        will render scene in exactly the same way except that Z-order will be flipped. Z-order flipping
        is usually not an issue in ortho projections unless it is ;)

        @param    left      The left side depth-clipping plane position
        @param    rigth     The right side depth-clipping plane position
        @param    bottom    The bottom side depth-clipping plane position
        @param    top       The top side depth-clipping plane position
        @param    zNear     The near depth-clipping plane position
        @param    zFar      The far depth-clipping plane position
    **/
    public static function ortho( left : Float, right : Float, bottom : Float, top : Float, zNear : Float, zFar : Float ) : Mat4 {

        var sx = 1.0 / ( right - left );
        var sy = 1.0 / ( top - bottom );
        var sz = 1.0 / ( zFar - zNear );

        var tx = ( left + right ) * sx;
        var ty = ( top + bottom ) * sy;
        var tz = ( zFar + zNear ) * sz;

        var matrix = new Mat4();

        matrix[ 0 ] = 2 * sx;
        matrix[ 1 ] = 0;
        matrix[ 2 ] = 0;
        matrix[ 3 ] = 0;

        matrix[ 4 ] = 0;
        matrix[ 5 ] = 2 * sy;
        matrix[ 6 ] = 0;
        matrix[ 7 ] = 0;

        matrix[ 8 ] = 0;
        matrix[ 9 ] = 0;
        matrix[ 10 ] = 2 * sz;
        matrix[ 11 ] = 0;

        matrix[ 12 ] = - tx;
        matrix[ 13 ] = - ty;
        matrix[ 14 ] = - tz;
        matrix[ 15 ] = 1;

        return matrix;
    }

    /**
        Create matrix with values for a perspective projection
        @param    angleDegrees  The vertical view angle, angular size of the near plane height as viewed from the camera eye point
        @param    aspect        The value of vieport width divided by height, aspect ratio of the viewport
        @param    zNear         The near depth-clipping plane position
        @param    zFar          The far depth-clipping plane position
    **/
    // http://www.terathon.com/gdc07_lengyel.pdf
    public static function perspective( fov : Float, aspect : Float, zNear : Float, zFar : Float ) : Mat4 {

        // to radians
        fov = fov * Math.PI / 180.;
        // "focal length" is distance from camera position (the "eye") to the projection plane
        // assuming that projection plane is square and it's dimensions are [ -1, +1 ]
        var focal_length = 1. / Math.tan( fov / 2. );

        var matrix = new Mat4();

        matrix[ 0 ] = focal_length / aspect; // keep height
        // matrix[ 0 ] = focal_length; // keep width
        matrix[ 1 ] = 0;
        matrix[ 2 ] = 0;
        matrix[ 3 ] = 0;

        matrix[ 4 ] = 0;
        matrix[ 5 ] = focal_length; // keep height
        // matrix[ 5 ] = focal_length * aspect; // keep width
        matrix[ 6 ] = 0;
        matrix[ 7 ] = 0;

        matrix[ 7 ] = 0;
        matrix[ 8 ] = 0;
        matrix[ 10 ] = - ( zNear + zFar ) / ( zFar - zNear );
        matrix[ 11 ] = -1.;

        matrix[ 12 ] = 0.;
        matrix[ 13 ] = 0.;
        matrix[ 14 ] = - 2. * zFar * zNear  / ( zFar - zNear );
        matrix[ 15 ] = 0;

        return matrix;
    }
}