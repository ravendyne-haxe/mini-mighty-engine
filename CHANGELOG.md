# HEAD


# v1.4.1

- fix `StlLoader` initialization bug


# v1.4.0

- create `XGLRegistry` per context
- `SceneNode` updates children visibility on `visible` property change
- simplify `Utils.convertPath2D()`
- add `encode()/decode()` methods to `PolyLine2D`
- add `PathUtils`
- remove `x,y` parameters from `TextLabel2D` constructor
- add `SceneNode.toString()` method
- add `fontSize` property to `FontAtlas` and `FontSource` and update `default.fontatlas` binary
- add `PathUtils.triangulateContour()`
- rewrite 2D point fragment shader to use distance fields
- add diamond shaped 2D point
- remove parameter in `MMEngine.initialize()`
- create missing registry for context in `MMEngine.addRegistryExtension()`
- remove `XGLRegistry.initialized`
- return `null` in `XGLRegistry.get()` if no registry for any context has been initialized
- use `ImageBuffer` as a data source for `XGLTexture`
- make approximation_scale configurable in `XGLConvertPolyline2DToBufferLine2D` and `XGLConvertLineCollection2DToBufferLine2D`


# v1.3.0

- add vertical text rendering to `FontAtlas` and `FontSource` `renderText()` methods
- refactor `XGLConvertToBufferBasic` to correctly support indexed meshes
- refactor `XGLConvertToBufferUV` to correctly support indexed meshes
- refactor `XGLConvertToBufferNormal` to correctly support indexed meshes
- refactor `XGLConvertToBufferNormalUV` to correctly support indexed meshes
- remove all overrides of `renderDraw()` and move code to `XGLRenderObject` base class
- replace `lime.math.RGBA` with `mme.core.RBGAColor`
- add `Utils.convertPath2D()` method to convert `Path2D` to one or more `Polyline2D` objects
- move vertex attribute setup and buffer(s) binding to `XGLRenderObject`
- introduce two stage conversion to `XGLMeshConverter` and refactor subclasses to use it


# v1.2.0

- remove `finalize()` method for point cloud 2d
- remove `finalize()` method for line collection 2d
- remove `finalize()` methods for polylines and curve2d
- add `Mesh.clear()` method
- fix `FontAtlas` and `FontSource` handling of non-image glyphs
- fix `Mesh.copyFrom()` method: clear arrays before copy
- add `TextLabel2D.setText()` method
- add scaling to `SceneNode`
- add `MMEngine.nextId()` function
- replace shader instance variables with a map id -> shader in `XGLRegistry`
- replace `ShaderName` constants with static `ID`s defined in shader classes
- update `Curve2D` and `PolylineToMesh` to conform to new `VertexConsumer` interface
- add `Utils.triangulatePolyline()` method
- add `SceneNode.visible` property and support for it
- discard fragments with alpha ~ 0 in point shader
- add optional shader file name parameter to `getVertexShaderSource`/`getFragmentShaderSource` macros
- add element type arg to `Mesh` constructor


# v1.1.0

- change implementation of 2D line rendering to support line caps (butt, square, round) and joins (miter, bevel, round)
- replace line3d shader with simpler one: all line segments rendered as independent lines with butt or square cap
- remove `u_Miter` uniform and add `u_Opacity` to line3d shader
- fix line 2d rendering issues:
    - move `thickness` from material to scene object
    - update `edgeColor` dynamically inside render object
    - add handling of closed polylines in polyline convertor
- add `scene` property check in `SceneObject.needsUpdate()`
- add `aspect` property to `Camera`
- add `u_Aspect` uniform to 3D line shader
- copy `otherParams` too in `Mesh.copyFrom()`
- add `MMEngine` class
- add `OrthoScene` class
- change texture materials constructors to accept `lime.graphics.Image` as parameter instead of a string
- change `XGLRegistry.getShader()` to look in extensions first so we can (possibly) override built-in shaders
- add `sizeX/Y/Z` properties to `BoundingBox`
- update `XGLConvertToBufferBasic` to handle point, line, triangle and quad mesh types
- replace `MeshElement.elementType` with `MeshElement.elementCount`
- update `XGLConvertToBufferUV` to handle all mesh types
- add support for rendering text: font atlas and SDF based shader. OpenSans Regular added as default font to assets.
- fix passing parameter to `gl.uniform3fv()` so it works with javascript (convert `float[4]` to `float[3]`)


# v1.0.1

- rename shader attributes to conform to standard (prefix `a_`)
- refactor and cleanup geometry converters
- move `XGLVertexAttributeParams` to `XGLArrayBuffer`
- remove unused imports
- create default material for `PointCloud2D` when no material and no color are specified
- replace `miter` constant with `u_Miter` uniform in `XGLLineShader` and add `applyMiter` property to `LineMaterial`
- add `background` parameter to `Renderer`
- `XGLRegistry.createRenderObjectFor()` first queries extensions so we can override use of default materials
- remove `XGLRenderObject.mesh` and `XGLRenderObject.meshMaterial` references


# v1.0.0
- Initial release
