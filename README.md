
# Mini Mighty Engine

- **Mini** - well, it is small
- **Mighty** - not really, it's more tongue-in-cheek kind of an attribute :)
- **Engine** - well, it is an engine


## Dependencies

- [Lime](https://github.com/openfl/lime)
- [haxe-gl-matrix](https://gitlab.com/ravendyne-haxe/haxe-gl-matrix)
- [mme-tools-library](https://gitlab.com/ravendyne-haxe/mme-tools-library)


## Background

This project started as a way to learn Haxe and create something useful on the way. OpenGL and 3D graphics was one of possible areas to create something in, and it was the one that won in the end.

Out of all the Haxe-based graphics frameworks out there, **Lime** is selected because:

- it's super easy (and super lean) to create "just-open-that-graphics-window" kind of an app
- OpenGL is (almost) directly exposed
- number of build targets supported, and thus possible to play with
- it's, arguably, the leanest framework that just exposes OpenGL and adds a number of utilites you may, but don't have to, use

Regardles of dependency on Lime, it is possible to, more or less, easily port MME to use some other framework due to its layered design and extensive use of OpenGL API.


## Very shortly, what it is and how it looks like

MME is **very much** a work in progress that may or may not evolve into anything other than a toy-engine.

That being said, its design is pretty simple and straightforward and it is usable as a platform to implement and play with ideas you might have about how to do this or that in OpenGL.


### What is it

MME aims to provide simple way of creating and rendering 3D content by combining _scene objects_ into a tree hierarchy called _a scene_. This is considered a _high-level_ layer of MME desing.

Second important aim of MME is to provide a fairly simple way of adding new ways to render 3D content. This is done by implementing a new GLSL shader together with supporting Haxe code that integrates the shader into MME. This part is considered a _low-level_ layer of MME design.


### How it looks like: Internal design

So, MME has two layers:

- **high-level**: this is where all 3D-content related classes live (cameras, lights, meshes, boxes, planes, etc. etc. all that fun stuff)
- **low-level**: this is what translates high-level stuff to OpenGL API realm (think shaders, geometry buffers, API calls, rendering engine, etc. etc. all that fun stuff :) )

There is also a kind of a mid-layer which is considered a part of low-level stuff. This mid-layer code is responsible for decoupling high-level design from data structures and design of the low-level layer. For example, mesh converters are a kind of code that could be considered a mid-layer code. As far as overall design is conserned, mid-layer code belongs to low-level layer.

It is easy to spot classes that belong to low-level layer:

- they all live in `mme.xgl` package
- they all have `XGL` prefix added to their name


### How it looks like: High-level Layer

Basis of _high-level_ layer is `SceneNode`. Scene nodes are added to one another to form a tree structure which is later on rendered to OpenGL context. `Scene` is used to serve as a unit that keeps all the `SceneNode`s together and the scene is rendered by `Renderer` into a `lime.graphics.RenderingContext` of a `lime.ui.Window`.

`SceneObject` is a subclass of `SceneNode` that will end up visible on screen. There is a number of subclasses of `SceneObject` that represent some simple objects: `Box`, `Cone`, `Circle`, `Plane`, `Cylinder` etc.

Each `SceneObject` has a `Material` assigned to it that determines how it will be rendered at the end.

There are also `OrthoCamera` and `PerspectiveCamera` as well as `AmbientLight` and `PointLight`, all subclasses of `SceneNode`.

A sample scene might be constructed like this:

~~~haxe
    var scene = new Scene();

    var camera = new PerspectiveCamera( 60, window.width / window.height, 1, 5000 );
    camera.translate([ 0, 0, 1000 ]);
    scene.addCamera( camera );

    var light = new PointLight();
    light.translate([ 500, 500, 750 ]);
    scene.addLight( light );

    scene.addLight( new AmbientLight() );

    var box = new Box( 300, 300, new PhongMaterial( NamedColor.green ) );
    box.rotate( 45, Vec3.Y_AXIS );
    box.rotate( 45, Vec3.X_AXIS );
    scene.add( box );

    var renderer = new Renderer( window.context.webgl );
~~~

and then in `lime.app.Application.onRender()` override you would have to call:

~~~haxe
    renderer.render( context.webgl, scene );
~~~

By doing that, after building and running the app, you may end up with a window looking like this:

![MiniMightyEngine_sample](img/MiniMightyEngine_sample.png)


### How it looks like: Low-level Layer

At the low level, MME is designed around GLSL shaders: shaders need source code to compile and link, and then they need vertex attribute (and other) buffers and uniforms properly set up to be able to do their work.

Three foundational low-leve parts of MME are:

- **shader**: represented by `XGLShader` class
- **geometry buffer**: represented by `XGLGeometryBuffer` class
- **material**: represented by `XGLMaterial` class

These three are combined into a **render object** which maps one-on-one into a `SceneObject` created by high-level code. Different types of render objects are implemented by subclassing `XGLRenderObject` class.

Render objects are created, collected and kept up-to-date by an instance of `XGLRenderer` which is thinly wrapped into `Renderer` class used by high-level code to create a renderer.

`XGLRenderer` keeps render objects sorted by a shader binary program instance they use, and when time comes it loads and sets up a specific shader program and then renders all objects that use that shader program, then goes on to the next shader etc.

And that's pretty much it.


## Quick full working example app how-to

Of course, first make sure you have Haxe installed ;)

MME is currently developed using Haxe v4.0.0-rc2 and Lime v7.3.0.

Next, install MME:

    haxelib install mini-mighty-enige

That will pull in all required dependencies.

Create a Lime project

    lime create project mme-test
    cd mme-test
    lime build neko
    lime run neko

If you haven't got `lime` alias set up then just replace all `lime` references with `haxelib run lime`, i.e.:

    haxelib run lime create project mme-test

Once you run `lime run neko` you should see black empty window:

![MmeStart_EmptyWindow](img/MmeStart_EmptyWindow.png)

Edit `project.xml` file and add this line to it:

~~~xml
    <haxelib name="mini-mighty-engine" />
~~~

Replace content of `Source/Main.hx` file with this:

~~~haxe
    package;

    import lime.app.Application;
    import lime.graphics.RenderContext;

    import mme.math.glmatrix.Vec3;

    import mme.core.Scene;
    import mme.core.Renderer;
    import mme.light.PointLight;
    import mme.light.AmbientLight;
    import mme.material.PhongMaterial;
    import mme.material.NamedColor;
    import mme.geometry.Box;

    import mme.camera.PerspectiveCamera;
    import mme.extras.camera.OrbitCameraController;

    class Main extends Application
    {
        var scene : Scene;
        var renderer : Renderer;

        public function new()
        {
            super();
        }

        public override function onPreloadComplete() : Void {

            scene = new Scene();

            var camera = new PerspectiveCamera( 60, window.width/window.height, 1, 5000 );
            camera.translate([ 0, 0, 1000 ]);
            scene.addCamera( camera );

            var ctrlr = new OrbitCameraController( camera, window );
            scene.add( ctrlr );

            var light = new PointLight();
            light.translate([ 500, 500, 750 ]);
            scene.addLight( light );

            scene.addLight( new AmbientLight() );

            var box = new Box( 300, 300, new PhongMaterial( NamedColor.green ) );
            box.rotate( 45, Vec3.Y_AXIS );
            box.rotate( 45, Vec3.X_AXIS );
            scene.add( box );

            renderer = new Renderer( window.context.webgl );
        }

        public override function render( context : RenderContext ) : Void {

            if (!preloader.complete) return;

            switch (context.type) {

                case OPENGL, OPENGLES, WEBGL:

                    renderer.render( context.webgl, scene );

                default:

            }
        }
    }
~~~

Build and run again:

    lime build neko
    lime run neko

and you should see something like this:

![MmeStart_HelloWorld](img/MmeStart_HelloWorld.png)

Since we added `OrbitCameraController` to the scene, you can click and drag mouse to rotate the cube around.

You can also compile this code without any changes for `html5` target.

Build and start the app:

    lime build html5
    lime run html5

and you should see something like this:

![MmeStart_Mozilla_Firefox](img/MmeStart_Mozilla_Firefox.png)

Again, you can click and drag mouse to rotate the cube around.



## Other stuff

- There will be a bit more detailed how-to documentation on MME later on.
- MME can load STL and OBJ/MTL files as `ScenObjects` that can be directly added to a scene.
- There are few pseudo-2D `SceneObjects` (lines, points, polylines, bezier curves) that can be used with `OrthoCamera` for pseudo-2D drawing
- MME has emissive, textured, phong colored and textured materials and a simple wireframe material. All just basic stuff.
- There's `OrbitCameraController` which enables scene rotation and zoom in/out with mouse and keeps view up to date with window resize events, nothing too fancy.
- `OrthoCameraController` enables pan and zoom in/out with few different options on how to keep ortho camera up to date with window resize events.
- `mme.math.Parametric` class helps with generating parametric surfaces in general.

## Few screenshots

![MiniMightyEngine_Terrain](img/MiniMightyEngine_Terrain.png)

![MiniMightyEngine_Loaders_Demo](img/MiniMightyEngine_Loaders_Demo.png)

![MiniMightyEngine_Loaders_Demo](img/MiniMightyEngine_Two_Scenes_Demo.png)

![MiniMightyEngine_Loaders_Demo](img/MiniMightyEngine_Device_Demo.png)

![MiniMightyEngine_Lodaer_Trees](img/MiniMightyEngine_Lodaer_Trees.png)

![MiniMightyEngine_Wireframe](img/MiniMightyEngine_Wireframe.png)
